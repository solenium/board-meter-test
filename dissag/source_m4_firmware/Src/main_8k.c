/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "openamp.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "arm_math.h"
#include "tm_stm32_buffer.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */
// Definiciones para usar el canal virtual USART0 y 1
VIRT_UART_HandleTypeDef virtUART0;							// Handler for Virtual Usart0
VIRT_UART_HandleTypeDef virtUART1;							// Handler for Virtual Usart1
VIRT_UART_StatusTypeDef virtualUsart0_State;				// TxFlag for Virtual Usart0
VIRT_UART_StatusTypeDef virtualUsart1_State;				// TxtFlag for Virtual Usart1

#define MAX_BUFFER_SIZE 512									// Max number of chars in a single VirtualUsart Transaction
__IO FlagStatus VirtUart0_RxMsg = RESET;					// RxFlag for Virtual Usart0
__IO FlagStatus VirtUart1_RxMsg = RESET;					// RxFlag for Virtual Usart1
uint8_t VirtUart0_ChannelBuffRx[MAX_BUFFER_SIZE];			// RxBuffer for Virtual Usart0
uint8_t VirtUart1_ChannelBuffRx[MAX_BUFFER_SIZE];			// RxBuffer for Virtual Usart1
uint16_t VirtUart0_ChannelRxSize = 0;						// AuxVariable to hold the number of Rx chars for Usart0
uint16_t VirtUart1_ChannelRxSize = 0;						// AuxVariable to hold the number of Rx chars for Usart1

/* Variables del ADC */
#define samplingRate_Hz  8000
#define signalFrecuency	 60
#define numberOfSamplesPeriod  	(samplingRate_Hz/signalFrecuency)
#define numberOfSamples			1000 //TEST_LENGTH_SAMPLES

#define ADC_BUF_LEN 4											// Number of channels used for ADC conversions
#define VOL_BUF_LEN numberOfSamples //(numberOfSamplesPeriod)	// Number of data to hold on FFT buffers
uint16_t adc_buf[ADC_BUF_LEN];
uint16_t voltage1_buf[VOL_BUF_LEN];
uint16_t voltage2_buf[VOL_BUF_LEN];
uint16_t voltage3_buf[VOL_BUF_LEN];

uint16_t current1_buf[VOL_BUF_LEN];
uint16_t current2_buf[VOL_BUF_LEN];
uint16_t current3_buf[VOL_BUF_LEN];

uint8_t vol_counter 	= 0;									// Counter for voltage

/* Variables to send the buffers in realTime */
#define lenADCMssg (ADC_BUF_LEN*3 +1)
char bufferVirtualUsart0[lenADCMssg] = {0};

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;
DMA_HandleTypeDef hdma_adc1;

IPCC_HandleTypeDef hipcc;

TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim5;

UART_HandleTypeDef huart7;

/* USER CODE BEGIN PV */

/* Xring buffers definitions to simulate queues */
TM_BUFFER_t xQueueA_, xQueueB_, xQueueC_;

// Definiciones de variables y tipo de datos para generar el eventDetector
samplesPerPhase dataA, dataB, dataC; 			 // Structure to send the data to the Queue A, B or C
detectorConfiguration config;                    // Pointer to config

// Variables que van a almacenar la información de los paquetes de datos
uint8_t  countSamples[3] = {0};      // Sample counter of semi-cicle timelapse
uint32_t actualVoltage[3];           // Voltage of received voltage variable
uint32_t actualVoltagePeak[3];       // Voltage peak in a semi-cicle timelapse variable
bool 	 lastSingVol[3];             // Last sing of voltage wave, true for positive
bool 	 actualSingVol[3];           // Actual sing of voltage wave. true for positive
bool 	 lastSingCurr[3];            // Last sing of current wave. true for positive
bool 	 actualSingCurr[3];          // Actual sing of current wave. true for positive
uint8_t  counterShift[3] = {0};      // Counter variable to measure the shift bw the voltage and current waves
uint32_t currentSum[3]   = {0};      // Current acumulator of one semi-cicle of voltage wave

BaseType_t pxHigherPriorityTaskWokenBuff;  // Priority variables of ring buffer to use in ISR functions
BaseType_t pxHigherPriorityTaskWokenQueue; // Priority variable of queue to use in ISR functions

float thresholdNeg;                  // Negative threnshold is the same that threnshold
float threshold;                     // Thenshold variable
float thresholdNeg2;                 // Negative threnshod but in % of negative thenshold variable
float threshold2;                    // The same as the last
uint8_t eventDuration[3] = {0};      // Duration of the event in the W2 window
uint32_t ts;                         // Timestamp variable


/* Definiciones de los buffer Circulares (RingBuffers) */
TM_BUFFER_t ringBuffersHandle[3];
TM_BUFFER_t ringBuffersHandle_ToSend[3];

/* Variables necesarias para la función processData*/
// Some definitions
#define NUMBER_OF_EVENTS 	20			// Longitud de la ventana
#define AVG_LEN 			3			// Numero de mestras para comparación
// Estados para la máquina de estados
#define NE	 	0			// NE: No Event
#define PC1 	1			// PC: Pre-Confirmation
#define BE 		2			// BE: Begin Event
#define PC3 	3
#define PC2		4
#define EE 		5			// EE: End Event
#define SCALE_PEAK 1.5		// Factor de escala para a detección de picos

#define NO_EVENT	1
#define EVENT		0

// Variables de procesamiento
bool areEvents[3][256] 				= {false};		// Matriz booleana que pone un '1' cuando hay un evento en primer ringBuffer
uint8_t counterDataToSend[3] 		= {0};			// Contador auxiliar
uint32_t itemSize;									// Tamaño del dato leído del ringBuffer
uint32_t itemSizeToSend;							// Tamaño del dato leído del ringBufferToSend
uint8_t *itemBuff;									// Puntero en donde se almacenará el dato leído del ringbuffer
uint8_t *itemBuffToSend;							// Puntero en donde se almacenará el dato leído del segundo ringbuffer
uint8_t counterTimeStamp[3]			= {0};			// Contador que se irá incrementado con cada dato de la cola para el timeStamp
uint8_t counterEvent[3]				= {0};			// Contador que me indica la posición del iésimo elemento de las 20 muestras
uint8_t seconCounterEvent[3]		= {0};			// Contador auxiliar para desplazarme dentro del arreglo
float currentVector[3][20]			= {0};			// Vector para almacenar las 20 muestras de corrientes de cada semiperiodo
float currentVectorToSend[3][256]	= {0};			// Vector para almacenar los datos de corriente que se pretenden enviar
float voltageVectorToSend[3][256]	= {0};			// Vector para almacenar los datos de voltaje que se pretenden enviar
uint8_t shiftVectorToSend[3][256]	= {0};			// Vector para almacenar los datos del shift que se pretenden enviar
bool eventCondition[3]				= {0};			// Vector que contiene la información de si el evento está iniciando o si está finalizando
float currentAverage				= 0;			// Variable para almacenar el valor promedio de la corriente, usado para calcular el threshold
float thresholdEvent				= 0;			// Delta entre el promedio de las siguientes/anteriores 3 muestras con respecto a la actual
uint8_t eventState[3]				= {0};			// Estado de la maquina finita de estados (FSM)
float steadyCurrentEvent[3]			= {0};          // Steady current in the event window (Corriente al inicia del evento)
float deltaCurrentEvent[3]			= {0};          // Delta current in the event window
uint8_t steadyShiftEvent[3]			= {0};			// Steady shift in the event window
float peakPrev[3]					= {0};			// Vector que almacena el valor pico previo del evento
float eventPeak[3]					= {0};			// Vector que almacena el valor pico previo del evento
uint8_t  preConfirmationState[3]	= {0};			// Vector que almacena si nos encontramos en el PC1 o PC2 (creciente o decreciente)
uint8_t counterStableSamples[3]		= {0};			// Vector que almacena el número de muestras cuando la señal ya se encuentra estable

uint8_t f[3] 						= {0};
float endCurr						= 0.0;			// Variable auxiliar para comparar el pico hayado con el resto de muestras
float tempStartCurr					= 0.0;			// Variable auxiliar para comparar el pico hayado con el resto de muestras
uint8_t tempCounter 				= 0;			// Variable auxiliar para comparar el pico hayado con el resto de muestras

uint8_t startPeakPosition[3]		= {0};			// Posicion del pico hallado
uint8_t eventToBuff[14]				= {0};			// Arreglo que contendrá la información para ser enviada
uint16_t tempMap16					= 0;			// Variable auxiliar para guardar la información de la función map16
float ctCurrentNeg 					= 0.0;			// Corriente negativa del CT definida en la estructura de configuración
uint8_t addToBufferState			= 0;			// Variable para realizar el control de la escritura de los datos en el buffer circular
uint8_t toSendBuff[5 + (9 * MAX_EVENTS_BUFFER_TO_SEND)];
uint8_t counterBufferEvents = 0;
VIRT_UART_StatusTypeDef virtualUsartState;
uint8_t arrayToSend[14] = {0};

uint8_t auxCounter[3]				= {0};				// Contador auxiliar para almacenar valores temporales
uint16_t auxDataCalculation			= 0;				// Variable para guardar el valor residual de una operación en la primera etapa del detector de eventos

/* Variables para realizar Debugging */
uint8_t eventStateInformation[256]		= {0};			// Arreglo que contendrá los estados que pasaron por el detector de eventos
uint8_t counterEventStateInformation 	= 0;			// Numero de estados acumulados de la FSM
uint8_t counterEventsFullConfirmed		= 0;			// Numero de estados completamente confirmados (Pasan por el estado EE)
uint8_t auxArray[256]					= {0};			// Arreglo que permite ver el número de muestras por semiperiodo para el voltaje

uint8_t auxCounterTIM5 = 0;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void PeriphCommonClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_IPCC_Init(void);
static void MX_TIM2_Init(void);
static void MX_ADC1_Init(void);
static void MX_UART7_Init(void);
static void MX_TIM5_Init(void);
int MX_OPENAMP_Init(int RPMsgRole, rpmsg_ns_bind_cb ns_bind_cb);
/* USER CODE BEGIN PFP */
uint8_t addToBuffer(TM_BUFFER_t *buff, uint8_t counter, uint32_t TS, uint8_t countTS, bool type, uint8_t eventTime, float delta, float steady, float vol, uint8_t shift);									// Función que prepara los datos y los agrega al ringbufferDataToSend
uint16_t mapb16(float Value, float in_min, float in_max);
void DMA_Init_Full(void);	// Funcion para deInicializar la DMA
void virt_UART0_cb0(VIRT_UART_HandleTypeDef *huart); // Callback para el puerto virtual0
void virt_UART1_cb0(VIRT_UART_HandleTypeDef *huart); // Callback para el puerto virtual1
void processData(samplesPerPhase data, uint8_t p); // Función que permite hacer la detección de eventos
void setUpDetector(void);
void initQueues(void);
void vTaskProcessData(void);
void sendEvent(void);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
// Definición del valor absoluto para un dato
float absValue(float value){
	if(value >= 0){
		return value;
	}
	else{
		return (-value);
	}
}

void processData(samplesPerPhase data, uint8_t p){
	// Check if there are events in the firts ring buffer
	if(areEvents[p][counterDataToSend[p]]){
		// Se busca pasar el evento del ringbuffer al ringbufferToSend
		itemSize = 0;

		// Leemos el dato del ringBuffer
		itemSize = TM_BUFFER_Read(&ringBuffersHandle[p],itemBuff,1);

		// Si hay elementos en el ringBuffer
		if(itemSize != 0){

			// Devolvemos el dato al ring buffer
			TM_BUFFER_WriteToTop(&ringBuffersHandle[p],itemBuff,1);
			// Cargamos el dato en el segundo ringBuffer (Buffer_ToSend)
			TM_BUFFER_Write(&ringBuffersHandle_ToSend[p],itemBuff,1);
		}
		// Si no hay eventos se llena toda la matriz de areEvents con '0s'
		else{
            for (int j = 0; j <= 255; j++)
            {
                areEvents[p][j] = false;
            }
		}
        // Ya se atendideron los eventos, por lo tanto se baja la 'bandera'
        areEvents[p][counterDataToSend[p]] = false;
	}

	// Aumentamos el contador del TimeStamp
	counterTimeStamp[p]++;

	// Nos aseguramos de reiniciar el contador de 20 muestras
	if(counterEvent[p] >= NUMBER_OF_EVENTS){
		counterEvent[p] = 0;
	}

	 // Save the receive data to corresponding buffers, some data would be processed
	// Convertimos la corriente de ADC -> A unidades de corriente
	currentVector[p][counterEvent[p]] = ((float) data.current) / ((float) data.samples);//((/**/(((float) data.current) / ((float) data.samples)) - 1489.5)*5)/496.5;//((float) config.lsbCurrent);
	seconCounterEvent[p] = counterEvent[p];			// Actualizamos el contador auxiliar para poder desplazarnos dentro del arreglo de 20 elementos
	// Cargamos el valor actual en el vector para realizar el envío de los datos de corriente; voltage y corriente
	currentVectorToSend[p][counterDataToSend[p]] = currentVector[p][counterEvent[p]];
	voltageVectorToSend[p][counterDataToSend[p]] = ((float)data.voltage);//(((float)data.voltage) * 240)/4096;//config.lsbVoltage;
	shiftVectorToSend[p][counterDataToSend[p]]	 = data.shift;

	// Verification if the event is starting o ending, the threshold calculation is different in both cases
	if(eventCondition[p] == NO_EVENT){	// Case in not event
		// Se realiza un promedio con los proximos 3 índices (primeras 3 muestras - Muestras más viejas) para compararlo con la más nueva
		for(uint8_t i = 0; i < AVG_LEN; i++){
			// Si se llega al final, se reinicia el contador
			if(seconCounterEvent[p] >= NUMBER_OF_EVENTS -1){
				seconCounterEvent[p] = 0;
			}
			else{
				seconCounterEvent[p]++;
			}
			currentAverage += currentVector[p][seconCounterEvent[p]];
		}
		// Se calcula el delta (threshold)
		thresholdEvent = currentVector[p][counterEvent[p]] - (currentAverage/AVG_LEN);
		currentAverage = 0;		// Se reinicia el promedio para la siguiente muestra
	}
	else{	// Case in event
		// Se realiza un promedio con los anteriores 3 índices (ultimas 3 muestras)
		for(uint8_t i = 0; i < AVG_LEN; i++){
			currentAverage += currentVector[p][seconCounterEvent[p]];
			// Si se llega al final, se reinicia el contador
			if(seconCounterEvent[p] <= 0){
				seconCounterEvent[p] = NUMBER_OF_EVENTS -1;
			}
			else{
				seconCounterEvent[p]-= 2;
			}
		}
		// Se calcula el delta (threshold)
		if(counterEvent[p] >= NUMBER_OF_EVENTS - 1){
			thresholdEvent = (currentAverage/AVG_LEN) - currentVector[p][0];
			currentAverage = 0;		// Se reinicia el promedio para la siguiente muestra
		}
		else{
			thresholdEvent = (currentAverage/AVG_LEN) - currentVector[p][counterEvent[p] + 1];
			currentAverage = 0;		// Se reinicia el promedio para la siguiente muestra
		}
	}

    // Finite State Machine implementation to detect the events, each case is a state in the FSM
	switch(eventState[p]){

	// Primer caso: NE : No Event
	case(NE):{	// NE State, is waiting a threnshold break
		// Se compara el threshold de un apagado
		if(thresholdEvent >= threshold){
			eventState[p] = PC1;			// Se actualiza el valor para pasar a la siguiente fase de preconfirmación
			steadyCurrentEvent[p] 	= currentVector[p][counterEvent[p]] - thresholdEvent;
			steadyShiftEvent[p]		= shiftVectorToSend[p][counterDataToSend[p]];

			// Codigo para debuggear los estados previos
			eventStateInformation[counterEventStateInformation] = eventState[p];
			counterEventStateInformation++;
		}
		// Threshold de un encendido
		else if(thresholdEvent <= thresholdNeg){
			eventState[p] = PC2;			// Se actualiza el valor para pasar a la siguiente fase de preconfirmación
			steadyCurrentEvent[p] 	= currentVector[p][counterEvent[p]] - thresholdEvent;
			steadyShiftEvent[p]		= shiftVectorToSend[p][counterDataToSend[p]];

			// Codigo para debuggear los estados previos
			eventStateInformation[counterEventStateInformation] = eventState[p];
			counterEventStateInformation++;
		}
		// Se actualiza la condición que indica si estamos iniciando un evento, o si ya estamos en uno
		eventCondition[p] = NO_EVENT; 	// Recordar que eventCondition es para saber si comparo con respecto a las 3 siguientes o las 3 anteriores
		break;						// Rompemos el case
	}	// Fin case NE
	// Segundo caso: PC1 : Preconfimation
	case(PC1):{// PC1 state, preconfirmation of start event, if it is confirmed, goes to state BE and search the correct start position of the event
		// Se confirma que se vuelva a detectar el threshold
		if(thresholdEvent >= threshold){
			eventState[p] 			= BE;		// Se actualiza el estado del evento
			preConfirmationState[p] = PC1;		// Se actualiza la bandera que me indica el estado previo de Preconfirmación
			counterStableSamples[p] = 0;		// Se inicia el contador de la cantidad de muestras que son estables (Para restarle al numero de muestras totales)

			// Codigo para debuggear los estados previos
			eventStateInformation[counterEventStateInformation] = eventState[p];
			counterEventStateInformation++;

			// Ya que el evento se encuentra confirmado, se busca analizar los picos para evitar pérdida de la información
			peakPrev[p] = 0.0;

			// Definicion de variables auxiliares para barrer las 20 muestras
			uint8_t counterAux_1 			= 0;
			uint8_t counterAux_2 			= counterEvent[p];
			float accumulated				= 0.0;						// Variable que va a almacenar el promedio
			eventDuration[p]				= NUMBER_OF_EVENTS - 1;		// Duración del evento
			// Se calcula la duracion del evento
			for(uint8_t j = 0; j < (NUMBER_OF_EVENTS - (AVG_LEN + 1)); j++){
				counterAux_2++;							// Aumentamos el contador auxiliar para desplazarnos dentro del arreglo
				if(counterAux_2 >= NUMBER_OF_EVENTS){
					counterAux_2 = 0;					// Reiniciamos el contador si superamos el tamaño del arrego
				}
				counterAux_1 = counterAux_2; 			// Actualizamos el counter previo (counterAux_1)
				for(uint8_t i = 0; i < AVG_LEN; i++){
					counterAux_1 = (counterAux_1 >= NUMBER_OF_EVENTS) ? 0 : counterAux_1 + 1;	// Reiniciamos el contador si supera las 3 muestras
					accumulated += currentVector[p][counterAux_1];								// Sumamos al valor acumulado (Para calcular el promedio)
				}
				if(currentVector[p][counterEvent[p]] - (accumulated / AVG_LEN) < threshold2){
					eventDuration[p] = (NUMBER_OF_EVENTS - j);									// Debería ser J (?
				}
			}

		}
		// Si no se preconfirma, se devuelve al primera estado de la máquina de estados
		else{
			// Se actualiza el estado del evento
			eventState[p] = NE;
			// Codigo para debuggear los estados previos
			eventStateInformation[counterEventStateInformation] = eventState[p];
			counterEventStateInformation++;
		}
		// Se actualiza la condición que indica si estamos iniciando un evento, o si ya estamos en uno
		eventCondition[p] = NO_EVENT;
		break;
	}	// Fin case PC1

	case(BE):{ // BE state, the event confirmed and is waiting the end of it, also, look for the peak.
		// Se busca el pico del evento
		eventPeak[p] = currentVector[p][counterEvent[p]] - steadyCurrentEvent[p];	// Se calcula el delta de corriente con respecto a la primera muestra del evento
		// Se calcula el cambio máximo
		if(preConfirmationState[p] == PC1){
			// Si el nuevo pico es superior al anterior se actualiza (Pico positivo)
			if(eventPeak[p] > peakPrev[p]){
				peakPrev[p] = eventPeak[p];
			}
		}
		else{
			// Si el nuevo pico es inferior al anterior se actualiza (Pico negativo)
			if(eventPeak[p] < peakPrev[p]){
				peakPrev[p] = eventPeak[p];
			}
		}
		// Se evalúa si ya se empezó a estabilizar la muestra
		if(thresholdEvent <= threshold2 && thresholdEvent > thresholdNeg2){
			eventState[p] = PC3;		// Se actualiza el estado del evento

			// Codigo para debuggear los estados previos
			eventStateInformation[counterEventStateInformation] = eventState[p];
			counterEventStateInformation++;
		}
		// Se actualiza la condición que indica si estamos iniciando un evento, o si ya estamos en uno
		eventCondition[p] = EVENT;
		eventDuration[p]++;				// Aumentamos en uno la duración del evento (se pasó un dato más)
		counterStableSamples[p]++;		// Aumentamos en uno el número de muestras estables
		// Si el contador de muestras sobrepasa las 256 muestras permitidas del vector de corriente, se reinicia (El evento es muy largo)
		if(eventDuration[p] == 0xFF){
			eventDuration[p] = 0;		// Se reinicia el contador
			eventState[p] = NE;

			// Codigo para debuggear los estados previos
			eventStateInformation[counterEventStateInformation] = eventState[p];
			counterEventStateInformation++;
		}
		break;
	}	// Fin case BE
	case(PC3):{// PC3 state, check that the event ends correctly
		// Se comprueba que siga estable (el evento ya finalizó)
		if(thresholdEvent <= threshold2 && thresholdEvent > thresholdNeg2){
			eventState[p] = EE;				// Se actualiza el estado del evento
			eventCondition[p] = NO_EVENT;	// Se actualiza la condición que indica si estamos iniciando un evento, o si ya estamos en uno

			// Codigo para debuggear los estados previos
			eventStateInformation[counterEventStateInformation] = eventState[p];
			counterEventStateInformation++;
		}
		else{	// Si aún continuamos en el evento (Aún no es estable)
			eventState[p] = BE;				// Se actualiza el estado del evento
			eventCondition[p] = EVENT;		// Se actualiza la condición que indica si estamos iniciando un evento, o si ya estamos en uno

			// Codigo para debuggear los estados previos
			eventStateInformation[counterEventStateInformation] = eventState[p];
			counterEventStateInformation++;
		}
		break;
	}	// Fin case PC3
	case(PC2):{// PC2 state, is the same that PC1 state, but if down event
		// Se confirma que se vuelva a detectar el threshold
		if(thresholdEvent <= thresholdNeg){
			eventState[p] 			= BE;		// Se actualiza el estado del evento

			// Codigo para debuggear los estados previos
			eventStateInformation[counterEventStateInformation] = eventState[p];
			counterEventStateInformation++;

			preConfirmationState[p] = PC2;		// Se actualiza la bandera que me indica el estado previo de Preconfirmación
			counterStableSamples[p] = 0;		// Se inicia el contador de la cantidad de muestras que son estables (Para restarle al numero de muestras totales)
			// Ya que el evento se encuentra confirmado, se busca analizar los picos para evitar pérdida de la información
			peakPrev[p] = 0.0;

			// Definicion de variables auxiliares para barrer las 20 muestras
			uint8_t counterAux_1 			= 0;
			uint8_t counterAux_2 			= counterEvent[p];
			float accumulated				= 0.0;						// Variable que va a almacenar el promedio
			eventDuration[p]				= NUMBER_OF_EVENTS - 1;		// Duración del evento
			// Se calcula la duracion del evento
			for(uint8_t j = 0; j < (NUMBER_OF_EVENTS - (AVG_LEN + 1)); j++){
				counterAux_2++;							// Aumentamos el contador auxiliar para desplazarnos dentro del arreglo
				if(counterAux_2 >= NUMBER_OF_EVENTS){
					counterAux_2 = 0;					// Reiniciamos el contador si superamos el tamaño del arrego
				}
				counterAux_1 = counterAux_2; 			// Actualizamos el counter previo (counterAux_1)
				for(uint8_t i = 0; i < AVG_LEN; i++){
					counterAux_1 = (counterAux_1 >= NUMBER_OF_EVENTS - 1) ? 0 : counterAux_1 + 1;	// Reiniciamos el contador si supera las 3 muestras
					accumulated += currentVector[p][counterAux_1];								// Sumamos al valor acumulado (Para calcular el promedio)
				}
				if(currentVector[p][counterEvent[p]] - (accumulated / AVG_LEN) < thresholdNeg2){
					eventDuration[p] = (NUMBER_OF_EVENTS - j);									// Debería ser J (?
				}
			}

		}
		// Si no se preconfirma, se devuelve al primera estado de la máquina de estados
		else{
			// Se actualiza el estado del evento
			eventState[p] = NE;

			// Codigo para debuggear los estados previos
			eventStateInformation[counterEventStateInformation] = eventState[p];
			counterEventStateInformation++;
		}
		// Se actualiza la condición que indica si estamos iniciando un evento, o si ya estamos en uno
		eventCondition[p] = NO_EVENT;
		break;
	}	// Fin case PC2
	case(EE):{ // EE state, the event is confirmed and the data would be processed to send.
		eventState[p] = NE;	// Se actualiza el estado del evento al inicio de todos

		// Codigo para debuggear cuando se generan eventos dentro de la máquina de estados
		eventStateInformation[counterEventStateInformation] = eventState[p];
		counterEventStateInformation++;
		counterEventsFullConfirmed++;

		// To delete shorts peaks we use counterStableSamples
		if(counterStableSamples[p] >= NUMBER_OF_EVENTS - 6)
		{
			// Se calcula delta de corriente desde el inicio (t0) hasta el final del evento (tf)
			deltaCurrentEvent[p] 	= (currentVector[p][counterEvent[p]] - thresholdEvent) - steadyCurrentEvent[p];		// Calcule the current delta
			eventDuration[p] 		= eventDuration[p] - NUMBER_OF_EVENTS - 2;							// Calcule the event duration
			if(peakPrev[p] > (float) abs(deltaCurrentEvent[p]) * SCALE_PEAK){							// Check if the peak is considerable and calculate the peak duration
				endCurr	= currentVector[p][counterEvent[p]] - thresholdEvent;							// Se calcula el último valor de corriente quitándole el delta
				// Se busca recorrer el arreglo de datos para enviar, con la finalidad de comparar el pico con respecto a los otros valores
				for(uint8_t i = eventDuration[p]; i < 255; i++){
					tempStartCurr = 0.0;												// Se inicializa la variable auxiliar para la corriente
					for(uint8_t j = 0; j < 3; j++){
						uint8_t pos 	 = (uint8_t) (counterDataToSend[p] - i + j);	// Se recorre el arreglo de datos para enviar
						tempStartCurr	+= currentVectorToSend[p][pos];
					}
					tempStartCurr /= 3;													// Se calcula el promedio cada 3 muestras
					if(tempStartCurr < endCurr){										// Se recalcula el delta del pico, para que no varie tanto entre eventos

						startPeakPosition[p] = 256 + counterDataToSend[p] - i;			// Se calcula la posición neta del arreglo (recordar que i es el desplazamiento dentro del arreglo)
						peakPrev[p] = 0.0;												// Se inicia en 0 para volver a calcular el pico
						for(uint8_t n = 0; n < i; n++){									// Comparo con respecto a los índices del arreglo de datos para enviar
							peakPrev[p] += currentVectorToSend[p][startPeakPosition[p] + n];	// Llego hasta el índice de counterDataToSend
						}
						peakPrev[p] = (2.0 * peakPrev[p]) / (float) i;					// Se "amplifica" ligeramente el pico
                        /**
                         * Aca va la logica para enviar todo antes del inicio del pico
                         * Se calcula la posicición del inicio del pico, hay que tener cuidado porque de 255 pasa a 0
                         * Se pasa al otro buffer todo lo que haya hasta la posición de inicio del pico
                         * Lo que este despues de esa poscicion se lee pero no se pasa, solo se retorna el item para sacarla del buffer
                         */

						while(1)
						{
							// Se busca pasar el evento del ringbuffer al ringbufferToSend
							itemSize = 0;
							// Leemos el dato del ringBuffer
							itemSize = TM_BUFFER_Read(&ringBuffersHandle[p],itemBuff,1);
							// Si hay elementos en el ringBuffer
							if(itemSize != 0){
								// Devolvemos el dato al ring buffer
								TM_BUFFER_WriteToTop(&ringBuffersHandle[p],itemBuff,1);
								// Creamos una copia del dato recién leído
								auxCounter[p] = *itemBuff;									// Revisarrrrrrrrrrrrrrrrrrrr

								if(startPeakPosition[p] >= counterDataToSend[p])
								{
									// Check if the events in buffer are outside peak duration
									if((auxCounter[p] > counterDataToSend[p]) && (auxCounter[p] <= startPeakPosition[p]))
									{
										// Cargamos el dato en el segundo ringBuffer (Buffer_ToSend)
										TM_BUFFER_Write(&ringBuffersHandle_ToSend[p],itemBuff,1);
									}

								}
								else
								{
									// Check if the events in buffer are outside peak duration
									if((auxCounter[p] > counterDataToSend[p]) || (auxCounter[p] <= startPeakPosition[p]))
									{
										// Cargamos el dato en el segundo ringBuffer (Buffer_ToSend)
										TM_BUFFER_Write(&ringBuffersHandle_ToSend[p],itemBuff,1);
									}
								}
							}
							// Si no hay elementos en el ringBuffer
							else
							{
								for(int k = 0; k < 256; k++){
									// Se llena la matriz booleana
									areEvents[p][k] = false;
								}
								break;
							}
						}	// Fin ciclo While

						//	Agregamos los datos para enviar
						if(steadyCurrentEvent[p] >= tempStartCurr)// Add the peak to send buffer, but check the correct steady
						{
							// Función para el envío de los datos
							addToBufferState = addToBuffer(&ringBuffersHandle_ToSend[p], counterDataToSend[p], ts, counterTimeStamp[p], TYPE_PEAK, i, peakPrev[p], tempStartCurr, voltageVectorToSend[p][counterDataToSend[p]], shiftVectorToSend[p][counterDataToSend[p]] - steadyShiftEvent[p]);
							if(addToBufferState != SUCCESS)
							{
								Error_Handler();
							}
						}
						else
						{
							// Función para el envío de los datos
							addToBufferState = addToBuffer(&ringBuffersHandle_ToSend[p], counterDataToSend[p], ts, counterTimeStamp[p], TYPE_PEAK, i, peakPrev[p], steadyCurrentEvent[p], voltageVectorToSend[p][counterDataToSend[p]], shiftVectorToSend[p][counterDataToSend[p]] - steadyShiftEvent[p]);
							if(addToBufferState != SUCCESS)
							{
								Error_Handler();
							}
						}
                        /**
                         * Se valida si el evento actual es evento delta para pasarlo al buffer y que no pase una ronda completa antes de moverlo
                         *
                         */
						// Check if the peak ocurr with an event at the same time and pass it to the send buffer
						if(deltaCurrentEvent[p] >= threshold)		// Short Event Peak
						{
							// Función para el envío de los datos
							addToBufferState = addToBuffer(&ringBuffersHandle_ToSend[p], counterDataToSend[p], ts, counterTimeStamp[p], TYPE_EVENT, eventDuration[p], deltaCurrentEvent[p], steadyCurrentEvent[p], voltageVectorToSend[p][counterDataToSend[p]], shiftVectorToSend[p][counterDataToSend[p]] - steadyShiftEvent[p]);
							if(addToBufferState != SUCCESS)
							{
								Error_Handler();
							}
						}
						break;
					} 	// Fin recalculación del delta del pico

				}	// Fin iteración para recorrer el bufferDataToSend
			}	// End  checking if the preak is considerable

			else if(deltaCurrentEvent[p] >= threshold || deltaCurrentEvent[p] < thresholdNeg)	// Regular event without peak
			{
				// Función para el envío de los datos: LONG EVENT
				addToBufferState = addToBuffer(&ringBuffersHandle_ToSend[p], counterDataToSend[p], ts, counterTimeStamp[p], TYPE_EVENT, eventDuration[p], deltaCurrentEvent[p], steadyCurrentEvent[p], voltageVectorToSend[p][counterDataToSend[p]], shiftVectorToSend[p][counterDataToSend[p]] - steadyShiftEvent[p]);
				if(addToBufferState == SUCCESS)
				{
					areEvents[p][counterDataToSend[p]] = true;
				}
				else
				{
					Error_Handler();
				}
			}
		}	// End delete shorts peaks
		/* Código SOLAMENTE PARA PROBAR CON IMPULSOS UNITARIOS - COMENTAR PARA SU CORRECTO FUNCIONAMIENTO*/
//		else{
//			// Función para el envío de los datos: LONG EVENT
//			addToBufferState = addToBuffer(&ringBuffersHandle_ToSend[p], counterDataToSend[p], ts, counterTimeStamp[p], TYPE_EVENT, eventDuration[p], deltaCurrentEvent[p], steadyCurrentEvent[p], voltageVectorToSend[p][counterDataToSend[p]], shiftVectorToSend[p][counterDataToSend[p]] - steadyShiftEvent[p]);
//			if(addToBufferState == SUCCESS)
//			{
//				areEvents[p][counterDataToSend[p]] = true;
//			}
//			else
//			{
//				Error_Handler();
//			}
//		}
//		/* Código SOLAMENTE PARA PROBAR CON IMPULSOS UNITARIOS */
		break;
	}	// Fin case EE2
	default:{break;}
	}	// End Switch Finite State Machine
	// Aumentamos los contadores de ambas ventanas (la de 20 muestras, y la de 256)
	counterEvent[p]++;
	counterDataToSend[p]++;
}	// Fin ProcessData

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  if(IS_ENGINEERING_BOOT_MODE())
  {
    /* Configure the system clock */
    SystemClock_Config();
  }

  if(IS_ENGINEERING_BOOT_MODE())
  {
    /* Configure the peripherals common clocks */
    PeriphCommonClock_Config();
  }
  else
  {
    /* IPCC initialisation */
    MX_IPCC_Init();
    /* OpenAmp initialisation ---------------------------------*/
    MX_OPENAMP_Init(RPMSG_REMOTE, NULL);
  }

  /* USER CODE BEGIN SysInit */
  // Para cuando se esté en modo ingeniería y se quiera que funcione el IPCC
  /*---------------------------------------------------------*/
//  /*HW semaphore Clock enable*/
//  __HAL_RCC_HSEM_CLK_ENABLE();
//  /* IPCC initialisation */
//  MX_IPCC_Init();
//  /* OpenAmp initialisation */
//  MX_OPENAMP_Init(RPMSG_REMOTE, NULL);
  /*---------------------------------------------------------*/
  /* FPU settings ------------------------------------------------------------*/
//  #if (__FPU_PRESENT == 1) && (__FPU_USED == 1)
//    SCB->CPACR |= (0xF << 20);  /* set CP10 and CP11 Full Access */
//  #endif

  DMA_Init_Full(); // Inicialización de la DMA
  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_TIM2_Init();
  MX_ADC1_Init();
  MX_UART7_Init();
  MX_TIM5_Init();
  /* USER CODE BEGIN 2 */

  /* Inicializacion de los LEDS*/
	GPIO_InitTypeDef LED7;
	LED7.Pin = GPIO_PIN_7;
	LED7.Mode = GPIO_MODE_OUTPUT_PP;
	LED7.Pull = GPIO_PULLUP;
	LED7.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	HAL_GPIO_Init(GPIOH, &LED7);

//	GPIO_InitTypeDef LED5;
//	LED5.Pin = GPIO_PIN_14;
//	LED5.Mode = GPIO_MODE_OUTPUT_PP;
//	LED5.Pull = GPIO_PULLUP;
//	LED5.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
//	HAL_GPIO_Init(GPIOA, &LED5);

	GPIO_InitTypeDef LED6;
	LED6.Pin = GPIO_PIN_13;
	LED6.Mode = GPIO_MODE_OUTPUT_PP;
	LED6.Pull = GPIO_PULLUP;
	LED6.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	HAL_GPIO_Init(GPIOA, &LED6);

  /* virtUART0 and virtUART1 initialization and cb association*/
	if (VIRT_UART_Init(&virtUART0) != VIRT_UART_OK)
	{
	  Error_Handler();
	}
	if (VIRT_UART_Init(&virtUART1) != VIRT_UART_OK)
	{
	  Error_Handler();
	}
	/* Callback del USART VIRTUAL */
	if (VIRT_UART_RegisterCallback(&virtUART0, VIRT_UART_RXCPLT_CB_ID, virt_UART0_cb0) != VIRT_UART_OK)
	{
	  Error_Handler();
	}
	if (VIRT_UART_RegisterCallback(&virtUART1, VIRT_UART_RXCPLT_CB_ID, virt_UART1_cb0) != VIRT_UART_OK)
	{
	  Error_Handler();
	}
	// Inicialización de la DMA
	if(HAL_ADC_Start_DMA(&hadc1,(uint32_t*)adc_buf,ADC_BUF_LEN)!=HAL_OK)
	{
		  Error_Handler();
	}
	/* Inicialización del timer 2 */
	if (HAL_TIM_Base_Start(&htim2) != HAL_OK)
	{
		Error_Handler();
	}
	/* Inicialización del timer 5 */
	if (HAL_TIM_Base_Start_IT(&htim5) != HAL_OK)
	{
		Error_Handler();
	}

	// Inicializamos el numero de fases
	setUpDetector();
	// Inicializamos los xRing buffers to simulate Queues
	initQueues();
	  // Se inicializa la FFT
//	  if(arm_rfft_fast_init_f32(&handler_Rfft,TEST_LENGTH_SAMPLES)!= ARM_MATH_SUCCESS){
//		  Error_Handler();
//	  }


//	arm_rfft_init_q15(&handlerRfft, TEST_LENGTH_SAMPLES, ifftFlag, doBitReverse);


  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
	  // Funcion que permite detectar interrupciones de recepcion en los puertos seriales virtuales
	  OPENAMP_check_for_message();
	  // Funciones encargada de ejecutar la maquina de estados
	  vTaskProcessData();
	  // Función que envía los eventos generados en formato hexadecimal
	  sendEvent();



    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure LSE Drive Capability
  */
  HAL_PWR_EnableBkUpAccess();
  __HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_MEDIUMHIGH);

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_CSI|RCC_OSCILLATORTYPE_HSI
                              |RCC_OSCILLATORTYPE_HSE|RCC_OSCILLATORTYPE_LSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_BYPASS_DIG;
  RCC_OscInitStruct.LSEState = RCC_LSE_ON;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSIDivValue = RCC_HSI_DIV1;
  RCC_OscInitStruct.CSIState = RCC_CSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  RCC_OscInitStruct.PLL2.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL2.PLLSource = RCC_PLL12SOURCE_HSE;
  RCC_OscInitStruct.PLL2.PLLM = 3;
  RCC_OscInitStruct.PLL2.PLLN = 66;
  RCC_OscInitStruct.PLL2.PLLP = 2;
  RCC_OscInitStruct.PLL2.PLLQ = 1;
  RCC_OscInitStruct.PLL2.PLLR = 1;
  RCC_OscInitStruct.PLL2.PLLFRACV = 0x1400;
  RCC_OscInitStruct.PLL2.PLLMODE = RCC_PLL_FRACTIONAL;
  RCC_OscInitStruct.PLL3.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL3.PLLSource = RCC_PLL3SOURCE_HSE;
  RCC_OscInitStruct.PLL3.PLLM = 2;
  RCC_OscInitStruct.PLL3.PLLN = 34;
  RCC_OscInitStruct.PLL3.PLLP = 2;
  RCC_OscInitStruct.PLL3.PLLQ = 17;
  RCC_OscInitStruct.PLL3.PLLR = 37;
  RCC_OscInitStruct.PLL3.PLLRGE = RCC_PLL3IFRANGE_1;
  RCC_OscInitStruct.PLL3.PLLFRACV = 6660;
  RCC_OscInitStruct.PLL3.PLLMODE = RCC_PLL_FRACTIONAL;
  RCC_OscInitStruct.PLL4.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL4.PLLSource = RCC_PLL4SOURCE_HSE;
  RCC_OscInitStruct.PLL4.PLLM = 4;
  RCC_OscInitStruct.PLL4.PLLN = 99;
  RCC_OscInitStruct.PLL4.PLLP = 6;
  RCC_OscInitStruct.PLL4.PLLQ = 8;
  RCC_OscInitStruct.PLL4.PLLR = 8;
  RCC_OscInitStruct.PLL4.PLLRGE = RCC_PLL4IFRANGE_0;
  RCC_OscInitStruct.PLL4.PLLFRACV = 0;
  RCC_OscInitStruct.PLL4.PLLMODE = RCC_PLL_INTEGER;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** RCC Clock Config
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_ACLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2
                              |RCC_CLOCKTYPE_PCLK3|RCC_CLOCKTYPE_PCLK4
                              |RCC_CLOCKTYPE_PCLK5;
  RCC_ClkInitStruct.AXISSInit.AXI_Clock = RCC_AXISSOURCE_PLL2;
  RCC_ClkInitStruct.AXISSInit.AXI_Div = RCC_AXI_DIV1;
  RCC_ClkInitStruct.MCUInit.MCU_Clock = RCC_MCUSSOURCE_PLL3;
  RCC_ClkInitStruct.MCUInit.MCU_Div = RCC_MCU_DIV1;
  RCC_ClkInitStruct.APB4_Div = RCC_APB4_DIV2;
  RCC_ClkInitStruct.APB5_Div = RCC_APB5_DIV4;
  RCC_ClkInitStruct.APB1_Div = RCC_APB1_DIV2;
  RCC_ClkInitStruct.APB2_Div = RCC_APB2_DIV2;
  RCC_ClkInitStruct.APB3_Div = RCC_APB3_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Set the HSE division factor for RTC clock
  */
  __HAL_RCC_RTC_HSEDIV(24);
}

/**
  * @brief Peripherals Common Clock Configuration
  * @retval None
  */
void PeriphCommonClock_Config(void)
{
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the common periph clock
  */
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_CKPER;
  PeriphClkInit.CkperClockSelection = RCC_CKPERCLKSOURCE_HSE;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_MultiModeTypeDef multimode = {0};
  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */
	// Se duplica la configuración para permitir generar el deInit() del ADC a la hora de volver a cargar el codigo
	hadc1.DMA_Handle = &hdma_adc1;
	hadc1.Instance = ADC1;
	hadc1.Init.ClockPrescaler = ADC_CLOCK_ASYNC_DIV1;
	hadc1.Init.Resolution = ADC_RESOLUTION_12B;
	hadc1.Init.ScanConvMode = ADC_SCAN_ENABLE;
	hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
	hadc1.Init.LowPowerAutoWait = DISABLE;
	hadc1.Init.ContinuousConvMode = DISABLE;
	hadc1.Init.NbrOfConversion = 4;
	hadc1.Init.DiscontinuousConvMode = DISABLE;
	hadc1.Init.ExternalTrigConv = ADC_EXTERNALTRIG_T2_TRGO;
	hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_RISING;
	hadc1.Init.ConversionDataManagement = ADC_CONVERSIONDATA_DMA_CIRCULAR;
	hadc1.Init.Overrun = ADC_OVR_DATA_PRESERVED;
	hadc1.Init.LeftBitShift = ADC_LEFTBITSHIFT_NONE;
	hadc1.Init.OversamplingMode = DISABLE;
	if (HAL_ADC_DeInit(&hadc1) != HAL_OK)
	{
	Error_Handler();
	}
  /* USER CODE END ADC1_Init 1 */

  /** Common config
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_ASYNC_DIV1;
  hadc1.Init.Resolution = ADC_RESOLUTION_12B;
  hadc1.Init.ScanConvMode = ADC_SCAN_ENABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  hadc1.Init.LowPowerAutoWait = DISABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.NbrOfConversion = 4;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConv = ADC_EXTERNALTRIG_T2_TRGO;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_RISING;
  hadc1.Init.ConversionDataManagement = ADC_CONVERSIONDATA_DMA_CIRCULAR;
  hadc1.Init.Overrun = ADC_OVR_DATA_PRESERVED;
  hadc1.Init.LeftBitShift = ADC_LEFTBITSHIFT_NONE;
  hadc1.Init.OversamplingMode = DISABLE;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure the ADC multi-mode
  */
  multimode.Mode = ADC_MODE_INDEPENDENT;
  if (HAL_ADCEx_MultiModeConfigChannel(&hadc1, &multimode) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure Regular Channel
  */
  sConfig.Channel = ADC_CHANNEL_0;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLETIME_1CYCLE_5;
  sConfig.SingleDiff = ADC_SINGLE_ENDED;
  sConfig.OffsetNumber = ADC_OFFSET_NONE;
  sConfig.Offset = 0;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure Regular Channel
  */
  sConfig.Channel = ADC_CHANNEL_1;
  sConfig.Rank = ADC_REGULAR_RANK_2;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure Regular Channel
  */
  sConfig.Channel = ADC_CHANNEL_13;
  sConfig.Rank = ADC_REGULAR_RANK_3;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure Regular Channel
  */
  sConfig.Channel = ADC_CHANNEL_6;
  sConfig.Rank = ADC_REGULAR_RANK_4;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC1_Init 2 */

  /* USER CODE END ADC1_Init 2 */

}

/**
  * @brief IPCC Initialization Function
  * @param None
  * @retval None
  */
static void MX_IPCC_Init(void)
{

  /* USER CODE BEGIN IPCC_Init 0 */

  /* USER CODE END IPCC_Init 0 */

  /* USER CODE BEGIN IPCC_Init 1 */

  /* USER CODE END IPCC_Init 1 */
  hipcc.Instance = IPCC;
  if (HAL_IPCC_Init(&hipcc) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN IPCC_Init 2 */

  /* USER CODE END IPCC_Init 2 */

}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 208;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 130;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_UPDATE;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */

  /* USER CODE END TIM2_Init 2 */

}

/**
  * @brief TIM5 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM5_Init(void)
{

  /* USER CODE BEGIN TIM5_Init 0 */

  /* USER CODE END TIM5_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM5_Init 1 */

  /* USER CODE END TIM5_Init 1 */
  htim5.Instance = TIM5;
  htim5.Init.Prescaler = 20888;
  htim5.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim5.Init.Period = 2500;//10000;
  htim5.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim5.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim5) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim5, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim5, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM5_Init 2 */
  HAL_TIM_Base_MspInit(&htim5);
  /* USER CODE END TIM5_Init 2 */

}

/**
  * @brief UART7 Initialization Function
  * @param None
  * @retval None
  */
static void MX_UART7_Init(void)
{

  /* USER CODE BEGIN UART7_Init 0 */

  /* USER CODE END UART7_Init 0 */

  /* USER CODE BEGIN UART7_Init 1 */

	// Importante dejarlo sin parity check (ParityNone)
	huart7.Instance = UART7;
	huart7.Init.BaudRate = 115200;
	huart7.Init.WordLength = UART_WORDLENGTH_8B;
	huart7.Init.StopBits = UART_STOPBITS_1;
	huart7.Init.Parity = UART_PARITY_NONE;
	huart7.Init.Mode = UART_MODE_TX_RX;
	huart7.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	huart7.Init.OverSampling = UART_OVERSAMPLING_16;
	huart7.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
	huart7.Init.ClockPrescaler = UART_PRESCALER_DIV1;
	huart7.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
	// Hay que agregar la configuración de los pines a través de la siguiente función:
	HAL_UART_MspInit(&huart7);

  /* USER CODE END UART7_Init 1 */
  huart7.Instance = UART7;
  huart7.Init.BaudRate = 115200;
  huart7.Init.WordLength = UART_WORDLENGTH_8B;
  huart7.Init.StopBits = UART_STOPBITS_1;
  huart7.Init.Parity = UART_PARITY_NONE;
  huart7.Init.Mode = UART_MODE_TX_RX;
  huart7.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart7.Init.OverSampling = UART_OVERSAMPLING_16;
  huart7.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart7.Init.ClockPrescaler = UART_PRESCALER_DIV1;
  huart7.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart7) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_SetTxFifoThreshold(&huart7, UART_TXFIFO_THRESHOLD_1_8) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_SetRxFifoThreshold(&huart7, UART_RXFIFO_THRESHOLD_1_8) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_DisableFifoMode(&huart7) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN UART7_Init 2 */

  /* USER CODE END UART7_Init 2 */

}

/**
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void)
{

  /* DMA controller clock enable */
  __HAL_RCC_DMAMUX_CLK_ENABLE();
  __HAL_RCC_DMA2_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA2_Stream0_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA2_Stream0_IRQn, 1, 0);
  HAL_NVIC_EnableIRQ(DMA2_Stream0_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOE_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_1, GPIO_PIN_RESET);

  /*Configure GPIO pin : PD1 */
  GPIO_InitStruct.Pin = GPIO_PIN_1;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pin : PA14 */
  GPIO_InitStruct.Pin = GPIO_PIN_14;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : USB_PWR_CC2_Pin USB_PWR_CC1_Pin */
  GPIO_InitStruct.Pin = USB_PWR_CC2_Pin|USB_PWR_CC1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */
/* Funcion para inicializar la DMA con toda la configuración*/
/**
 * @brief DMA Initialization Function. It is necesary to used every time the firmware has a restart. That is because the DMA don´t stop, even if
 * the CortexM4 has already stopped
 */
void DMA_Init_Full(void)
{

	// Se duplica la configuración para permitir generar el deInit() del ADC a la hora de volver a cargar el codigo
	hadc1.DMA_Handle = &hdma_adc1;
	hadc1.Instance = ADC1;
	hadc1.Init.ClockPrescaler = ADC_CLOCK_ASYNC_DIV1;
	hadc1.Init.Resolution = ADC_RESOLUTION_12B;
	hadc1.Init.ScanConvMode = ADC_SCAN_ENABLE;
	hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
	hadc1.Init.LowPowerAutoWait = DISABLE;
	hadc1.Init.ContinuousConvMode = DISABLE;
	hadc1.Init.NbrOfConversion = 4;
	hadc1.Init.DiscontinuousConvMode = DISABLE;
	hadc1.Init.ExternalTrigConv = ADC_EXTERNALTRIG_T2_TRGO;
	hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_RISING;
	hadc1.Init.ConversionDataManagement = ADC_CONVERSIONDATA_DMA_CIRCULAR;
	hadc1.Init.Overrun = ADC_OVR_DATA_PRESERVED;
	hadc1.Init.LeftBitShift = ADC_LEFTBITSHIFT_NONE;
	hadc1.Init.OversamplingMode = DISABLE;

	/* DMA controller clock enable */
	__HAL_RCC_DMAMUX_CLK_ENABLE();
	__HAL_RCC_DMA2_CLK_ENABLE();

	// Se genera un handler de la DMA
	hdma_adc1.Instance = DMA2_Stream0;
	hdma_adc1.Init.Request = DMA_REQUEST_ADC1;
	hdma_adc1.Init.Direction = DMA_PERIPH_TO_MEMORY;
	hdma_adc1.Init.PeriphInc = DMA_PINC_DISABLE;
	hdma_adc1.Init.MemInc = DMA_MINC_ENABLE;
	hdma_adc1.Init.PeriphDataAlignment = DMA_PDATAALIGN_HALFWORD;
	hdma_adc1.Init.MemDataAlignment = DMA_MDATAALIGN_HALFWORD;
	hdma_adc1.Init.Mode = DMA_CIRCULAR;
	hdma_adc1.Init.Priority = DMA_PRIORITY_LOW;
	hdma_adc1.Init.FIFOMode = DMA_FIFOMODE_DISABLE;

	// Se apaga la DMA
	if (HAL_ADC_Stop_DMA(&hadc1) != HAL_OK)
	{
	Error_Handler();
	}

	// Se desinicializa la DMA
	if (HAL_DMA_DeInit(&hdma_adc1) != HAL_OK)
	{
	  Error_Handler();
	}
	// Se inicializa la DMA
	if (HAL_DMA_Init(&hdma_adc1) != HAL_OK)
	{
	  Error_Handler();
	}

	// Se linkea la DMA con el ADC para que queden sincronizados
	 __HAL_LINKDMA(&hadc1,DMA_Handle,hdma_adc1);

	/* DMA interrupt init */
	/* DMA2_Stream0_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(DMA2_Stream0_IRQn, 5, 0);
	HAL_NVIC_EnableIRQ(DMA2_Stream0_IRQn);
}

/**
 * @brief Funcion de inicialización de los xRing buffers para simular la xQueues
 * */
void initQueues(void){
	// QueueA
	xQueueA_.Size = sizeof(samplesPerPhase) * 50;
	TM_BUFFER_Init(&xQueueA_, xQueueA_.Size, NULL);
	// QueueB
	xQueueB_.Size = sizeof(samplesPerPhase) * 50;
	TM_BUFFER_Init(&xQueueB_, xQueueB_.Size, NULL);
	// QueueC
	xQueueC_.Size = sizeof(samplesPerPhase) * 50;
	TM_BUFFER_Init(&xQueueC_, xQueueC_.Size, NULL);
}

/**
 * @brief Funcion de inicialización de la estructura inicial
 * */
void setUpDetector(void){
	// Se inicializa la estructura de configuración
	config.disagPhases 	= First_Phase;
	config.threshold	= 2000;			// Dado en unidades del ADC -> 12 CT100A
	config.lsbCurrent 	= 5/4095;
	config.lsbVoltage 	= 110/4095;
	config.ctCurrent	= 50.0;


	// Inicializacion de variables globales
	threshold 		= config.threshold;       // Se carga por valor el treshold establecido en la estructura de configuración
	thresholdNeg 	= (threshold) * -1.0;     // Se genera el treshold negativo
	threshold2 		= threshold * 0.75;       // Se genera el segundo para evaluar si ya se estabilizó la señal
	thresholdNeg2 	= threshold2 * -1.0;      // Se genera el segundo treshold negativo

	ctCurrentNeg = config.ctCurrent*(-1.0);	  // Asignación del valor negativo configurado dentro del CTCurrent

	// Creación de los ring buffers de acuerdo a la cantidad de fases
	for(uint8_t i = 0; i <= config.disagPhases; i++){
		// Creación del buffer circular con un tamaño de MAX_EVENTS_BUFFER*24 BYTES
		TM_BUFFER_t ringBufferCreate = {0};										// Instanciamos una estructura
		ringBufferCreate.Size = (MAX_EVENTS_BUFFER * 24);						// Asignamos el tamaño máximo
		ringBuffersHandle[i] = ringBufferCreate;								// Guardamos esa estructura en el arreglo global de handlers
		TM_BUFFER_Init(&ringBuffersHandle[i],ringBuffersHandle[i].Size,NULL);	// Inicializamos el ringBuffer para los datos

		// Creación del ring buffer para el envío de los datos con un tamaño de MAX_EVENTS_BUFFER_TO_SEND*24 Bytes
		TM_BUFFER_t ringBufferCreateToSend = {0};											// Instanciamos una estructura
		ringBufferCreateToSend.Size = (MAX_EVENTS_BUFFER_TO_SEND * 24);						// Asignamos el tamaño máximo
		ringBuffersHandle_ToSend[i] = ringBufferCreateToSend;								// Guardamos esa estructura en el arreglo global de handlers
		TM_BUFFER_Init(&ringBuffersHandle_ToSend[i],ringBuffersHandle_ToSend[i].Size,NULL);	// Inicializamos el ringBuffer para los datos
	}
}


/**
 * @brief Funcion encargada de preparar (codificar los datos) y cargarlos al ring buffer 2
 * @param buff Ring buffer that contains the data to send
 * @param counter Contador
 * @param TS TimeStamp
 * @param countTS contador del TimeStamp
 */
uint8_t addToBuffer(TM_BUFFER_t *buff, uint8_t counter, uint32_t TS, uint8_t countTS, bool type, uint8_t eventTime, float delta, float steady, float vol, uint8_t shift)
{
	// Se comienza a realizar la máscara de los datos
    eventToBuff[0] = counter;
    eventToBuff[1] = TS >> 24;							// MSByte
    eventToBuff[2] = (TS >> 16) & 0xFF;
    eventToBuff[3] = (TS >> 8) & 0xFF;
    eventToBuff[4] = (TS)&0xFF;							// LSByte
    eventToBuff[5] = (type << 7) | ((countTS)&0x7F); 	// pico/event & timestamp
    eventToBuff[6] = eventTime;                      	// Event time

    // Delta de corriente
    tempMap16 = mapb16(delta, ctCurrentNeg, config.ctCurrent);
    eventToBuff[7] = tempMap16 >> 8;
    eventToBuff[8] = tempMap16 & (0xFF);
    // Cantidad de muestras estables
    tempMap16 = mapb16(steady, 0, config.ctCurrent);
    eventToBuff[9] = tempMap16 >> 8;
    eventToBuff[10] = tempMap16 & 0xFF;
    // Valor de voltaje
    tempMap16 = mapb16(vol, 0, MAX_VOLTAGE);
    eventToBuff[11] = tempMap16 >> 8;
    eventToBuff[12] = tempMap16 & 0xFF;
    eventToBuff[13] = shift;

    // Escribimos el dato en el ringBuffer
    if(TM_BUFFER_Write(buff,(uint8_t *)eventToBuff,14) != 0){
    	return SUCCESS;
    }
    else{
    	return ERROR;
    }
//    taskYIELD();
}

uint16_t mapb16(float Value, float in_min, float in_max)
{
    return (Value - in_min) / (in_max - in_min) * pow(2, 16);
}

void vTaskProcessData(void)
{
  /* USER CODE BEGIN vTaskProcessData */

	samplesPerPhase recData;            // Estructura que almacenará el iésimo dato obtenido de la cola

		// Se analiza cada cola con respecto a cada fase (QueueA para la Fase 1, y de igual forma el resto respectivamente)
	switch(config.disagPhases){

	// Caso para la tercera fase
	case(Third_Phase):{
		// Se verifica si hay algún elemento en la Queue
		if(TM_BUFFER_Read(&xQueueC_, &recData, 10) == 10)
		{
			// Se verifica si quedan menos de 5 espacios disponibles en la cola para liberar espacio
			if((TM_BUFFER_GetFree(&xQueueC_)/10) <= 5)
			{
				// Mandamos un mensaje por consola para indicar que la colaX se llenó.
				char bufferUsart[64]={0};
				sprintf(bufferUsart,"Queue C is Full: %u \n",(unsigned int)
						(TM_BUFFER_GetFree(&xQueueC_)/10));
				VIRT_UART_Transmit(&virtUART0, bufferUsart, 64);
				// Se reinicia la cola para evitar un reinicio del sistema.
				TM_BUFFER_Free(&xQueueC_);
			}

			// Si hay espacio en la cola, y además hay un mensaje se llama la función de procesamiento
			processData(recData, Third_Phase);
		}
		} // Fin case 1 // @suppress("No break at end of case")
		/* no break */
	// Caso para la segunda fase
	case(Second_Phase):{
		// Se verifica si hay algún elemento en la Queue
		if(TM_BUFFER_Read(&xQueueB_,&recData,10) == 10)
		{
			// Se verifica si quedan menos de 5 espacios disponibles en la cola para liberar espacio
			if((TM_BUFFER_GetFree(&xQueueB_)/10) <= 5)
			{
				// Mandamos un mensaje por consola para indicar que la colaX se llenó.
				char bufferUsart[64]={0};
				sprintf(bufferUsart,"Queue B is Full: %u \n",(unsigned int)
						(TM_BUFFER_GetFree(&xQueueB_)/10));
				VIRT_UART_Transmit(&virtUART0, bufferUsart, 64);
				// Se reinicia la cola para evitar un reinicio del sistema.
				TM_BUFFER_Free(&xQueueB_);
			}
			// Si hay espacio en la cola, y además hay un mensaje se llama la función de procesamiento
			processData(recData, Second_Phase);
		}
		} // Fin case 2 // @suppress("No break at end of case")
		/* no break */
	// Caso para la primera fase
	case(First_Phase):{
		// Se verifica si hay algún elemento en la Queue
		if(TM_BUFFER_Read(&xQueueA_,&recData,10) == 10)
		{
			// Se verifica si quedan menos de 5 espacios disponibles en la cola para liberar espacio
			if((TM_BUFFER_GetFree(&xQueueA_)/10) <= 5)
			{
				// Mandamos un mensaje por consola para indicar que la colaX se llenó.
				char bufferUsart[64]={0};
				sprintf(bufferUsart,"Queue A is Full: %u \n",(unsigned int)
						(TM_BUFFER_GetFree(&xQueueA_)/10));
				VIRT_UART_Transmit(&virtUART0, bufferUsart, 64);
				// Se reinicia la cola para evitar un reinicio del sistema.
				TM_BUFFER_Free(&xQueueA_);
			}
			// Si hay espacio en la cola, y además hay un mensaje se llama la función de procesamiento
			processData(recData, First_Phase);
		}
		break;	// Rompemos el SwitchCase
		} 		// Fin case 1
	default:{
		// Imprimimos un mensaje de error
		char bufferUsart[64]={0};
		sprintf(bufferUsart,"[ED] Invalid disag phases \n");
		VIRT_UART_Transmit(&virtUART0, bufferUsart, 64);
		break;
	}

	} // Fin SwitchCase
  /* USER CODE END vTaskProcessData */
}

/**
 * @brief Funcion para enviar los datos a través del IPCC
 *
 */
uint32_t tsSend;
uint32_t tsEvent;

void sendEvent(void){

	// Se itera para las tres fases
	for(uint8_t p = 0; p <= config.disagPhases; p++)
	{
		// Se lee el dato desde el segundo ringBuffer

		itemSizeToSend = TM_BUFFER_Read(&ringBuffersHandle_ToSend[p],itemBuffToSend,14);
		if(itemSizeToSend != 0)		// Si se sacó un dato
		{
			// Se convierten los datos a hexadecimal para ser enviados por el IPCC:

			// Calcular la longitud total de los datos que deseas concatenar
			size_t _totalLength = (sizeof(uint8_t) * itemSizeToSend) * 2 + 1;
		    // Se crea un buffer para almacenar los datos concatenados
		    char _bufferVirtualUsart0[_totalLength]; // *2 para los caracteres hexadecimales, +1 para el nulo '\0'

		    for (size_t i = 0; i < itemSizeToSend; i++)
		    {
		        sprintf(&_bufferVirtualUsart0[i * 2], "%02X", *(itemBuffToSend+i)); // Convierte cada byte a formato hexadecimal
		    }

		    // Se transmiten los datos haciendo uso del IPCC
			virtualUsartState = VIRT_UART_Transmit(&virtUART1, _bufferVirtualUsart0, _totalLength);
			virtualUsartState = VIRT_UART_Transmit(&virtUART1, "\n", 2);
		}
	}

//	/***** Codigo para enviar los datos usando la interrupcion del TIMER -> REVISAR *****/
//	// Se reinician los contadores
//	counterTimeStamp[0] = 0;
//	counterTimeStamp[1] = 0;
//	counterTimeStamp[2] = 0;
//
//	// Se itera para las tres fases
//	for(uint8_t p = 0; p < config.disagPhases; p++)
//	{
//        tsSend = 0;
//        counterBufferEvents = 0;
//        while(1)
//        {
//        	itemSizeToSend = 0;
//        	// Se lee el dato desde el segundo ringBuffer
//        	itemSizeToSend = TM_BUFFER_Read(&ringBuffersHandle_ToSend[p],itemBuffToSend,14);
//        	if(itemSizeToSend != 0)		// Si se sacó un dato
//        	{
//    			// Devolvemos el dato al ring buffer
//    			TM_BUFFER_WriteToTop(&ringBuffersHandle_ToSend[p],itemBuffToSend,itemSizeToSend);
//    			tsEvent = (itemBuffToSend[1] << 24) | (itemBuffToSend[2] << 16) | (itemBuffToSend[3] << 8) | (itemBuffToSend[4]);
//    			if(tsSend != tsEvent)
//    			{
//                    if (counterBufferEvents > 0)
//                    {
//                    	//Función para enviar a través del IPCC
////                        callbackEvent(toSendBuff, (counterBufferEvents * 9) + 5, true);
//                    }
//                    tsSend = tsEvent;
//                    toSendBuff[0] = itemBuffToSend[1];
//                    toSendBuff[1] = itemBuffToSend[2];
//                    toSendBuff[2] = itemBuffToSend[3];
//                    toSendBuff[3] = itemBuffToSend[4];
//                    toSendBuff[4] = p;
//                    for (uint8_t i = 5; i < 14; i++)
//                    {
//                        toSendBuff[i] = itemBuffToSend[i];
//                    }
//                    counterBufferEvents = 1;
//                }
//    			else
//    			{
//                    if ((counterBufferEvents < MAX_EVENTS_BUFFER_TO_SEND) && (counterBufferEvents > 0))
//                    {
//                        for (uint8_t i = 0; i < 9; i++)
//                        {
//                            toSendBuff[(counterBufferEvents * 9) + i + 5] = itemBuffToSend[i + 5];
//                        }
//                        counterBufferEvents++;
//                    }
//                    else
//                    {
//                        tsSend = 0;
//                    }
//    			}
//        	}
//        }	// Fin ciclo While
//        if(counterBufferEvents > 0)
//        {
//        	//Función para enviar a través del IPCC
////			callbackEvent(toSendBuff, (counterBufferEvents * 9) + 5, true);
//        }
//
//	}	// Fin ciclo For
//	/***** Codigo para enviar los datos usando la interrupcion del TIMER -> REVISAR *****/


}


//*****// CALLBACKS //*****//

/**
 * @brief Callback del USART VIRTUAL
 */
void virt_UART0_cb0(VIRT_UART_HandleTypeDef *huart)
{
  VirtUart0_ChannelRxSize = huart->RxXferSize < MAX_BUFFER_SIZE ? huart->RxXferSize : MAX_BUFFER_SIZE - 1;
  memcpy(VirtUart0_ChannelBuffRx, huart->pRxBuffPtr, VirtUart0_ChannelRxSize);

  VirtUart0_RxMsg = SET;
}
void virt_UART1_cb0(VIRT_UART_HandleTypeDef *huart)
{
  VirtUart1_ChannelRxSize = huart->RxXferSize < MAX_BUFFER_SIZE ? huart->RxXferSize : MAX_BUFFER_SIZE - 1;
  memcpy(VirtUart1_ChannelBuffRx, huart->pRxBuffPtr, VirtUart1_ChannelRxSize);

  VirtUart1_RxMsg = SET;
}
/* CallBacks del ADC */
void HAL_ADC_ConvHalfCpltCallback(ADC_HandleTypeDef* hadc)
{
	/* Actualización de los buffer de canal del ADC*/
//	voltage1_buf[vol_counter] = adc_buf[0];
//	current1_buf[vol_counter] = adc_buf[3];				// Vamos guardando cada valor del primer DMA en un arreglo hasta obtener 2048 muestras
//	current2_buf[vol_counter] = adc_buf[1];
//	voltage2_buf[vol_counter] = adc_buf[2];
//	current3_buf[vol_counter] = adc_buf[1];
//	voltage3_buf[vol_counter] = adc_buf[2];
	/* Actualización de los buffer de canal del ADC*/

    for (size_t i = 0; i < ADC_BUF_LEN; i++)
    {
        sprintf(&bufferVirtualUsart0[i * 3], "%03X", adc_buf[i]); // Convierte cada byte a formato hexadecimal
    }
    virtualUsart0_State = VIRT_UART_Transmit(&virtUART0, bufferVirtualUsart0, lenADCMssg);
    virtualUsart0_State = VIRT_UART_Transmit(&virtUART0, "\n", 2);
}

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{
	/* Codigo propio de la detección de eventos*/

	// Se empieza a actualizar los valores de la estructura de configuración
	switch (config.disagPhases){

	case(Third_Phase):{

		actualSingCurr[0] 	= (adc_buf[3] < ADC_Zero_Reference_Current) ? Negative : Positive; // Signo en la onda de corriente 1: negativo, 0 positivo
		actualSingVol[0]	= (adc_buf[0] < ADC_Zero_Reference_Voltage) ? Negative : Positive; // Signo en la onda de voltaje

		// Si el numero es negativo, lo rectifico. Si el numero es positivo, no hago nada (Tanto para corriente como para voltaje)
		currentSum[0]		+= ((actualSingCurr[3]) ? ((((2750 << 1) - adc_buf[3]) > 4095) ? 4095 : (2750 << 1) - adc_buf[3]) :  adc_buf[3]);	// (2*2750) = (2750 << 1)
		actualVoltage[0] 	 = ((actualSingVol[0])	? ((((2750 << 1) - adc_buf[0]) > 4095) ? 4095 : (2750 << 1) - adc_buf[3]) :  adc_buf[0]);
		// Actualizamos el pico de voltaje con respecto al pico anterior
		actualVoltagePeak[0] = (actualVoltagePeak[0] < actualVoltage[0]) ? actualVoltage[0] : actualVoltagePeak[0];
		// Aumentamos el número de muestras recolectadas
		countSamples[0]++;
		// Si existe un cambio en el signo de la corriente (lastSingCurr[2] ^ actualSingCurr[2] = 1 cuando son diferentes) se guarda el # de muestras
		if (lastSingCurr[0] ^ actualSingCurr[0]){
				counterShift[0] = countSamples[0];
			}
		// Si existe un cambio en el signo del voltaje se guardan todos los datos
		if (lastSingVol[2] ^ actualSingVol[2])
		{

			dataC.current = currentSum[2];			// Actualizamos la suma de corriente
			dataC.voltage = actualVoltagePeak[2];	// Actualizamos el pico del voltaje
			dataC.samples = countSamples[2];		// Actualizamos el número de muestras hasta que la corriente cruzó el 0 (Cambio de signo)
			// Se compara: 1-> La cantidad de muestras en el cambio de la corriente (counterShift), con respecto a  2-> Las mitas de muestras de un semiperiodo (countSamples>>1)
			dataC.shift = (counterShift[2] <= (countSamples[2] >> 1)) ? counterShift[2] : (uint8_t)(counterShift[2] - countSamples[2]);

			// Se intenta mandar los datos (dataC) a la cola correspondiente (QueueC)
			if(!((TM_BUFFER_Write(&xQueueC_, &dataC, 10) == 10)))
				{
					Error_Handler();
				}
			// Se reinician los valores para una nueva captura de un próximo semiciclo
			currentSum[2] 			= 0;
			countSamples[2] 		= 0;
			actualVoltagePeak[2]	= 0;
		}
		// Se actualizan los signos para compararlos con la siguiente muestra
		lastSingCurr[2] = actualSingCurr[2];
		lastSingVol[2] = actualSingVol[2];

		// Se sigue con el case 2
	} // @suppress("No break at end of case")
	case(Second_Phase):{

		actualSingCurr[0] 	= (adc_buf[3] < ADC_Zero_Reference_Current) ? Negative : Positive; // Signo en la onda de corriente 1: negativo, 0 positivo
		actualSingVol[0]	= (adc_buf[0] < ADC_Zero_Reference_Voltage) ? Negative : Positive; // Signo en la onda de voltaje

		// Si el numero es negativo, lo rectifico. Si el numero es positivo, no hago nada (Tanto para corriente como para voltaje)
		/*
		 * ((2750*2) - adc_buf[3]) :  adc_buf[3]);
		 * ((2050*2) - adc_buf[0]) :  adc_buf[0]);
		 * */
		currentSum[0]		+= ((actualSingCurr[1]) ? ((((2750 << 1) - adc_buf[3]) > 4095) ? 4095 : (2750 << 1) - adc_buf[3]) :  adc_buf[3]);	// (2*2750) = (2750 << 1)
		actualVoltage[0] 	 = ((actualSingVol[1])	? ((((2750 << 1) - adc_buf[0]) > 4095) ? 4095 : (2750 << 1) - adc_buf[3]) :  adc_buf[0]);
		// Actualizamos el pico de voltaje con respecto al pico anterior
		actualVoltagePeak[0] = (actualVoltagePeak[0] < actualVoltage[0]) ? actualVoltage[0] : actualVoltagePeak[0];
		// Aumentamos el número de muestras recolectadas
		countSamples[0]++;
		// Si existe un cambio en el signo de la corriente (lastSingCurr[2] ^ actualSingCurr[2] = 1 cuando son diferentes) se guarda el # de muestras
		if (lastSingCurr[0] ^ actualSingCurr[0]){
				counterShift[0] = countSamples[0];
			}
		// Si existe un cambio en el signo del voltaje se guardan todos los datos
		if (lastSingVol[1] ^ actualSingVol[1])
		{

			dataB.current = currentSum[1];			// Actualizamos la suma de corriente
			dataB.voltage = actualVoltagePeak[1];	// Actualizamos el pico del voltaje
			dataB.samples = countSamples[1];		// Actualizamos el número de muestras hasta que la corriente cruzó el 0 (Cambio de signo)
			// Se compara: 1-> La cantidad de muestras en el cambio de la corriente (counterShift), con respecto a  2-> Las mitas de muestras de un semiperiodo (countSamples>>1)
			dataB.shift = (counterShift[1] <= (countSamples[1] >> 1)) ? counterShift[1] : (uint8_t)(counterShift[1] - countSamples[1]);

			// Se intenta mandar los datos (dataB) a la cola correspondiente (QueueB)
			if(!((TM_BUFFER_Write(&xQueueB_, &dataB, 10) == 10)))
				{
					Error_Handler();
				}
			// Se reinician los valores para una nueva captura de un próximo semiciclo
			currentSum[1] 			= 0;
			countSamples[1] 		= 0;
			actualVoltagePeak[1] 	= 0;
		}
		// Se actualizan los signos para compararlos con la siguiente muestra
		lastSingCurr[1] = actualSingCurr[1];
		lastSingVol[1] = actualSingVol[1];

		// Se sigue con el case 1
	} // @suppress("No break at end of case")
	case(First_Phase):{

		actualSingCurr[0] 	= (adc_buf[3] < ADC_Zero_Reference_Current) ? Negative : Positive; // Signo en la onda de corriente 1: negativo, 0 positivo
		actualSingVol[0]	= (adc_buf[0] < ADC_Zero_Reference_Voltage) ? Negative : Positive; // Signo en la onda de voltaje

//		if(actualSingCurr[0]){
//			auxDataCalculation 		= (Double_Current_Reference - adc_buf[3]);
//			currentSum[0]			+= ((auxDataCalculation > 4095) ? 4095 : auxDataCalculation);
//		}
//		else{
//			currentSum[0]			+= adc_buf[3];
//		}

		// Si el numero es negativo, lo rectifico. Si el numero es positivo, no hago nada (Tanto para corriente como para voltaje)
		currentSum[0]		+= ((actualSingCurr[0]) ? adc_buf[3]:adc_buf[3]);//(((Double_Current_Reference - adc_buf[3]) > 4095) ? 4095 : Double_Current_Reference - adc_buf[3]) :  adc_buf[3]);
		actualVoltage[0] 	 = ((actualSingVol[0])	? (((Double_Current_Reference - adc_buf[0]) > 4095) ? 4095 : Double_Current_Reference - adc_buf[0]) :  adc_buf[0]);
		// Actualizamos el pico de voltaje con respecto al pico anterior
		actualVoltagePeak[0] = (actualVoltagePeak[0] < actualVoltage[0]) ? actualVoltage[0] : actualVoltagePeak[0];
		// Aumentamos el número de muestras recolectadas
		countSamples[0]++;
		// Si existe un cambio en el signo de la corriente (lastSingCurr[2] ^ actualSingCurr[2] = 1 cuando son diferentes) se guarda el # de muestras
		if (lastSingCurr[0] ^ actualSingCurr[0])
		{
			counterShift[0] = countSamples[0];
		}
		// Si existe un cambio en el signo del voltaje se guardan todos los datos
//		if(countSamples[0] == 66)
		if ((lastSingVol[0] ^ actualSingVol[0]))// && (countSamples[0] >= 60))
		{
			auxArray[countSamples[0]] += 1;			// Variable para debuggear

			dataA.current 	= currentSum[0];			// Actualizamos la suma de corriente
			dataA.voltage 	= actualVoltagePeak[0];	// Actualizamos el pico del voltaje
			dataA.samples 	= countSamples[0];		// Actualizamos el número de muestras hasta que la corriente cruzó el 0 (Cambio de signo)
			// Se compara: 1 --> La cantidad de muestras en el cambio de la corriente (counterShift), con respecto a  2-> Las mitas de muestras de un semiperiodo (countSamples>>1)
			dataA.shift 	= (counterShift[0] <= (countSamples[0] >> 1)) ? counterShift[0] : (uint8_t)(counterShift[0] - countSamples[0]);

			// Se intenta mandar los datos (dataA) a la cola correspondiente (QueueA)
			if(!((TM_BUFFER_Write(&xQueueA_, &dataA, 10) == 10)))
				{
					Error_Handler();
				}
			// Se reinician los valores para una nueva captura de un próximo semiciclo
			currentSum[0] 			= 0;
			countSamples[0] 		= 0;
			actualVoltagePeak[0] 	= 0;

		}
		// Se actualizan los signos para compararlos con la siguiente muestra
		lastSingCurr[0] 	= actualSingCurr[0];
		lastSingVol[0] 		= actualSingVol[0];
		// Se termina de analizar todas las fases, y ya se puede salir del switch case
		break;
	}	// Fin First_Phase
	default:{break;}
	}	// Fin SwitchCase

	// Se reinicia la suma acumulada
	if(countSamples[0] == 0)
	{
		currentSum[0] = 0;
	}
	// Se realiazan unos blinky de control
	HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_13);		// Realizamos un blinky en el LED6
//	HAL_GPIO_TogglePin(GPIOD, GPIO_PIN_1);		// Realizamos un blinky en el LED6
}
/* USER CODE END 4 */

/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM4 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM4) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */
  if(htim->Instance == TIM5){
	  auxCounterTIM5++;
	  if(auxCounterTIM5 == 12){
		  // Generamos un Blinky en un led (Para la señal del eventDetector)
		  HAL_GPIO_TogglePin(GPIOD, GPIO_PIN_1);
	  }
	  // Generamos un blinky de StateLed
	  HAL_GPIO_TogglePin(GPIOG, GPIO_PIN_3);
  }
  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
