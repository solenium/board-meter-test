/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32mp1xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "openamp_log.h"
#include "virt_uart.h"
#include "stdbool.h"
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

// Definiciones de la cantidad de datos en cada uno de los buffer
#define MAX_EVENTS_BUFFER			50
#define MAX_EVENTS_BUFFER_TO_SEND	10

#define MAX_VOLTAGE 200		// Voltaje máximo que usará el medidor
#define TYPE_EVENT	false
#define TYPE_PEAK	true

// Definicion de tipos de dato para el detector de eventos

#define Third_Phase 	2
#define Second_Phase 	1
#define First_Phase 	0

#define ADC_Zero_Reference_Voltage	2050//2670
#define ADC_Zero_Reference_Current	2050//2750
#define Double_Current_Reference 	2*ADC_Zero_Reference_Current
#define ADC_Zero_Reference			2050

#define Negative	true
#define Positive	false

typedef long BaseType_t;
typedef unsigned long UBaseType_t;

typedef struct{
    uint32_t current;
    uint32_t voltage;
    uint8_t samples;
    uint8_t shift;
}samplesPerPhase;

typedef struct{
	double lsbCurrent;
	double lsbVoltage;
	double lsbPower;
	uint8_t disagPhases;
	float threshold;
	uint32_t ctCurrent;
	uint8_t pecentOfStabilization;
	UBaseType_t taskPriority;       // Tipo de dato propio de FREERTOS para almacenar información
	UBaseType_t processCore;        // de configuración
}detectorConfiguration;
/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define USB_PWR_CC2_Pin GPIO_PIN_5
#define USB_PWR_CC2_GPIO_Port GPIOA
#define USB_PWR_CC1_Pin GPIO_PIN_4
#define USB_PWR_CC1_GPIO_Port GPIOA

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
