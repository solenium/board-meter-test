# Manejo de hilos
import threading,time
# Librería para la comunicación serial
import serial as sr
# Creación del servicio MQTT
import paho.mqtt.client as mqtt
# Manejo de colas
import queue
# Manejo de señales de finalizacion
import signal
# Manejo de los delay
import time
# Ejecución de operaciones en bash por consola
import os, sys
# Manejo de archivos .env
from dotenv import dotenv_values

# Firmware Information
firmware_name = 'ADC_Sampling_8kHz_CM4.elf'
firmware_path = '/lib/firmware'

# Name of the scrip in C for the feature extraction
dissagregation_ScriptName = 'feature_Extraction_Script_compiled'
dissagregation_path       = ''
 
# Configuration files in .yaml format
config_path = '/home/root/app/config_files/'

# Variable for use the C script
startDisaggregationScrip = False

# Function to read the configuration files in .env format
def get_env(file_name: str):
    """This functions reads the environmental variables file and loads it
    to the script so the variables can be used.

    Args:
        Configuration file name.

    Returns:
        [Env Object]: [Environmental object of the environ library with the environmental variables]
    """
    try:
        #env = environ.Env(DEBUG=(bool, False))
        env = dotenv_values(f'{config_path}.env_{file_name}') #/home/root/app/board-meter/
        while (len(env) == 0):
            env = dotenv_values(f'/home/root/app/config_files/.env_{file_name}')
        return env
    except:
        raise ValueError("Revisa que el archivo de variables de entorno se llame .env y esté en la misma carpera que este script")

config  = get_env(file_name='config')  # Read the DK2 configuration file in .env format
net     = get_env(file_name='network')        # Read the network configuration file in .env format

# Inicialización del puerto serial
_port       = "/dev/ttyRPMSG1" #""/dev/ttyRPMSG0"/dev/pts/5"
_baudrate   = 115200
_bytesize   = 8
_parity     = sr.PARITY_NONE
_stopbits   = 1
_timeout    = None

# Variables para el manejo de datos MQTT
mac = "DK2_001"
_client_id              = "mt" + mac
disag_topic             = "quoia_energy_meter/mt"+ mac + "/disag_wv"        # Topic for disaggregation data
mqtt_broker             = config['MQTT_BROKER'].upper()
broker_address          = net['MQTT_DEFAULT_SERVER']                       # '192.168.68.157'
broker_port             = int(net['MQTT_DEFAULT_PORT'])                       # 1883
_username               = net['MQTT_DEFAULT_USER']
_password               = net['MQTT_DEFAULT_PASSW']
mqttClient              = mqtt.Client("mt"+mac)
mqttClient.username_pw_set(username=net[f'MQTT_{mqtt_broker}_USER'],password=net[f'MQTT_{mqtt_broker}_PASSW'])

# Inicialización de una cola para el manejo de los datos entrantes
queue_DataToSend = queue.Queue(maxsize = 200)

# Manejo para los datos perdidos
bufferDataLost      = [0 for x in range(256)]
counterDataLost     = 0

# Variable Global para controlar el final de los hilos
threadState = True

# Counter for logMessages
counterLogMessages = 0
logMessage         = f"[ED_{counterLogMessages:06d}] "

def incLogCounter():
    global counterLogMessages
    counterLogMessages += 1
    global logMessage 
    logMessage= f"[ED_{counterLogMessages:06d}] "

def id_generator():
    # Función para definir el ID de cada QUOIA
	return "DK2_0001"

def on_message(client, userdata, message):
    # Definir el callback cuando se reciba un mensaje
    incLogCounter()
    print(logMessage + f"Mensaje recibido en el tópico {message.topic}: {message.payload.decode()}")

def on_connect(client, userdata, flags, rc):
    incLogCounter()
    print(logMessage + f'connected ({client._client_id}) at topic: {disag_topic}')
    # Funcion para subscribirse a diferentes topicos
	#client.subscribe(topic = broker_topic, qos=2)                        

def dataReception():
    incLogCounter()
    print(logMessage + "// ***// Inicio del servicio de captura de datos // ***//")
    counterToQuit = 0  
    # Variable Bandera
    global threadState  

    # Conexion al puerto serial    
    while threadState:
        try:
            # Creación del objeto
            USBPort = sr.Serial(_port, xonxoff=0, rtscts=0)
            USBPort.baudrate = _baudrate
            USBPort.bytesize = _bytesize
            USBPort.parity   = _parity
            USBPort.stopbits = _stopbits
            USBPort.timeout  = _timeout
            incLogCounter()
            print(logMessage + " Inicialización Puerto Serial Exitosa ")
            incLogCounter()
            print(logMessage + " Esperando datos...")
            break
        except:
                if(counterToQuit == 5):
                    _killService()
                    break
                else:
                    incLogCounter()
                    print(logMessage + "Error de conexión en el puerto: " + _port)
                    incLogCounter()
                    print(logMessage + "Reintentando...")
                    time.sleep(5)
                counterToQuit += 1

    # //***// Inicio de la recepción de datos a través del IPCC //***//
    while(threadState):
        # Empezamos a leer los datos provenientes del puerto serial
        try:
            lineData = USBPort.readline()                       # El método .readline devuelve un elemento tipo 'bytes'
            dataString = (lineData.decode('utf-8')).strip()     # .decode convierte el elemento de 'bytes' a 'str', y el .strip() quita los '\n\r'
            try:
                queue_DataToSend.put(dataString, block=False)   # Se intenta agregar el elemento a un cola
            except queue.Full:                                  # Manejo de error en el caso de que la cola se encuentre llena
                counterDataLost += 1
                bufferDataLost[counterDataLost] = dataString
                incLogCounter()
                print(logMessage + "Se han perdido {} datos".format(counterDataLost))
                # Se reinicia el contador (simulando 8 bits)
                if(counterDataLost == 255):
                    counterDataLost = 0
            # Se imprimen los datos recibidos
            #incLogCounter()
            #print(logMessage + "Datos recibidos:", dataString)
        except UnicodeDecodeError:
            pass
        except NameError:
            incLogCounter()
            print(logMessage + "NameError")

    try:
        USBPort.close()  # Cierra el puerto serial al finalizar
    except UnboundLocalError:
        pass
    incLogCounter()
    print(logMessage + "/***/ Cerrando puerto Serial /***/")


def dataSendMQTT():
    # Creacion del objeto MQTT
    client = mqtt.Client(client_id = _client_id, clean_session   = False)      # False: Remember Client Mssg when it is offline - True: Don't remember
    client.username_pw_set(username=_username,password=_password)
    client.on_connect = on_connect                               # Callback cuando hay una conexion exitosa al broker
    client.on_message = on_message                               # Callback para subscribir
    client.connect(host = broker_address, port = broker_port)    # Nos conectamos al broker     
    client.loop_start()                                                            # Iniciamos el servicio MQTT

    # Introducir un delay de 2 segundos
    time.sleep(2)

    client.publish(disag_topic, "Conectado")

    # Variable Bandera
    global threadState

    while threadState:
        data                    = queue_DataToSend.get()                            # Obtener datos de la cola
        if(data[0] == '\0'):                                                        # Se elimina el espacio vacío del inicio del dato
            data = data[1:]
        data_with_timeStamp     = data[0:2] + get_rtc_timeStamp() + data[10:-1]     # Agregar el Time Stamp
        try:
            data_coded              = bytes.fromhex(data_with_timeStamp)                # Convierte la cadena hexadecimal en una secuencia de bytes
        except ValueError:
            pass
        client.publish(disag_topic, data_coded)                                     # Publicar los datos en el tópico
        incLogCounter()
        print(logMessage + f"Dato original: {data_coded}")
        print(logMessage + f"Datos enviados: {data_coded.hex()}")
    incLogCounter()
    print(logMessage + "/***/ Desconectando servicio MQTT /***/")
    # Desconectar el cliente MQTT
    client.disconnect()
    client.loop_stop()

def _killService():
    global threadState
    threadState = False
    queue_DataToSend.put("DataEndProgram", block=False)   # Se intenta agregar el elemento a un cola
    print("Finalizando Hilos...")

def killService(signal, frame):
    _killService()

def copro_is_fw_running():
    MAX_BUF = 80
    result  = 0
    user = os.getenv("USER")
    if (user == "root"):
        os.system("XTERM=xterm su root -c 'cat /sys/class/remoteproc/remoteproc0/state' > /tmp/remoteproc0_state")
        fd = open("/tmp/remoteproc0_state", "r")
    else:
        fd = open("/sys/class/remoteproc/remoteproc0/state", "r")
    
    if fd is None:
        print(f"CA7 : Error opening remoteproc0/state, err=-{os.errno}")
        return -os.errno
    
    bufRead = fd.read(MAX_BUF)
    if len(bufRead) >= len("running"):
        pos = bufRead.find("running")
        if pos != -1:
            result = 1
    else:
        result = 0
    
    fd.close()
    return result

def copro_stopFw():
    user = os.getenv("USER")
    if (user == "root"):
        os.system("su root sh -c 'echo stop > /sys/class/remoteproc/remoteproc0/state'")
        return 0
    try:
        fd = open("/sys/class/remoteproc/remoteproc0/state", "w") 
        fd.write("stop")
        fd.close()
        return 0
    except Exception as e:
        print(f"CA7 : Error opening remoteproc0/state, err={e}")
        return -1

def copro_startFw():
    user = os.getenv("USER")
    if (user == "root"):
        os.system("su root sh -c 'echo start > /sys/class/remoteproc/remoteproc0/state'")
        return 0
    try:
        fd = open("/sys/class/remoteproc/remoteproc0/state", "w") 
        fd.write("start")
        fd.close()
        return 0
    except Exception as e:
        print(f"CA7 : Error opening remoteproc0/state, err={e}")
        return -1

def copro_setFwPath(pathStr):
    user = os.getenv("USER")
    if (user == "root"):
        os.system(f"echo {pathStr} > /sys/module/firmware_class/parameters/path")
        return len(pathStr)
    
    try:
        fd = open("/sys/module/firmware_class/parameters/path", "w")
        fd.write(pathStr)
        return len(pathStr)
    except Exception as e:
        print(f"Error opening firmware_class/parameters/path: {str(e)}")
        return -1

def copro_setFwName(nameStr):
    user = os.getenv("USER")
    if (user == "root"):
        os.system(f"echo {nameStr} > /sys/class/remoteproc/remoteproc0/firmware")
        return len(nameStr)
    
    try:
        fd = open("/sys/class/remoteproc/remoteproc0/firmware", "w") 
        fd.write(nameStr)
        return len(nameStr)
    except Exception as e:
        print(f"Error opening remoteproc0/firmware: {str(e)}")
        return -1

def copro_openTtyRpmsg(ttyNumber):
    os.system("stty -onlcr -echo -F /dev/ttyRPMSG"+str(ttyNumber))
    os.system("echo Ping test 0 > /dev/ttyRPMSG"+str(ttyNumber))

def get_rtc_timeStamp():
    rtc_file = open('/proc/driver/rtc', 'r')
    lines = rtc_file.readlines()

    rtc_time_line   = next(line for line in lines if ('rtc_time' in line))          # Read the value at 'rtc_time'
    rtc_date_line   = next(line for line in lines if ('rtc_date' in line))          # Read the value at 'rtc_date'

    rtc_time = rtc_time_line[11:-1]     # Save the values str int format
    rtc_date = rtc_date_line[11:-1]     # Save the values str int format

    time_zone   = 18000                 # Time zone for Colombia
    
    # Combined Value of date and time
    timestamp = int(time.mktime(time.strptime(f"{rtc_date} {rtc_time}", "%Y-%m-%d %H:%M:%S"))) - time_zone
    # Convert the integer value to hexadecimal of 4 bytes (8 characters)
    hex_timestamp = f"{timestamp:08X}"
    return hex_timestamp

if __name__=='__main__':
    firmware_init_flag = True
    while firmware_init_flag:
        # Inicialización del Firmware
        if(copro_is_fw_running() == 0):
            copro_setFwPath(firmware_path)
            copro_setFwName(firmware_name)
            copro_startFw()
            incLogCounter()
            print(logMessage + "Starting Firmware...")
            time.sleep(1)
            copro_openTtyRpmsg(1)
            if(startDisaggregationScrip):
                os.system("./"+dissagregation_ScriptName+" &")
                copro_openTtyRpmsg(0)
            firmware_init_flag = False
        # En el caso de que haya un firmware ejecutándose, se apaga, y se reinicia con el firmware actual
        elif(copro_is_fw_running() == 1):
            copro_stopFw()
            incLogCounter()
            print(logMessage + "RestartingFirmware...")
            time.sleep(2)

    # Atencion a la interrupción por teclado
    signal.signal(signal.SIGINT, killService)

    # Creacion de los hilos
    mqttTask = threading.Thread(target=dataSendMQTT)
    IPCCTask = threading.Thread(target=dataReception)
    # Inicio de los hilos
    mqttTask.start()
    IPCCTask.start()
    # Finalizacion de los hilos
    mqttTask.join()
    IPCCTask.join()
incLogCounter()
print(logMessage + "Desactivando Firmware...")
copro_stopFw()
incLogCounter()
print(logMessage + "Programa Finalizado")