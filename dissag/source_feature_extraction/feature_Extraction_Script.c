/* Libraries for IPCC */
#include <signal.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
/* Libraries for threads */
#include <pthread.h>
#include <stdbool.h>
#include <semaphore.h>
/* Library for delta time */
#include <time.h>
/* Library variable format length*/
#include <stdarg.h>
/* Library for FFT */
#include "kiss_fft.h"

/* Global variables definition */
int errno                   = 0;
int FdRpmsg;
#define numberOfChannels    4
#define lenDataReceive      (numberOfChannels*3 + 1)
char dataReceive[lenDataReceive]     = {0};
uint16_t counterADC         = 0;
uint16_t counterSeconds     = 0;

struct timespec inicio, fin;
double tiempo_transurrido;

/* Variables definitions for the FFT*/
uint16_t bufferADC_ReceivedData[numberOfChannels]           = {0};
#define lenReceptionBuffer  2048
//volatile float firstInputBuffer_FFT[lenReceptionBuffer]     = {0};
//volatile float secondInputBuffer_FFT[lenReceptionBuffer]    = {0};
bool bufferIsFull                                           = false;

# define M_PI		3.14159265358979323846	/* pi */

#define N                   2048          // Tamaño de la FFT
#define SAMPLING_RATE       8000.0        // Tasa de muestreo en Hz
#define FREQ1               50            // Frecuencias para la señal de prueba
#define FREQ2               150
#define FREQ3               337

// Structs of configuration for fft
kiss_fft_cfg fft_handler_cfg;
kiss_fft_cpx fft_in[N], fft_out[N];

kiss_fft_cpx firstInputBuffer_FFT[N]    = {0};
kiss_fft_cpx secondInputBuffer_FFT[N]   = {0};

// Input and output Buffers for the current
kiss_fft_cpx* current_FirstInputBuffer_FFT[numberOfChannels/2]      = {0};
kiss_fft_cpx* current_SecondInputBuffer_FFT[numberOfChannels/2]     = {0};
kiss_fft_cpx* current_OutputBuffer_FFT[numberOfChannels/2]          = {0};
// Input Buffers for the Voltage
kiss_fft_cpx* voltage_FirstInputBuffer_FFT[numberOfChannels/2]      = {0};
kiss_fft_cpx* voltage_SecondInputBuffer_FFT[numberOfChannels/2]     = {0};
kiss_fft_cpx* voltage_OutputBuffer_FFT[numberOfChannels/2]          = {0};

#define numberOfHarmonics   3
float maxAmplitudes[numberOfHarmonics]     = {0};
float maxFrecuencies[numberOfHarmonics]    = {0};

/* Definitions for the measure of time of the FFT*/
struct timespec ts_FFTbegin, ts_FFTend;
double fft_duration;

/* Global variables for the log Message */
static uint16_t counterLogMessages = 0;

void logMessage(char* string, ...)
{
    char basicLogMessage[100] = {0};

    va_list args;
    va_start(args, string);
    vsnprintf(basicLogMessage, sizeof(basicLogMessage), string, args);
    va_end(args);
    counterLogMessages++;
    printf("[DISAG_%06d] %s", counterLogMessages, basicLogMessage);
}

/* Control de recursos compartidos entre los hilos */
sem_t semaphore;

// Variable Global para controlar el final de los hilos
bool threadState = true; 

/* Global headers definitions */
int openTtyRpmsg(int ttyNb, int *FdRpmsg);
int closeTtyRpmsg(int FdRpmsg);
int writeTtyRpmsg(int FdRpmsg, int len, char* pData);
int readTtyRpmsg(int FdRpmsg, int len, char* pData);
void *task_FFT(void *arg);
void *dataReception(void *arg);
void killService(int signal);
void dataUnpacking(char* stringPackage, uint16_t *ptrData);
void calculate_harmonics(kiss_fft_cpx* out, uint8_t nHarmonics, float* max_magnitude, float* max_indices);
void initFFT(kiss_fft_cfg* cfg);
void deInitFFT(kiss_fft_cfg* cfg);
void computeFFT(kiss_fft_cfg* cfg, kiss_fft_cpx* in,kiss_fft_cpx* out);

/* Ciclo Principal */

int main(void) {
    logMessage("Iniciando Programa \n");

    // Atencion a la interrupción por teclado
    signal(SIGINT, killService);

    // Instancia del semaforo
    sem_init(&semaphore, 0, 0); // Inicializa el semáforo en 0 (no disponible)

    // Instancias para los hilos
    pthread_t fft_Task;
    pthread_t IPCCTask;

    logMessage("Iniciando Hilos \n");
    // Creacion de los hilos
    pthread_create(&fft_Task, NULL, task_FFT, (void *) 1);
    pthread_create(&IPCCTask, NULL, dataReception, (void *) 2);

    // Finalizacion de los hilos
    pthread_join(fft_Task, NULL);
    pthread_join(IPCCTask, NULL);

    sem_destroy(&semaphore);                // Libera recursos del semáforo

    logMessage("Programa Finalizado \n");
}

void *task_FFT(void *arg)
{
    (void) arg;

    sleep(1);

    // Initialization of the FFT
    initFFT(&fft_handler_cfg);

    while (threadState) 
    {
        logMessage("Thread processingFFT \n");

        /* FFT calculation */
        if(bufferIsFull)
        {
            // Se llenó el PRIMER (1) inputBuffer 
            computeFFT(&fft_handler_cfg, firstInputBuffer_FFT, fft_out);
        }
        else
        {
            //Se llenó el SEGUNDO (2) inputBuffer
            computeFFT(&fft_handler_cfg, firstInputBuffer_FFT, fft_out);
        }
        // Atemp to hold the semaphore
        sem_wait(&semaphore);
    }
    logMessage("/***/ Desconectando servicio FFT /***/ \n");
    deInitFFT(&fft_handler_cfg);
    pthread_exit(NULL);
}

void *dataReception(void *arg)
{
    (void) arg;
    int numberPort = 0; 

    openTtyRpmsg(numberPort, &FdRpmsg);   // Initialization of the port

    logMessage("Esperando datos...\n");

    //inicio = clock();
    while(threadState)
    {
        int bytes_read = readTtyRpmsg(FdRpmsg,lenDataReceive,dataReceive);
        dataReceive[bytes_read] = '\0';

        if(bytes_read == lenDataReceive) //if (bytes_read >= (lenDataReceive - 1))
        {
            //dataReceive[bytes_read] = '\0';
            if(counterADC == lenReceptionBuffer)
            {   
                clock_gettime(1, &fin);                       // Get timeStamp
                tiempo_transurrido = (fin.tv_sec - inicio.tv_sec) + (fin.tv_nsec - inicio.tv_nsec) / 1e3;
                logMessage("Tiempo en llenar el buffer en microsegundos: %f\n", tiempo_transurrido);
                
                bufferIsFull ^= 1;      // Buffer change
                if(bufferIsFull)
                {
                    logMessage(" Se llenó el PRIMER (1) inputBuffer \n");
                }
                else
                {
                    logMessage(" Se llenó el SEGUNDO (2) inputBuffer \n");
                }
                clock_gettime(1, &inicio);             // Get timeStamp
                counterADC = 0;             // Restart of the ADC counter index
                sem_post(&semaphore);       // Semaphore Release


            }
            else
            {   
                // Convert the hexadecimal data into an array of N channels
                dataUnpacking(dataReceive, bufferADC_ReceivedData);
                if(!bufferIsFull)
                {
                    firstInputBuffer_FFT[counterADC].r = bufferADC_ReceivedData[0];

                    /*for(uint8_t i = 0; i < numberOfChannels; i += 2)
                    {
                        current_FirstInputBuffer_FFT[i][counterADC].r = bufferADC_ReceivedData[i];
                        voltage_FirstInputBuffer_FFT[i][counterADC].r = bufferADC_ReceivedData[i+1];
                    }*/

                }
                else
                {
                    secondInputBuffer_FFT[counterADC].r = bufferADC_ReceivedData[0];

                    /*for(uint8_t i = 0; i < numberOfChannels; i += 2)
                    {
                        current_SecondInputBuffer_FFT[i][counterADC].r = bufferADC_ReceivedData[i];
                        voltage_SecondInputBuffer_FFT[i][counterADC].r = bufferADC_ReceivedData[i+1];
                    }*/
                }
                counterADC++;
            }
        }
        else
        {
            usleep(100);
        }
    }

    closeTtyRpmsg(FdRpmsg);
    logMessage("/***/ Desconectando servicio RemoteProg /***/\n");
    pthread_exit(NULL);
}




/* Definición de Funciones */

/**
 * @brief Function to de-initialice the structs and free the memory used by the FFT
*/
void deInitFFT(kiss_fft_cfg* cfg)
{
    // Free the alocated memory
    kiss_fft_free(*cfg);
}

/**
 * @brief Function to initialice the "objetcs" for the FFT routines
*/
void initFFT(kiss_fft_cfg* cfg)
{
    /* FFT initialization */  
    *cfg = kiss_fft_alloc(N, 0, NULL, NULL);

    // initialize data storage
    memset(fft_in, 0, N * sizeof(kiss_fft_cpx));
    memset(fft_out, 0, N * sizeof(kiss_fft_cpx));

    // Generar una señal compuesta de 3 frecuencias:
    for (int i = 0; i < N; i++) 
    {
        fft_in[i].r = sin(2 * M_PI * FREQ1 * ((float) i / SAMPLING_RATE)) +
                   sin(2 * M_PI * FREQ2 * ((float) i / SAMPLING_RATE)) +
                   sin(2 * M_PI * FREQ3 * ((float) i / SAMPLING_RATE));
        fft_in[i].i = 0;
    }        
}

/**
 * @brief Function to compute the FFT and the harmonics of 2048 sample
*/
void computeFFT(kiss_fft_cfg* cfg, kiss_fft_cpx* in,kiss_fft_cpx* out)
{
    clock_gettime(1, &ts_FFTbegin);             // Get timeStamp
    kiss_fft(*cfg, in, out);                    // Compute FFT
    calculate_harmonics(out, numberOfHarmonics, maxAmplitudes, maxFrecuencies);
    /* FFT calculation */
    clock_gettime(1, &ts_FFTend);                       // Get timeStamp
    // Compute the fft duration in microsecopernds
    fft_duration = (ts_FFTend.tv_sec - ts_FFTbegin.tv_sec) + (ts_FFTend.tv_nsec - ts_FFTbegin.tv_nsec) / 1e3;
    logMessage("Tiempo transcurrido en microsegundos: %f\n", fft_duration);
}   

/**
 * @brief Function to find the N principal harmonics
 * @param out KissFFT outputStruct
 * @param nHarmonics number of principal harmonics to find
 * @param max_magnitude Array holding the magnitudes of the i's harmonic
 * @param max_indices Array holding the frecuencies of the i's harmonic
*/
void calculate_harmonics(kiss_fft_cpx* out, uint8_t nHarmonics, float* max_magnitude, float* max_indices) {

    // Initialization fo the arrays
    for (int i = 0; i < nHarmonics; i++) 
    {
        max_magnitude[i]    = 0;
        max_indices[i]      = 0;
    }
    // Calcular los n armónicos dominantes
    for (int i = 0; i < N/2; i++) 
    {
        // Calculation of the magnitude
        float magnitude = sqrt(out[i].r * out[i].r + out[i].i * out[i].i);

        for (int j = 0; j < nHarmonics; j++) 
        {
            if (magnitude > max_magnitude[j]) 
            {
                for (int k = nHarmonics - 1; k > j; k--) 
                {
                    max_magnitude[k] = max_magnitude[k - 1];
                    max_indices[k] = max_indices[k - 1];
                }

                max_magnitude[j] = magnitude;
                max_indices[j] = i;
                break;
            }
        }
    }


    for (int i = 0; i < nHarmonics; i++) {
        float frequency = (float)max_indices[i] * SAMPLING_RATE / N;
        max_indices[i]  = frequency;
        logMessage("Harmonic %d - Frequency: %.2f Hz, Magnitude: %.2f\n", i+1, frequency, max_magnitude[i]);
    }
}

/**
 * @brief Function to manage the keyboard interruption Ctrl+C
*/
void killService(int signal)
{
    (void) signal;
    threadState = false;
    sem_post(&semaphore);       // Semaphore Release
    logMessage("Desactivando todos los hilos...\n");
    //exit(0); // Termina el programa
}

/**
 * @brief Function to unpack de data recived from the serial port in hexadecimal format
 * @param stringPackage is the string recived from the serial port
*/
void dataUnpacking(char* stringPackage, uint16_t *ptrData)
{
    for(uint8_t i = 0; i < numberOfChannels; i++)
    {
        sscanf(stringPackage + (i*3), "%3hx", (ptrData+i));
    }
}

/**
 * @brief Function to initialize a Port
 * @param ttyNb x number from RPMSGx port
*/
int openTtyRpmsg(int ttyNb, int *FdRpmsg)
{
    char devName[50];                               // Port name reconstruction
    sprintf(devName, "/dev/ttyRPMSG%d", ttyNb);
    // Open in read/write mode (O_RDWR) no control terminal (O_NOCTTY) and Nonblocking mode | O_NONBLOCK
    *FdRpmsg = open(devName, (O_RDWR |  O_NOCTTY) & (~O_NONBLOCK));
    if (*FdRpmsg < 0) 
    {
        logMessage(" Error opening ttyRPMSG%d, err=-%d\n", ttyNb, errno);
        return (errno * -1);
    }
    return 0;
}

/**
 * @brief Function to close a Port
*/
int closeTtyRpmsg(int FdRpmsg)
{
    close(FdRpmsg);
    return 0;
}

/**
 * @brief Write nBytes of information into virtioChannel
*/
int writeTtyRpmsg(int FdRpmsg, int len, char* pData)
{
    int result = 0;
    if (FdRpmsg < 0) {
        logMessage("CA7 : Error writing ttyRPMSG, fileDescriptor is not set\n");
        return FdRpmsg;
    }
 
    result = write(FdRpmsg, pData, len);
    return result;
}

/**
 * @brief Read nBytes of information into virtioChannel
 * @param FdRpmsg global definition of the serial Port
 * @param len length of bytes to read
 * @param pData pointer where the data will be stored
*/
int readTtyRpmsg(int FdRpmsg, int len, char* pData)
{
    int byte_rd, byte_avail;
    int result = 0;
    if (FdRpmsg < 0) {
        logMessage("CA7 : Error reading ttyRPMSG, fileDescriptor is not set\n");
        return FdRpmsg;
    }
    // byte_avail holds the numbers of byte to been readed
    ioctl(FdRpmsg, FIONREAD, &byte_avail);
    if (byte_avail > 0) {
        if (byte_avail >= len) {
            byte_rd = read(FdRpmsg, pData, len);
        } else {
            byte_rd = read(FdRpmsg, pData, byte_avail);
        }
        result = byte_rd;
    } else {
        result = 0;
    }
    return result;
}
