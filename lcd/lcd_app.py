# Manejo de hilos
import threading
# Manejo de señales de finalizacion
import signal
# Manejo de los delay
import time
# Manejo de los botones de la dk2
from buttons_control import button
# Manejo de la pantalla OLED
from lcd_driver import lcd_25664
# Manejo del reproc del M4
from M4_rproc import copro_start_firmware, copro_stopFw
# Manejo de las comandos por sistemas
import sys, os


# Absolute path to the folder
absolute_path = '/home/root/app/board-meter/lcd/'

# Arrays holding the Menu information
OLED_MENU = ["Datos metering", "Iniciar WebServer", "ADCs con RS485 M4"]
menu_states = {}

# Flags to control de Finite State Machine of the Menu
menu_On      = 1
menu_Off     = 0
menu_freeze  = 2
menu_update  = 3
lcd_state    = menu_Off

# Init all the menu states to 0
for i in range(len(OLED_MENU)):
    menu_states[OLED_MENU[i]] = 0

#************ Definición de las funciones de cada uno de los elementos del menú ************#
def metering_function(state):
    if(state == 0):
        # Activando metering
        voltaje = 120
        oled.print_row_LCD_25664(0,"---- Vol:120V  Power:469kW ----")
    else:
        oled.print_row_LCD_25664(0,"----  Medidor de Frontera  ----")

def web_server_function(state):
    if(state == 0):
        # Activando webserver
        oled.print_row_LCD_25664(0,"---- Acceso IP:192.168.4.1 ----")
        os.system(f'python3 /home/root/app/board-meter/web_server/wireless_config.py')
        #oled.print_row_LCD_25664(0,"---- Iniciando Puerto-RS485----")
        #oled.print_row_LCD_25664(0,"----  Medidor de Frontera  ----")
        #oled.print_row_LCD_25664(0,"---- Vol:120V  Power:469kW ----")
    else:
        # Desactivando webserver
        os.system('python3 /home/root/app/board-meter/web_server/wireless_config.py')
        oled.print_row_LCD_25664(0,"----  Medidor de Frontera  ----")

def adc_rs485_m4_function(state):
    if(state == 0):
        # Activando ADCs con RS485 M4
        copro_start_firmware("/lib/firmware", "ADC_M4_RS485.elf")
        oled.print_row_LCD_25664(0,"---- Iniciando PuertoRS485 ----")
    else:
        # Parando ADCs con RS485 M4
        copro_stopFw()
        oled.print_row_LCD_25664(0,"----  Medidor de Frontera  ----")

# Array holding the functions of each menu element
OLED_MENU_FUNCTIONS = [metering_function, web_server_function, adc_rs485_m4_function]

#************ Callback de cada uno de los botones ************#

def callback_left():
    global lcd_state
    print("Button left pressed")
    # Si el menu está apagado, no hacemos nada
    if(lcd_state != menu_freeze):       # Si no se encuentra congelado el menú puede proceder
        if(lcd_state == menu_On):       # Si el menu está encendido, lo apagamos y volvemos a la pantalla de inicio
            print(f"Volviendo a la pantalla de inicio")             
            lcd_state = menu_Off        # Apagamos el menu
            # Cargamos la pantalla de inicio
            oled.Enable_Partial_Display_25664(0x38,0x3F)
            oled.print_row_LCD_25664(7,f"   Cargando...")
            oled.ImageDisplay_25664(oled.quoia_by_solenium)
            oled.Exit_Partial_Display_25664()
            oled.print_LCD_25664(f"----  Medidor de Frontera  ----")

def callback_right():
    global lcd_state
    print("Button right pressed")
    # Si el menu está apagado, no hacemos nada
    if(lcd_state == menu_On or lcd_state == menu_freeze):     # Boton que controla los programas que se pueden seleccionar del menú
        # Obtenemos a cuál opcipón del Menú corresponde
        index = (oled.indicator - 2) // 2
        print(f"Activando {OLED_MENU[index]}")

        
        # Implementación de cada función para cada uno de los elementos del menú
        if(menu_states[OLED_MENU[index]] == 0):
            lcd_state = menu_update
            oled.update_indicator("select")
            OLED_MENU_FUNCTIONS[index](0)
            lcd_state = menu_freeze
        else:
            lcd_state = menu_update
            oled.update_indicator("deselect")
            OLED_MENU_FUNCTIONS[index](1)
            lcd_state = menu_On
        # Se cambia el estado al estado inmediatamente anterior
        menu_states[OLED_MENU[index]] ^= 1

def callback_enter():
    global lcd_state
    print("Button enter pressed")
    # Si el menu está apagado, no hacemos nada
    if(lcd_state != menu_freeze):       # Si no se encuentra congelado el menú puede proceder
        if(lcd_state == menu_On):       # Si el menu está encendido, nos desplazamos por el menu
            lcd_state = menu_freeze
            oled.update_indicator("up")
            lcd_state = menu_On
        if(lcd_state == menu_Off):   # Si el menu está apagado, lo encendemos
            oled.print_row_LCD_25664(7,f"   Cargando...")
            #oled.clean_mul_rows_lCD_25664(1,5)
            oled.Enable_Partial_Display_25664(0x00,0x08)
            oled.set_menu(OLED_MENU)
            oled.Exit_Partial_Display_25664()

            lcd_state = menu_On

def callback_down():
    global lcd_state
    print("Button down pressed")

    if(lcd_state != menu_freeze):
        if(lcd_state == menu_On):
            lcd_state = menu_freeze
            oled.update_indicator("down")
            lcd_state = menu_On

# Object creation for each button
button_left     = button("button_left", button.GPIOB, 10)       # Boton izquierdo (atras)
button_right    = button("button_right", button.GPIOB, 12)      # Boton derecho (adelante)
button_enter    = button("button_enter", button.GPIOG, 8)       # Boton central (arriba)
button_down     = button("button_down", button.GPIOB, 13)       # Boton central (abajo)

# Function to kill all the services
def killService(signal, frame):
    button_left.stop_interrupt()
    button_right.stop_interrupt()
    button_enter.stop_interrupt()
    button_down.stop_interrupt()
    print("Finalizando Hilos...")
    print("Presione cada uno de los botones para finalizar el programa")

# Function to init all the buttons
def init_buttons():
    button_left.init_port()
    button_right.init_port()
    button_enter.init_port()
    button_down.init_port()

# Function to init the screen
def start_Screen():
    oled = lcd_25664()

    oled.OLED_Init_25664()
    print("Mostrando imagenes en pantalla")
    oled.ImageDisplay_25664(oled.quoia_by_solenium)
    oled.print_LCD_25664(f"----  Medidor de Frontera  ----")
    return oled

# Main function
if __name__=='__main__':

    init_buttons()

    oled = start_Screen()

    # Atencion a la interrupción por teclado
    signal.signal(signal.SIGINT, killService)
    # Atencion a la finalizacion del servicio
    signal.signal(signal.SIGTERM, killService)

    button_left_task    = threading.Thread(target=button_left.init_interrupt, args=(callback_left,))
    button_right_task   = threading.Thread(target=button_right.init_interrupt, args=(callback_right,))
    button_enter_task   = threading.Thread(target=button_enter.init_interrupt, args=(callback_enter,))
    button_down_task    = threading.Thread(target=button_down.init_interrupt, args=(callback_down,))

    button_left_task.start()
    button_right_task.start()
    button_enter_task.start()
    button_down_task.start()

    button_left_task.join()
    button_right_task.join()
    button_enter_task.join()
    button_down_task.join()

oled.OLED_DeInit()
print("Finalizando programa Exitosamente")
