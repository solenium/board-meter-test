# Manejo de los delay
import time
# Ejecución de operaciones en bash por consola
import os

# Firmware Information
firmware_name = 'ADC_Sampling_8kHz_CM4.elf'
firmware_path = '/lib/firmware'

def copro_is_fw_running():
    MAX_BUF = 80
    result  = 0
    user = os.getenv("USER")
    if (user == "root"):
        os.system("XTERM=xterm su root -c 'cat /sys/class/remoteproc/remoteproc0/state' > /tmp/remoteproc0_state")
        fd = open("/tmp/remoteproc0_state", "r")
    else:
        fd = open("/sys/class/remoteproc/remoteproc0/state", "r")
    
    if fd is None:
        print(f"CA7 : Error opening remoteproc0/state, err=-{os.errno}")
        return -os.errno
    
    bufRead = fd.read(MAX_BUF)
    if len(bufRead) >= len("running"):
        pos = bufRead.find("running")
        if pos != -1:
            result = 1
    else:
        result = 0
    
    fd.close()
    return result


def copro_stopFw():
    user = os.getenv("USER")
    if (user == "root"):
        os.system("su root sh -c 'echo stop > /sys/class/remoteproc/remoteproc0/state'")
        return 0
    try:
        fd = open("/sys/class/remoteproc/remoteproc0/state", "w") 
        fd.write("stop")
        fd.close()
        return 0
    except Exception as e:
        print(f"CA7 : Error opening remoteproc0/state, err={e}")
        return -1

def copro_startFw():
    user = os.getenv("USER")
    if (user == "root"):
        os.system("su root sh -c 'echo start > /sys/class/remoteproc/remoteproc0/state'")
        return 0
    try:
        fd = open("/sys/class/remoteproc/remoteproc0/state", "w") 
        fd.write("start")
        fd.close()
        return 0
    except Exception as e:
        print(f"CA7 : Error opening remoteproc0/state, err={e}")
        return -1

def copro_setFwPath(pathStr):
    user = os.getenv("USER")
    if (user == "root"):
        os.system(f"echo {pathStr} > /sys/module/firmware_class/parameters/path")
        return len(pathStr)
    
    try:
        fd = open("/sys/module/firmware_class/parameters/path", "w")
        fd.write(pathStr)
        return len(pathStr)
    except Exception as e:
        print(f"Error opening firmware_class/parameters/path: {str(e)}")
        return -1

def copro_setFwName(nameStr):
    user = os.getenv("USER")
    if (user == "root"):
        os.system(f"echo {nameStr} > /sys/class/remoteproc/remoteproc0/firmware")
        return len(nameStr)
    
    try:
        fd = open("/sys/class/remoteproc/remoteproc0/firmware", "w") 
        fd.write(nameStr)
        return len(nameStr)
    except Exception as e:
        print(f"Error opening remoteproc0/firmware: {str(e)}")
        return -1
    
def copro_start_firmware(_firmware_path, _firmware_name):
    firmware_init_flag = True
    while firmware_init_flag:
        # Inicialización del Firmware
        if(copro_is_fw_running() == 0):
            copro_setFwPath(_firmware_path)
            copro_setFwName(_firmware_name)
            copro_startFw()

            print("Starting Firmware...")
            time.sleep(1)
            firmware_init_flag = False
        # En el caso de que haya un firmware ejecutándose, se apaga, y se reinicia con el firmware actual
        elif(copro_is_fw_running() == 1):
            copro_stopFw()
            print("RestartingFirmware...")
            time.sleep(2)


    