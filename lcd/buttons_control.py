import gpiod
import select
import os

class button:
    # Global defines
    GPIOA = 0
    GPIOB = 1
    GPIOC = 2
    GPIOD = 3
    GPIOE = 4
    GPIOF = 5
    GPIOG = 6
    GPIOH = 7
    GPIOI = 8

    # Constructor
    def __init__(self, name, port, pinNumber):
        self.name = name
        self.port = port
        self.pinNumber = pinNumber

    # Initializate the port method
    def init_port(self):
        self.chip_name = f"/dev/gpiochip{self.port}"
        self.chip = gpiod.chip(self.chip_name, gpiod.chip.OPEN_BY_PATH)
        self.line = self.chip.get_line(self.pinNumber)
        self.config = gpiod.line_request()
        self.config.consumer = self.name
        self.config.flags = gpiod.line_request.FLAG_BIAS_PULL_DOWN
        self.config.request_type = gpiod.line_request.EVENT_RISING_EDGE
        self.line.request(self.config)
        self.fd = self.line.event_get_fd()
    
    # Setting the callback method
    def _set_callback(self, callback):
        self.callback = callback

    def _start_polling(self):
        self.interrupt.poll(None)
        event = self.line.event_read()
        if self.on_flag == True:
            self.callback()

    # Loading the interrupt method
    def init_interrupt(self, callback):
        self._set_callback(callback)
        print(f"Monitoring GPIO events on {self.chip_name}. Press Ctrl+C to exit.")
        self.interrupt = select.poll()
        self.interrupt.register(self.fd, select.EPOLLIN)

        self.on_flag = True
        try:
            while self.on_flag:
                self._start_polling()
        except KeyboardInterrupt:
            pass
        finally:
            self.close_button()

    def stop_interrupt(self):
        self.on_flag = False

    # Method to close de gpio port
    def close_button(self):
        self.line.release()
        self.chip.reset()
        print(f"Closing {self.chip_name} port...")


# Object creation (Example of use)
"""
#button1 = button("button1", button.GPIOB, 10) # Boton izquierdo (atras)
#button1 = button("button1", button.GPIOB, 12)   # Boton derecho (adelante)
#button1 = button("button1", button.GPIOG, 8)   # Boton central (arriba)
button1 = button("button1", button.GPIOB, 13)   # Boton central (abajo)

button1.init_port()

def callback_button1():
    print("Button pressed")

button1.init_interrupt(callback_button1)
"""
