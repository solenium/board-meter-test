from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from .models import web_server
# Create your views here.

from .scripts.update_env_file import update_env_file, reset_mqtt_config

@login_required(login_url='login')  # Redirige a la página de inicio de sesión si el usuario no está autenticado
def settings(request):
    if request.method == 'POST':
        ssid = request.POST['ssid']
        password = request.POST['pass']
        data = {'WIFI_SSID': ssid,
                'WIFI_PASSW': password
            }
        state = update_env_file('network', data)
    
    projects = web_server.objects.all()
    return render(request, "core/wifi.html", {'projects': projects})


def logout_view(request):
    # Implementa la lógica de cierre de sesión aquí si es necesario
    logout(request);
    return redirect('login')  # Redirige a la página de inicio después del cierre de sesión

def login_view(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            # Redirigir al usuario a la página de inicio después de iniciar sesión
            return redirect('home')
        else:
            # Manejar caso de inicio de sesión fallido
            # Puedes agregar un mensaje de error o lógica adicional aquí
            pass

    # Renderizar la página de inicio de sesión (formulario)
    projects = web_server.objects.all()
    return render(request, 'core/login.html', {'projects': projects})

@login_required(login_url='login')  # Redirige a la página de inicio de sesión si el usuario no está autenticado
def home(request):
    projects = web_server.objects.all()
    return render(request, "core/home.html", {'projects': projects})

@login_required(login_url='login')  # Redirige a la página de inicio de sesión si el usuario no está autenticado
def wifi(request):
    if request.method == 'POST':
        ssid = request.POST['ssid']
        password = request.POST['pass']
        data = {'WIFI_SSID': ssid,
                'WIFI_PASSW': password
            }
        state = update_env_file('network', data)

    projects = web_server.objects.all()
    return render(request, "core/wifi.html", {'projects': projects})

@login_required(login_url='login')  # Redirige a la página de inicio de sesión si el usuario no está autenticado
def metering(request):
    projects = web_server.objects.all()
    return render(request, "core/metering.html", {'projects': projects})

@login_required(login_url='login')  # Redirige a la página de inicio de sesión si el usuario no está autenticado
def operation(request):

    if request.method == 'POST':
        # Obtener el valor seleccionado del campo 'mode'
        mode = request.POST.get('mode')

        data = {'OPERATION_MODE': mode.lower()}

        state = update_env_file('config', data)

    projects = web_server.objects.all()
    return render(request, "core/operation.html", {'projects': projects})

@login_required(login_url='login')  # Redirige a la página de inicio de sesión si el usuario no está autenticado
def advanced(request):
    if request.method == 'POST':
        form_name = request.POST.get('formulario_id')
        print(form_name)

        if form_name == 'advanced_set':
            ssid        = request.POST['ssid']
            password    = request.POST['pass']
            ip          = request.POST['ip']
            gw_ip       = request.POST['gw_ip']
            subnet      = request.POST['subnet']
            dns1        = request.POST['dns1']
            dns2        = request.POST['dns2']
            data = {'WIFI_SSID': ssid,
                    'WIFI_PASSW': password,
                    'WIFI_IP': ip,
                    'WIFI_GW_IP': gw_ip,
                    'WIFI_SUBNET': subnet,
                    'WIFI_DNS1': dns1,
                    'WIFI_DNS2': dns2
                        
                    }
            state = update_env_file('network', data)
            print('wifi')
        elif form_name == 'adv-web-set':
            web_server_user = request.POST['web_ssid']
            web_server_pass = request.POST['web_pass']
            data = {
                    'WEBSERVER_USER': web_server_user,
                    'WEBSERVER_PASSW': web_server_pass
                    }
            state = update_env_file('network', data)
            print('webserver')
        elif form_name == 'adv-mqtt-conf':
            server          = request.POST['server']
            port            = request.POST['port']
            user            = request.POST['user']
            mqtt_pass       = request.POST['mqtt_pass']
            data = {
                    'MQTT_SECOND_USER': user,
                    'MQTT_SECOND_SERVER': server,
                    'MQTT_SECOND_PORT': port,
                    'MQTT_SECOND_PASSW': mqtt_pass
                    }
            state = update_env_file('network', data)
            print('mqtt')
        elif form_name == 'mqtt_conf_reset':
            reset_mqtt_config()
            print('reseting')
        else:
            print('nothing')
            return redirect('home')
    projects = web_server.objects.all()
    return render(request, "core/advanced.html", {'projects': projects})