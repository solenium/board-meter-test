from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin

# Create your models here.
class web_server(models.Model):
    title = models.CharField(max_length=200, verbose_name="Titulo")
    description = models.TextField(verbose_name="Descripcion")
    image = models.ImageField(verbose_name="Imagen", upload_to="projects")
    created = models.DateTimeField(auto_now_add=True, verbose_name="Fecha de Creacion")
    updated = models.DateTimeField(auto_now=True, verbose_name="Fecha de edicion")
    link = models.URLField(null=True, blank=True, verbose_name="Direccion Web")

    class Meta:
        verbose_name = "web_server"
        verbose_name_plural = "web_servers"
        ordering = ["-created"]
    
    def __str__(self):
        return self.title
    
