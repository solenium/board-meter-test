from django.contrib import admin
from .models import web_server

# Register your models here.
class web_serverAdmin(admin.ModelAdmin):
    readonly_fields = ("created", "updated")

admin.site.register(web_server, web_serverAdmin)