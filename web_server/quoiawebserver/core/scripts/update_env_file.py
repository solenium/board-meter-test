from dotenv import dotenv_values, set_key

absoute_path        = "/home/root/app/config_files/" #"/home/jutoroa/Workspaces/quoia-mpu/STM32_Firmware/Scripts_Data_Processing_Python/"

# *** // Definicion de funciones // *** #
def set_env(file_name: str, key: str, value: str):
    """This function set a new value for a enviromental variable in the 
    file that the user wants to  write.
    
    Args:
        Configuration file name
        Key that the user wants to edit
        Value: New value  that the user wants in the specified key
    """

    try:
        set_key(f'{absoute_path}.env_{file_name}', key, value)
    except:
        raise ValueError("Check that the .env file is in the config_files folder")

def set_file(file_name, file):
    """This function set a new value for a enviromental variable in the 
    file that the user wants to  write.
    
    Args:
        Configuration file name
    """
    for key, value in file.items():
        set_env(file_name=file_name, key=key, value=value)

def get_env(file_name: str):
    """This functions reads the environmental variables file and loads it
    to the script so the variables can be used.

    Args:
        Configuration file name.

    Returns:
        [Env Object]: [Environmental object of the environ library with the environmental variables]
    """
    try:
        #env = environ.Env(DEBUG=(bool, False))
        env = dotenv_values(f'{absoute_path}.env_{file_name}') #/home/root/app/board-meter/
        while (len(env) == 0):
            env = dotenv_values(f'/home/root/app/config_files/.env_{file_name}')
        return env
    except:
        raise ValueError("Revisa que el archivo de variables de entorno se llame .env y esté en la misma carpera que este script")


def update_env_file(path, data):

    # Read the env file
    env_file = get_env(file_name=path)

    for key,value in data.items():
        env_file[key] = str(value)

    # Save the env file
    set_file(file_name=path, file=env_file)
    return 1
    
def reset_mqtt_config():
    # Read the env file
    env_file = get_env(file_name='network')
    # Update the dictionary
    data = {
            'MQTT_SECOND_USER': env_file['MQTT_DEFAULT_USER'],
            'MQTT_SECOND_SERVER': env_file['MQTT_DEFAULT_SERVER'],
            'MQTT_SECOND_PORT': env_file['MQTT_DEFAULT_PORT'],
            'MQTT_SECOND_PASSW': env_file['MQTT_DEFAULT_PASSW']
            }
    update_env_file('network', data)