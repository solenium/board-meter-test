function showPage(divNumber){
    if (divNumber == 1) {
            document.getElementById('page1').hidden = false;
            document.getElementById('page2').hidden = true;
            document.getElementById('page3').hidden = true;
            document.getElementById('page4').hidden = true;
            document.getElementById('page5').hidden = true;

        } else if (divNumber == 2) {
            document.getElementById('page1').hidden = true;
            document.getElementById('page2').hidden = false;
            document.getElementById('page3').hidden = true;
            document.getElementById('page4').hidden = true;
            document.getElementById('page5').hidden = true;
        } else if (divNumber == 3) {
            document.getElementById('page1').hidden = true;
            document.getElementById('page2').hidden = true;
            document.getElementById('page3').hidden = false;
            document.getElementById('page4').hidden = true;
            document.getElementById('page5').hidden = true;
        } else if (divNumber == 4){
            document.getElementById('page1').hidden = true;
            document.getElementById('page2').hidden = true;
            document.getElementById('page3').hidden = true;
            document.getElementById('page4').hidden = false;
            document.getElementById('page5').hidden = true;
        } else if (divNumber == 5){
            document.getElementById('page1').hidden = true;
            document.getElementById('page2').hidden = true;
            document.getElementById('page3').hidden = true;
            document.getElementById('page4').hidden = true;
            document.getElementById('page5').hidden = false;
        }  
    }
function showOpButtons(){
document.getElementById('op_mode').hidden = !document.getElementById('op_mode').hidden;
}

function instReady () {
    window.location.replace(`/installation_ready`);
}

function mode1 () {
    window.location.replace(`/mode1`);
}

function mode3 () {
    window.location.replace(`/mode3`);
}
function showAdvancedConfig () {
document.getElementById('adv-web-set').hidden = true;
document.getElementById('adv-mqtt-conf').hidden = true;
document.getElementById('advanced_set').hidden = !document.getElementById('advanced_set').hidden;
}

function showWebServerSettings () {

    document.getElementById('adv-mqtt-conf').hidden = true;
    document.getElementById('advanced_set').hidden = true;
    document.getElementById('adv-web-set').hidden = !document.getElementById('adv-web-set').hidden;
}

function showMqttConfig () {
    document.getElementById('advanced_set').hidden = true;
    document.getElementById('adv-web-set').hidden = true;
    document.getElementById('adv-mqtt-conf').hidden = !document.getElementById('adv-mqtt-conf').hidden;
}
function showGraph() {
    document.getElementById('metering-container').hidden = !document.getElementById('metering-container').hidden;
}