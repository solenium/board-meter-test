// Obtener el elemento canvas y crear un contexto
var canvas = document.getElementById('realtime_data');
var ctx = canvas.getContext('2d');

// Ajustar el tamaño del canvas según el tamaño de la ventana
function ajustarTamanoCanvas() {
    canvas.width = window.innerWidth; // Ancho igual al ancho de la ventana
    canvas.height = window.innerHeight; // Alto igual al alto de la ventana
}

// Llamar a la función para ajustar el tamaño del canvas cuando la ventana cambie de tamaño
window.addEventListener('resize', ajustarTamanoCanvas);
ajustarTamanoCanvas(); // Ajustar el tamaño inicial

// Crear un objeto de gráfico de línea
var miGrafico = new Chart(ctx, {
    type: 'line',
    data: {
        labels: [],
        datasets: [{
            label: 'Datos en tiempo real',
            data: [],
            fill: false,
            borderColor: 'blue',
            backgroundColor: 'rgba(128, 128, 128, 1)' // Color de fondo gris medio transparente
        }]
    },
    options: {
        responsive: true,
        maintainAspectRatio: false,
        scales: {
            x: [{
                type: 'linear',
                position: 'bottom'
            }]
        }
    }
});

// Variable para almacenar el identificador del temporizador
var timerId;
// Tamaño máximo del conjunto de datos (10 muestras)
var maxDataSize = 10;
var flagcounter = 0;

// Contador de muestras
var muestraCount = 0;

// Función para generar datos aleatorios y actualizar el gráfico
function actualizarGrafico() {
    // Generar un valor aleatorio
    var nuevoDato = Math.random() + 2;

    // Agregar el nuevo dato a los datos del gráfico
    miGrafico.data.labels.push('');
    miGrafico.data.datasets[0].data.push(nuevoDato);

    // Limitar la cantidad de datos en el gráfico
    if(flagcounter == 0){
        muestraCount++; // Incrementar el contador de muestras
        if (muestraCount > maxDataSize) {
            flagcounter = 1;            
        }
    }
    // Actualizar el gráfico
    miGrafico.update();
    if(flagcounter == 1){
        miGrafico.data.labels.shift();
        miGrafico.data.datasets[0].data.shift();
    }



    // Llamar a esta función nuevamente después de 1000 ms (1 segundo)
    setTimeout(actualizarGrafico, 1000);
}

// Llamar a la función para iniciar la actualización en tiempo real
actualizarGrafico();
