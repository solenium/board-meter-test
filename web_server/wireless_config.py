import os, time
import subprocess
from dotenv import dotenv_values, set_key


# *** // Definicion de funciones // *** #
def set_env(file_name: str, key: str, value: str):
    """This function set a new value for a enviromental variable in the 
    file that the user wants to  write.
    
    Args:
        Configuration file name
        Key that the user wants to edit
        Value: New value  that the user wants in the specified key
    """

    try:
        set_key(f'/home/root/app/config_files/.env_{file_name}', key, value)
    except:
        raise ValueError("Check that the .env file is in the config_files folder")

def set_file(file_name, file):
    """This function set a new value for a enviromental variable in the 
    file that the user wants to  write.
    
    Args:
        Configuration file name
    """
    for key, value in file.items():
        set_env(file_name=file_name, key=key, value=value)

def get_env(file_name: str):
    """This functions reads the environmental variables file and loads it
    to the script so the variables can be used.

    Args:
        Configuration file name.

    Returns:
        [Env Object]: [Environmental object of the environ library with the environmental variables]
    """
    try:
        #env = environ.Env(DEBUG=(bool, False))
        env = dotenv_values(f'/home/root/app/config_files/.env_{file_name}') #/home/root/app/board-meter/
        while (len(env) == 0):
            env = dotenv_values(f'/home/root/app/config_files/.env_{file_name}')
        return env
    except:
        raise ValueError("Revisa que el archivo de variables de entorno se llame .env y esté en la misma carpera que este script")
    
# Se abre el archivo .env
net = get_env(file_name='network')

# Se actualizan los estados de los modos de red (Access Ppint y STA)
state_ap    = (False if net['WIFI_STATE_AP'] == 'False' else True)
state_sta   = (False if net['WIFI_STATE_STA'] == 'False' else True)
state_ap    ^= True
state_sta   ^= True

net['WIFI_STATE_AP']    = str(state_ap)
net['WIFI_STATE_STA']   = str(state_sta)

set_file(file_name='network', file=net)

# Se detienen todos los servicios de red
os.system('systemctl stop hostapd')
os.system('systemctl disable hostapd')

os.system('systemctl stop wpa_supplicant@wlan0.service')
os.system('systemctl disable wpa_supplicant@wlan0.service')

# Se inicia la secuencia de inicialización del access point
if (state_ap == True and state_sta == False):
    # Se actuliza el .service para que inicie en modo AP
    os.system('cp /home/root/app/board-meter/web_server/webserver.service /etc/systemd/system/')
    # Inicia el modo AP
    #os.system('mv /lib/systemd/network/51-wireless.network /lib/systemd/network/51-wireless.network.old')
    os.system('systemctl enable hostapd')
    os.system('systemctl restart systemd-networkd')
    os.system('systemctl start hostapd')
    #TODO: Esperar a que el servivio inicie e iniciar el webserver

    result = subprocess.run('systemctl is-active hostapd', shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    salida_estandar = result.stdout.decode('utf-8').strip()

    while salida_estandar == "active":
        time.sleep(1.5)
        break
    
    # Se inicia el servidor de Django
    os.system('systemctl start webserver.service')
    print("Finalizando programa")
# Se inicia la secuencia de inicialización del STA
elif (state_ap == False and state_sta == True):
    #os.system('mv /lib/systemd/network/51-wireless.network.old /lib/systemd/network/51-wireless.network')
    
    SSID = net['WIFI_SSID']
    PASSW = net['WIFI_PASSW']
    
    os.system(f'wpa_passphrase \'{SSID}\' \'{PASSW}\' >> /etc/wpa_supplicant/wpa_supplicant-wlan0.conf')
    os.system('systemctl enable wpa_supplicant@wlan0.service')
    
    
    os.system('systemctl restart systemd-networkd')
    os.system('systemctl start wpa_supplicant@wlan0.service')

    result = subprocess.run('systemctl is-active wpa_supplicant@wlan0.service', shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    salida_estandar = result.stdout.decode('utf-8').strip()

    while salida_estandar == "active":
        time.sleep(1.5)
        break

    # Se para el servidor de Django
    os.system('systemctl stop webserver.service')
