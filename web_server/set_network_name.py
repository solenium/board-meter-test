import subprocess as sp

with open('/etc/hostapd.conf', 'r') as file:
    lines = file.readlines()

command = ["grep", "Serial" ,"/proc/cpuinfo"]
out=sp.check_output(command)
MAC = out.decode().strip().replace("Serial\t\t: ","")[8:16]

for i in range(len(lines)):
    if lines[i].startswith("ssid="):
        lines[i] = f"ssid=Quoia_Frontera_{MAC}\n"
    elif lines[i].startswith("wpa_passphrase="):
        lines[i] = "wpa_passphrase=88888888\n"



with open('/etc/hostapd.conf', 'w') as file:
    file.writelines(lines)

