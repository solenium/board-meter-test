import os, sys

# Comando a ejecutar: python3 web_server/quoiawebserver/manage.py runserver 192.168.4.1:8000
"""
Archivo de python que se encargará de ejecutar el servicio de Django para el webServer
"""

# Lugar desde donde se va a ejecutar el web server
#sys.path.insert(1, '/home/root/app/board-meter/')
# Se ejecuta el comando para iniciar el web server
os.system('python3 /home/root/app/board-meter/web_server/quoiawebserver/manage.py runserver 192.168.4.1:80')
