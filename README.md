# BorderMeter Firmware Repo

This branch hold all the firmware developed for the STM32MP157F, including the binaries and the corresponding source codes.

## How to clone, pull or push.

Take into account that this project has a git submodule, `webserver`, inside it. So you must follow the nexts indications:

- **When cloning or pulling:** pass the flag `--recurse-submodules` to the `clone` or `pull` command:

```git [clone or pull] --recurse-submodules https://gitlab.com/solenium/firmware/solenium-firmware/quoia/board-meter.git```

Then, checkout to desired branch and update submodules:

```git submodule update --init --recursive --remote```

- **When pushing:** pass the flag `--recurse-submodules=on-demand` to the `push`:

```git push --recurse-submodules=on-demand```

## Content

- [Information](#information)
- [Requirements](#requirements)
- [Use](#use)

## Módulos de utilidades

- `calibration`: script in charge of the calibration process 
- `config_files/`:  Configuration files (.yamls).
- `measurements/`: Files in charge of basic measurements from the ADEx.
- `dissag/`: Dissagregation files, including binaries and source code.
- `web_server/`: Webserver source files.
- `data/`: DataBase location
- `src/`: Libraries location, such as EnergyMeter, mqtt-encoder, etc
- `initiallizer.sh`: bash/python, this file initiallizes all the neccesary scripts
- `README.md`: Este archivo README.


## Requirements

#TODO: Falta hacer el bash/Python que inicializa todos los scripts, este debe estar a la par de las carpetas, es importante que esté ahí, de lo contrario, los scripts fallan por paths
#TODO: Falta hacer los scripts de calibracion
