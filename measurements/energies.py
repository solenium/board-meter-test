 
import numpy as np
import paho.mqtt.client as mqtt
import sysv_ipc


from dotenv import dotenv_values
from dotenv import set_key

from datetime import datetime as dt
import yaml
import base64
import os, sys
import time
import pickle

import sqlite3

#Quoia
sys.path.insert(1, '/home/quoia/app/board-meter/')
#PC
#sys.path.insert(1, '/home/obed/Documents/Solenium/Calibracion/quoia-meter')

from src.mqtt import mqtt_encoder
from src.utils import functions
from measurements.centralizado_with_Htimers import mac
import src.utils_db as db


disconnect_timestamp = 0
connect_timestamp = 0
disconnection_mqtt_flag =  True
connection_mqtt_flag = False
Send_recovery_Data_flag = False

service_topic = "q/mt"+mac+"/s"
energies_topic = "q/mt"+mac+"/en"
energies_topic_gen = "q/gen"+mac+"/en"
#mqtt_broaker = "MQTT_DEFAULT"

PATH_DB = os.path.join(os.path.dirname(os.path.abspath(__file__)))
dir_db = "/home/quoia/app/config_files"

dir_db_data = "/home/quoia/app/database"
name_db = ".metering"
name_config_db = ".central"
measurement_table = "metering_energy"
acumulate_energies = "acumulate"
#energies_table = "energies"
recovery_table = "recovery"

count = 0

#data_array =[[0]*9 for i in range(3)]
recovery_keys = list()


def get_config(table_name: str):
    """This functions reads all the columns values of a configuration/calibration database and saves it into a dictionary.

    Args:
        table_name: Name of the table to read.

    Returns:
        dict: Configuration/calibration variables saved in the table that we want to read
    """
    list_conf_keys = db.return_column_names(directory=dir_db, database=name_config_db, table_name=f"\'{table_name}\'")
    list_conf_values = db.return_values(directory=dir_db, database=name_config_db, table_name=table_name, columns=['*'])
    dict_conf = dict()
    #print(list_conf_values)
    for i in range(len(list_conf_keys) - 1):
        dict_conf[list_conf_keys[i+1][0]] = list_conf_values[0][i+1]
        #print(i)
    return dict_conf



def get_env(file_name: str):
    """This functions reads the environmental variables file and loads it
    to the script so the variables can be used.

    Args:
        Configuration file name.

    Returns:
        [Env Object]: [Environmental object of the environ library with the environmental variables]
    """
    try:
        #env = environ.Env(DEBUG=(bool, False))
        env = dotenv_values(f'/home/quoiaroot/app/config_files/.env_{file_name}')
        while (len(env) == 0):
            env = dotenv_values(f'/home/quoia/app/config_files/.env_{file_name}')
        return env
    except:
        raise ValueError("Revisa que el archivo de variables de entorno se llame .env y esté en la misma carpera que este script")



#with open('config_files/config.yaml','r') as f:
#    config = yaml.safe_load(f)

config = get_config(table_name='configuration')

#with open('config_files/network.yaml','r') as f:
#    net = yaml.safe_load(f)
net = get_config(file_name='network')

#with open('src/mqtt/frames/energy4bytes.yml', 'r') as f:
frame_energy = mqtt_encoder.frame_energy

def on_connect_mqtt(client, userdata, flags, rc):
    """This is a callback function is called once the client is connected to the broker.

    Args:
        MQTT client object.
    """
    #print("mqtt connected")
    #client.publish(service_topic,"{\"message_type\":\"start_up\",\"operation_mode\":2}")
    global disconnect_timestamp, connect_timestamp, disconnection_mqtt_flag, connection_mqtt_flag, Send_recovery_Data_flag
    recovery_flags(mqtt_connected=True)


def on_disconnect_mqtt(client, userdata, rc):
    #print("mqtt disconnected")
    recovery_flags(mqtt_connected=False)

def on_message_mqtt(client, userdata, msg):
    print("arrive msg",msg)

mqtt_loop_pid = 0
def mqtt_loop(client):
    mqtt_loop_pid=os.getpid()
    try:
        client.connect(net['MQTT_SERVER'],net['MQTT_PORT'], keepalive=30)
        client.loop_start()
    except Exception as e:
        print(e)



def buildFrame(data_value: float, pharam: str):
    """This function adds a key and value to a dictionary.

    Args:
        [data_value(list), pharam(str)]: value and key of the dictionary.

    Returns:
        [dict]: dictionary with the key and value
    """
    curr_dict = dict()
    curr_dict[pharam] = str(data_value)
    return curr_dict

def build_db_frame(pharam: str):

    curr_dict = dict()
    curr_dict[pharam] = 'REAL'
    return curr_dict



def recovery_flags(mqtt_connected: bool):
    """This function is called when we have  a  connection or a diisconnection.
    the client just connected to the  MQTT broker, we check the  connection and
    disconnection flag.

    If the disconnection flagh is False and the argument mqtt_connected is False, it means that
    we were previously connected to the broker and we just had a disconnection, so it's necessary to
    save the timestamp

    If the disconnection  flag was true and the argument mqtt_connected is  true,
    it means we were  previously disconnected and we had a  reconnection to the broker,
    in this case, we save this  timestamp  as  connection timestamp, so we will search the data saved
    between  the disconnection  timestamp and connection  timestamp   and we send the data
    to the broker.

    Args:
       [mqtt_connected(bool)]: This argument indicates  if we have connection to the broker.
    """

    global disconnect_timestamp, connect_timestamp, disconnection_mqtt_flag, connection_mqtt_flag, Send_recovery_Data_flag, recovery_keys
    if mqtt_connected == True:
        if(disconnection_mqtt_flag == True and connection_mqtt_flag == False):
            pharams_rc = recovery_keys
            connect_timestamp = int(round(dt.now().timestamp()))
            disconnection_mqtt_flag = False
            connection_mqtt_flag = True

            update_rec_db(disc_ts=None, conn_ts=connect_timestamp, disc_fl=disconnection_mqtt_flag, conn_fl=connection_mqtt_flag, snd_rc=None)
            if (connect_timestamp - disconnect_timestamp) >= 500 and (connect_timestamp - disconnect_timestamp) <= 864000:
                Send_recovery_Data_flag = True
                update_rec_db(disc_ts=None, conn_ts=None, disc_fl=None, conn_fl=None, snd_rc=Send_recovery_Data_flag)
                condition = f"date >= {disconnect_timestamp} AND date < {connect_timestamp}"
                data_to_recover = db.return_values(directory=dir_db_data, database=name_db, table_name=measurement_table, columns=energy_keys, where_clause=condition)
                if (config['TYPE'] == 'Mono'):
                    Send_Data(data_recover=data_to_recover, data_recover_gen= [],consumption=True)
                elif (config['TYPE'] == 'Dual'):
                    data_to_recover_gen = db.return_values(directory=dir_db_data, database=name_db, table_name="energies_gen", columns=energy_keys, where_clause=condition)
                    Send_Data(data_recover=data_to_recover, data_recover_gen= data_to_recover_gen, consumption=False)

            else:
                disconnect_timestamp = 0
                update_rec_db(disc_ts=disconnect_timestamp, conn_ts=None, disc_fl=None, conn_fl=None, snd_rc=False)




    else:
        if (disconnection_mqtt_flag == False and connection_mqtt_flag == True):
            if disconnect_timestamp == 0:
                disconnect_timestamp = int(round(dt.now().timestamp()))
                disconnection_mqtt_flag = True
                connection_mqtt_flag = False


                update_rec_db(disc_ts=disconnect_timestamp, conn_ts=None, disc_fl=disconnection_mqtt_flag, conn_fl=connection_mqtt_flag, snd_rc=None)
            else:
                disconnection_mqtt_flag = True
                connection_mqtt_flag = False
                Send_recovery_Data_flag = False
                update_rec_db(disc_ts=None, conn_ts=None, disc_fl=disconnection_mqtt_flag, conn_fl=connection_mqtt_flag, snd_rc=Send_recovery_Data_flag)

    #TODO: Caso de conexion cuando hay un reinicio del script


def Send_Data(data_recover:list, data_recover_gen: list, consumption: bool):
    """This function sends  the data that we searched between the disconnection and
    connection timestamp in the recovery_flags function. We encode the data to base64,
    and finally we send the data in a dictionary  via MQTT

    Args:
       [data_recover(list), data_recover_gen(list), consumption(bool)]: data_recover is the consumption
       recovery data, data_recover_gen is  the generation recovery data, consumption is  the flag
       that indicates  if the data we are sending is  consumption or generation.

    Returns:
    [int]: Returns 0 if all the data was  sent successfully, returns 1 if it wasn't possible
    to send all the data successfully.
    """

    global disconnect_timestamp, connect_timestamp, disconnection_mqtt_flag, connection_mqtt_flag, Send_recovery_Data_flag
    if (consumption == True):

        for i in range(int(config['PHASES'])):
                    data_reorganized.append([0]*4)

        for sample in data_recover:
            if (Send_recovery_Data_flag == True):
                timestamp_to_send = sample[0]
                data_reorganized = []

                for i in range(int(config['PHASES'])):
                    data_reorganized[i][0] = sample[1+i] #iaepi
                    data_reorganized[i][1] = sample[4+i] #irepi
                    data_reorganized[i][2] = sample[7+i] #eaepi
                    data_reorganized[i][3] = sample[10+i] #erepi

                data_encoded = bytes(mqtt_encoder.encode_energies(data_reorganized,int(config['PHASES'])))
                data_base64 = (base64.encodebytes(data_encoded)).decode('utf-8')
                recovery_frame = dict()
                recovery_frame['timestamp'] = str(timestamp_to_send)
                recovery_frame['Meter_data'] = data_base64

                disconnect_timestamp = timestamp_to_send
                time.sleep(1.0)



            else:

                update_rec_db(disc_ts=disconnect_timestamp, conn_ts=None, disc_fl=None, conn_fl=None, snd_rc=None)
                return 1


        disconnect_timestamp = 0
        Send_recovery_Data_flag = False
        update_rec_db(disc_ts=disconnect_timestamp, conn_ts=None, disc_fl=None, conn_fl=None, snd_rc=Send_recovery_Data_flag)

        return 0

    #Recuperacion dual
    else:
        for sample in range(len(data_recover)):

            if (Send_recovery_Data_flag == True):
                timestamp_to_send = data_recover[sample][0]
                data_reorganized = []
                data_reorganized_gen = []
                for i in range(int(config['PHASES'])):
                    data_reorganized.append([0]*4)
                for i in range(int(config['PHASES'])):
                    data_reorganized[i][0] = data_recover[sample][1+i] #vpi
                    data_reorganized[i][1] = data_recover[sample][4+i] #cpi
                    data_reorganized[i][2] = data_recover[sample][7+i] #appi
                    data_reorganized[i][3] = data_recover[sample][10+i] #rppi


                    data_reorganized_gen[i][0] = data_recover_gen[sample][1+i] #vpi
                    data_reorganized_gen[i][1] = data_recover_gen[sample][4+i] #cpi
                    data_reorganized_gen[i][2] = data_recover_gen[sample][7+i] #appi
                    data_reorganized_gen[i][3] = data_recover_gen[sample][10+i] #rppi


                data_encoded = bytes(mqtt_encoder.encode_metering(data_reorganized))
                data_encoded_gen = bytes(mqtt_encoder.encode_metering(data_reorganized_gen))

                data_base64 = (base64.encodebytes(data_encoded)).decode('utf-8')
                data_base64_gen = (base64.encodebytes(data_encoded_gen)).decode('utf-8')

                recovery_frame = dict()
                recovery_frame['timestamp'] = str(timestamp_to_send)
                recovery_frame['Meter_data'] = data_base64


                #TODO: Enviar los datos codificados en base64 por mqtt

                disconnect_timestamp = timestamp_to_send



            else:

                update_rec_db(disc_ts=disconnect_timestamp, conn_ts=None, disc_fl=None, conn_fl=None, snd_rc=None)
                return 1


        disconnect_timestamp = 0
        Send_recovery_Data_flag = False
        update_rec_db(disc_ts=disconnect_timestamp, conn_ts=None, disc_fl=None, conn_fl=None, snd_rc=Send_recovery_Data_flag)

        return 0


def create_databases(type_quoia: str):
    """This function creates the database if doesn't exist. We  create the recovery table and the
    energy table and acumulate  table,  we update the values for the values of the recovery table at the end.
    """

    global recovery_keys, energy_keys, disconnect_timestamp, connect_timestamp, disconnection_mqtt_flag, connection_mqtt_flag, Send_recovery_Data_flag
    db_dict = dict()
    p = frame_energy

    db_dict['date'] = 'INTEGER'

    for i in range(4):
        for j in range(int(config['PHASES'])):
            curr_dict = build_db_frame(p[(i*3)+j][0])
            db_dict |= curr_dict
        #for j in range(int(config['PHASES'])):
        #    curr_dict = build_db_frame(p[(i*3)+j][0])
        #    db_dict |= curr_dict



    #measurement_keys

    recovery_dict = dict()
    recovery_dict['disc_ts'] = 'INTEGER' #Disconnect timestamp
    recovery_dict['conn_ts'] = 'INTEGER' #Connect timestamp
    recovery_dict['disc_fl'] = 'NUMERIC' #Disconnect MQTT flag
    recovery_dict['conn_fl'] = 'NUMERIC' #Connect timestamp
    recovery_dict['snd_rc'] = 'NUMERIC' #

    recovery_keys = [x for x in recovery_dict.keys()]
    energy_keys = [x for x in db_dict.keys()]


    #Valores por defecto, cuando no haya nada en la base de datos se usarán estos valores
    default_dict = dict()
    default_dict['disc_ts'] = "0"
    default_dict['conn_ts'] = "0"
    default_dict['disc_fl'] = "1"
    default_dict['conn_fl'] = "0"
    default_dict['snd_rc'] = "0"


    #db.create_database(directory=dir_db_data, database=name_db)
    #db.create_table(directory=dir_db_data, database=name_db, table_name=measurement_table, columns=db_dict)
    #db.create_table(directory=dir_db_data, database=name_db, table_name=acumulate_energies, columns=db_dict)

    #db.create_table(directory=dir_db_data, database=name_db, table_name=recovery_table, columns=recovery_dict)

    #rec_pharam = [x for x in recovery_dict.keys()]
    connection_rc = db.return_values(directory=dir_db_data, database=name_db, table_name=recovery_table, columns=recovery_keys)
    if len(connection_rc) == 0:
        db.insert_values(directory=dir_db_data, database=name_db, table_name=recovery_table, values=default_dict)
        connection_rc = db.return_values(directory=dir_db_data, database=name_db, table_name=recovery_table, columns=recovery_keys)

    disconnect_timestamp = connection_rc[0][0]
    connect_timestamp = connection_rc[0][1]
    disconnection_mqtt_flag = bool(connection_rc[0][2])
    connection_mqtt_flag = bool(connection_rc[0][3])
    Send_recovery_Data_flag = bool(connection_rc[0][4])

    if (config['TYPE'] == 'Dual'):
        pass
        #db.create_table(directory=dir_db_data, database=name_db, table_name="measurements_gen", columns=db_dict)


def update_rec_db(disc_ts: int, conn_ts: int, disc_fl: bool, conn_fl: bool, snd_rc: bool):
    """This function updates the disconnection flag, connection flag, disconnection timestamp,
    connection timestamp and send flag in the database.


    Args:
       [disc_ts(int), conn_ts(int), disc_fl(bool), conn_fl(bool), snd_rc(bool)]: disc_ts is the disconnection
       timestamp, conn_ts is the connection timestamp, disc_fl is the disconnection flag
       (we were previously disconnected or not), conn_fl is the connection  mqtt flag
       (we were previously connected or not), snd_rc is the send_flag iis the flag that indicates if  we
       must send the data or not (in some cases this  is not neccesary, for exaple if we just lost
       the connection 5 minutes  we don't have to send the data to  the broker).
    """

    dict_up = dict()
    if disc_ts is not None :
        tmp_dit = dict()
        tmp_dit["disc_ts"] = f"{disc_ts}"
        dict_up |= tmp_dit
    if conn_ts is not None:
        tmp_dit = dict()
        tmp_dit["conn_ts"] = f"{conn_ts}"
        dict_up |= tmp_dit
    if disc_fl is not None:
        tmp_dit = dict()
        tmp_dit["disc_fl"] = f"{int(disc_fl)}"
        dict_up |= tmp_dit
    if conn_fl is not None:
        tmp_dit = dict()
        tmp_dit["conn_fl"] = f"{int(conn_fl)}"
        dict_up |= tmp_dit
    if snd_rc is not None:
        tmp_dit = dict()
        tmp_dit["snd_rc"] = f"{int(snd_rc)}"
        dict_up |= tmp_dit

    conn =  db.update_values(directory=dir_db_data, database=name_db, table_name=recovery_table, values=dict_up, where_clause="id = 1")
    conn.close()


def  Save_Energy_Data(data_to_save: list,  data_to_save_gen: list):
    """This functions saves the acumulated energy  in a single row into a  database. This
    is neccesary in order to update the Energy values in the ADE library attributes.

    Args:
        [data_to_save(list), data_to_save_gen(list)]: data_to_save is the consumption
        energy data and data_to_save_gen is the generation energy data.
    """

    date = int(round(dt.now().timestamp()))

    data_dict = dict()


    data_dict['date'] = date
    #db_dict['date'] = data
    p = frame_energy

    if config['TYPE'] == 'Mono':
        for i in range(4):
            for j in range(int(config['PHASES'])):
                curr_dict = buildFrame(data_to_save[j][i], p[(i*3)+j][0])
                data_dict |= curr_dict

            for k in range(int(config["PHASES"]), 3):
                curr_dict = buildFrame("NULL", p[(i*3)+k][0])
                data_dict |= curr_dict

        connection = db.update_values(directory=dir_db_data, database=name_db, table_name=acumulate_energies, values=data_dict, where_clause="1=1")
        connection.close()


    ##Guardamos los datos de generacion y consumno
    elif config['TYPE'] == 'Dual':

        data_dict_gen = dict()
        data_dict_gen['date'] = date

        for i in range(4):
            for j in range(int(config['PHASES'])):
                curr_dict = buildFrame(data_to_save[j][i], p[(i*3)+j]['var'])
                data_dict |= curr_dict

                curr_dict = buildFrame(data_to_save_gen[j][i], p[(i*3)+j]['var'])
                data_dict_gen |= curr_dict
                #curr_dict = build_db_frame(p[(i*3)+j]['var'])
                #db_dict |= curr_dict


        #db.create_database(directory="./database/", database="metering_db")
        #db.create_table(directory="./database/", database="metering_db", table_name="measurements", columns=db_dict)

        connection = db.update_values(directory=dir_db_data, database=name_db, table_name=acumulate_energies, values=data_dict, where_clause="1=1")
        connection.close()

        connection = db.update_values(directory=dir_db_data, database=name_db, table_name="acumulate_gen", values=data_dict_gen, where_clause="1=1")
        connection.close()

def save_full_DataN(data_to_save: list, data_to_save_gen:  list):
    """This function saves the energy measured data in the energy database.

    Args:
        [data_to_save(list), data_to_save_gen(list)]: data_to_save is the consumption
        energy data and data_to_save_gen is the generation energy data.
    """
    date = int(round(dt.now().timestamp()))

    data_dict = dict()


    data_dict['date'] = date
    #db_dict['date'] = data
    p = frame_energy

    if config['TYPE'] == 'Mono':
        for i in range(4):
            for j in range(int(config['PHASES'])):
                curr_dict = buildFrame(data_to_save[j][i], p[(i*3)+j][0])
                data_dict |= curr_dict

            for k in range(int(config["PHASES"]), 3):
                curr_dict = buildFrame("NULL", p[(i*3)+k][0])
                data_dict |= curr_dict

        connection = db.insert_values(directory=dir_db_data, database=name_db, table_name=measurement_table, values=data_dict)
        connection.close()


    ##Guardamos los datos de generacion y consumno
    elif config['TYPE'] == 'Dual':

        data_dict_gen = dict()
        data_dict_gen['date'] = date

        for i in range(4):
            for j in range(int(config['PHASES'])):
                curr_dict = buildFrame(data_to_save[j][i], p[(i*3)+j]['var'])
                data_dict |= curr_dict

                curr_dict = buildFrame(data_to_save_gen[j][i], p[(i*3)+j]['var'])
                data_dict_gen |= curr_dict
                #curr_dict = build_db_frame(p[(i*3)+j]['var'])
                #db_dict |= curr_dict


        #db.create_database(directory="./database/", database="metering_db")
        #db.create_table(directory="./database/", database="metering_db", table_name="measurements", columns=db_dict)

        connection = db.insert_values(directory=dir_db_data, database=name_db, table_name=measurement_table, values=data_dict)
        connection.close()

        connection = db.insert_values(directory=dir_db_data, database=name_db, table_name="measurements_gen", values=data_dict_gen)
        connection.close()

        # return data_dict
                #sqlite3


def procesar_datos():
    """This function creates the mqtt client, receives the data comming from the queues, waits until the
    configured time is up and sends the energy  variables to the broker.  It also saves the energy variables
    into the energy table in the database, if the variable acumulate is true, it saves    the last energy value
    in the table  acumulate, we use  this table in order toi update the acumulate energy values in the
    ADE library.
    """
    global queue_energies, count, queueconfig
    mqttClient = mqtt.Client("ener"+mac)
    #mqtt_broaker='MQTT_'+config['MQTT_BROKER']
    mqttClient.username_pw_set(username=net['MQTT_USER'],password=net['MQTT_PASSW'])
    mqttClient.on_connect = on_connect_mqtt
    mqttClient.on_disconnect = on_disconnect_mqtt
    mqttClient.on_message = on_message_mqtt
    mqtt_loop(mqttClient)

    while True:
        serialized_list, _ = queue_energies.receive()
        data = pickle.loads(serialized_list)

        try:
            message, msg_type = queueconfig.receive(block = False)
            if message.decode() == "Update":
                print(f"Consumidor: Recibido '{message.decode()}' de la cola")
                config = get_config(table_name="configuration")
            #print(f"Consumidor: Recibido '{message.decode()}' de la cola")
        except:
            pass
        #print("Lista recibida:", data)
        count += 1

        if (config['TYPE'] == 'Dual'):
            try:
                data_to_send=[[x[5],x[6],x[7],x[8]] for x in data[:int(config['PHASES'])]] #Consumo
                print(np.around(np.array(data_to_send),2))
                #mqttClient.publish(reltime_topic,mqtt_encoder.encode_realtime(data_to_send,-300.0*config['ade_config_m1']['ct']['current'],300.0*config['ade_config_m1']['ct']['current'],2))

                data_to_send_2=[[x[5],x[6],x[7],x[8]] for x in data[int(config['PHASES']):]] #Gen
                print(np.around(np.array(data_to_send_2),2))
                if (count == int(config['TIME_ENERGIES'])):
                    #mqttClient.publish(energies_topic,mqtt_encoder.encode_energies(data_to_send))
                    #mqttClient.publish(energies_topic_gen,mqtt_encoder.encode_energies(data_to_send_2))
                    count = 0

            except:
                print("No valid data")

        else:
            #try:
            data_to_send=[[x[5],x[6],x[7],x[8]] for x in data[:int(config['PHASES'])]] #Consumo
            #print(np.around(np.array(data_to_send),2))
            print(f"contador: {count}")
            if (count == int(config['TIME_ENERGIES'])):
                #for i in range(3-int(config['PHASES'])):
                #    data_to_send.append([0]*4)
                mqttClient.publish(energies_topic,mqtt_encoder.encode_energies(data_to_send, int(config['PHASES'])))
                save_full_DataN(data_to_save=data_to_send, data_to_save_gen=[])
                if ((config['ACUMULATE']) == True):
                    pass
                    #TODO: Agregar acumulado de enrgias
                count = 0

            #except:
            #    print("No valid data")
            #    mqttClient.publish(service_topic,"{\"message_type\":\"No valid data\"")


def main():
    """This is the main function, here we create the queues and we call the
    procesar_datos function.
    """
    global queue_energies

    create_databases(config['TYPE'])

    while True:
        try:

            queue_energies = sysv_ipc.MessageQueue(123456)
            queueconfig = sysv_ipc.MessageQueue(1234567890)

            print("Cola de mensajes creada. Comenzando a recibir mensajes.")
            break
        except sysv_ipc.ExistentialError:
            print("La cola no existe todavía. Esperando...")
            time.sleep(1)
    procesar_datos()

# Ejecutar el bucle principal
if __name__=="__main__":
    main()
