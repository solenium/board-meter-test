#!/bin/bash
/usr/bin/python3 /home/root/app/board-meter/measurements/metering.py &
PID1=$!
/usr/bin/python3 /home/root/app/board-meter/measurements/realtime.py &
PID2=$!
# Define una función para terminar los scripts de Python cuando se recibe SIGTERM
terminate_scripts() {
    kill $PID1
    kill $PID2
    exit 0
}

# Atrapa SIGTERM y llama a terminate_scripts
trap terminate_scripts SIGTERM

# Espera a que los scripts de Python terminen
wait $PID1
wait $PID2