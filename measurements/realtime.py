import numpy as np
import paho.mqtt.client as mqtt
import sysv_ipc

import yaml
import os, sys
import time
import pickle
from dotenv import dotenv_values
from  dotenv import set_key

sys.path.insert(1, '/home/root/app/board-meter/')

from src.mqtt import mqtt_encoder
from src.utils import functions
from measurements.centralizado_with_Htimers import mac
from src import utils_db as db
#from centralizado import mac
# Crea una instancia del cliente Redis
#mac = "165561561"
#print(f'MAC: {mac}')
service_topic = "q/mt"+mac+"/s"
#control_topic = "quoia/production/server/home/energy/mt"+mac+"/control"
reltime_topic = "q/mt"+mac+"/rt"
reltime_topic_gen = "q/gen"+mac+"/rt"
mqtt_broaker = "MQTT_DEFAULT"
PATH_DB = os.path.join(os.path.dirname(os.path.abspath(__file__)))
dir_db = "/home/root/app/config_files"

name_config_db = "config_values"

def get_config(table_name: str):
    """This functions reads all the columns values of a configuration/calibration database and saves it into a dictionary.

    Args:
        table_name: Name of the table to read.

    Returns:
        dict: Configuration/calibration variables saved in the table that we want to read
    """

    list_conf_keys = db.return_column_names(directory=dir_db, database=name_config_db, table_name=f"\'{table_name}\'")
    list_conf_values = db.return_values(directory=dir_db, database=name_config_db, table_name=table_name, columns=['*'])
    dict_conf = dict()
    #print(list_conf_values)
    for i in range(len(list_conf_keys) - 1):
        dict_conf[list_conf_keys[i+1][0]] = list_conf_values[0][i+1]
        #print(i)      
    return dict_conf

def get_env(file_name: str):
    """This functions reads the environmental variables file and loads it
    to the script so the variables can be used.

    Args:
        Configuration file name.

    Returns:
        [Env Object]: [Environmental object of the environ library with the environmental variables]
    """
    try:
        #env = environ.Env(DEBUG=(bool, False))
        env = dotenv_values(f'/home/root/app/config_files/.env_{file_name}')
        while (len(env) == 0):
            env = dotenv_values(f'/home/root/app/config_files/.env_{file_name}')
        return env
    except:
        raise ValueError("Revisa que el archivo de variables de entorno se llame .env y esté en la misma carpera que este script")

#with open('config_files/config.yaml','r') as f:
#    config = yaml.safe_load(f)

config = get_config(table_name="configuration")


#with open('config_files/network.yaml','r') as f:
#    net = yaml.safe_load(f)

net = get_env(file_name="network")

def on_connect_mqtt(client, userdata, flags, rc):
    print("mqtt connected")
    #client.publish(service_topic,"{\"message_type\":\"start_up\",\"operation_mode\":2}")

def on_disconnect_mqtt(client, userdata, rc):
    print("mqtt disconnected")

def on_message_mqtt(client, userdata, msg):
    print("arrive msg",msg)

mqtt_loop_pid = 0
def mqtt_loop(client):
    mqtt_loop_pid=os.getpid()
    try:
        client.connect(net[mqtt_broaker + '_SERVER'],int(net[mqtt_broaker + '_PORT']))
        client.loop_start()
    except Exception as e:
        print(e)


def procesar_datos():
    """This function set the connection with the MQTT  broker, afther that
    it will search if there is an element in the POSYX queue, once it reads data from the
    POSYX queue, it will send the data to the MQTT broker.
    """
    global mqtt_broaker, queueconfig
    mqttClient = mqtt.Client("rt"+mac)
    mqtt_broaker='MQTT_'+config['MQTT_BROKER']
    print(f"Broker: {mqtt_broaker}")
    mqttClient.username_pw_set(username=net[mqtt_broaker + '_USER'],password=net[mqtt_broaker + '_PASSW'])
    mqttClient.on_connect = on_connect_mqtt
    mqttClient.on_disconnect = on_disconnect_mqtt
    mqttClient.on_message = on_message_mqtt
    mqtt_loop(mqttClient)
    
    while True:
        serialized_list, _ = queue_rt.receive() #Reads the data  in the queue and saves it into the serialized_list  variable
        data = pickle.loads(serialized_list) #Read the data in a list object
        #print("Lista recibida:", data)
        
        try:
            message, msg_type = queueconfig.receive(block = False)
            if message.decode() == "Update":
                print(f"Consumidor: Recibido '{message.decode()}' de la cola")
                config = get_config(table_name="configuration")
            #print(f"Consumidor: Recibido '{message.decode()}' de la cola")
        except:
            pass
        #When the device is a Dual meter, in the first version of Quoia Frontera, the meter is Mono
        if (config['TYPE'] == 'Dual'): 
            try:
                data_to_send=[[x[2],x[3]] for x in data[:int(config['PHASES'])]] #Consumo
                print(np.around(np.array(data_to_send),2))
                #mqttClient.publish(reltime_topic,mqtt_encoder.encode_realtime(data_to_send,-300.0*config['ade_config_m1']['ct']['current'],300.0*config['ade_config_m1']['ct']['current'],2))
            
                data_to_send_2=[[x[2],x[3]] for x in data[int(config['PHASES']):]] #Gen
                print(np.around(np.array(data_to_send_2),2))
                #mqttClient.publish(reltime_topic_gen,mqtt_encoder.encode_realtime(data_to_send_2,-300.0*config['ade_config_m2']['ct']['current'],300.0*config['ade_config_m2']['ct']['current'],2))
            
            except:
                print("No valid data")   
        
        #Mono Meter
        else:
            try:
                data_to_send=[[x[2],x[3]] for x in data[:int(config['PHASES'])]] #Reads  the active and reactive power for each phase
                #print(np.around(np.array(data_to_send),2))
                mqttClient.publish(reltime_topic,mqtt_encoder.encode_realtime(data_to_send,-300.0*int(config['ADE_CONFIG_M1_CT_CURRENT']),300.0*int(config['ADE_CONFIG_M1_CT_CURRENT']),2))
        
            except:
                print("No valid data")
                mqttClient.publish(service_topic,"{\"message_type\":\"No valid data\"")

        
        
            
        


# Ejecutar el bucle principal
def main():
    """This function creates the POSYX queues, once the queue is created in the centralizado script, 
    it runs the procesar_datos function.
    """
    global queue_rt
            
    while True:
        try:

            queue_rt = sysv_ipc.MessageQueue(1234)
            queueconfig = sysv_ipc.MessageQueue(12345678)

            print("Cola de mensajes creada. Comenzando a recibir mensajes.")
            break
        except sysv_ipc.ExistentialError:
            print("La cola no existe todavía. Esperando...")
            time.sleep(1)
    procesar_datos()

# Ejecutar el bucle principal
if __name__=="__main__":
    main()
