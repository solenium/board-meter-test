# Import for GPIO management
import gpiod
# Import for polling
import select
#import for init hardware timer
import subprocess
# Other imports
import time, os,sys, yaml, csv, signal
from dotenv import dotenv_values
from dotenv import set_key


import paho.mqtt.client as mqtt
import numpy as np
from threading import Timer, Thread

from datetime import datetime as dt 
import sysv_ipc
import pickle


sys.path.insert(1, '/home/root/app/board-meter/')
from src.EnergyMeter import EnergyMeter
from src import utils_db as db
from src.utils import functions
#TODO (obed@solenium.co): Cambiar los nombres de los archivos por snake_case de python
mac = ""

PATH_DB = os.path.join(os.path.dirname(os.path.abspath(__file__)))
dir_db = "/home/root/app/config_files"
name_config_db = "config_values"


def get_config(table_name: str):
    """This functions reads all the columns values of a configuration/calibration database and saves it into a dictionary.

    Args:
        table_name: Name of the table to read.

    Returns:
        dict: Configuration/calibration variables saved in the table that we want to read
    """

    list_conf_keys = db.return_column_names(directory=dir_db, database=name_config_db, table_name=f"\'{table_name}\'")
    list_conf_values = db.return_values(directory=dir_db, database=name_config_db, table_name=table_name, columns=['*'])
    dict_conf = dict()
    #print(list_conf_values)
    for i in range(len(list_conf_keys) - 1):
        dict_conf[list_conf_keys[i+1][0]] = list_conf_values[0][i+1]
        #print(i)      
    return dict_conf

def set_env(file_name: str, key: str, value: str):
    """This function set a new value for a enviromental variable in the 
    file that the user wants to  write.
    
    Args:
        Configuration file name
        Key that the user wants to edit
        Value: New value  that the user wants in the specified key
    """

    try:
        set_key(f'/home/root/app/config_files/.env_{file_name}', key, value)
    except:
        raise ValueError("Check that the .env file is in the  config_files folder")


def get_env(file_name: str):
    """This functions reads the environmental variables file and loads it
    to the script so the variables can be used.

    Args:
        Configuration file name.

    Returns:
        [Env Object]: [Environmental object of the environ library with the environmental variables]
    """
    try:
        #env = environ.Env(DEBUG=(bool, False))
        env = dotenv_values(f'/home/root/app/config_files/.env_{file_name}')
        while (len(env) == 0):
            env = dotenv_values(f'/home/root/app/config_files/.env_{file_name}')
        return env
    except:
        raise ValueError("Revisa que el archivo de variables de entorno se llame .env y esté en la misma carpera que este script")
    



# Crea una instancia del cliente Redis
#with open('config_files/config.yaml','r') as f:
#    config = yaml.safe_load(f)

config = get_config(table_name="configuration")
def write_mac():
    return functions.get_ID()

#We read if the mac column is null in the database, if it's null, we search the MAC of the device and save it into the database
if (config['MAC'] == 'null'):
    mac = write_mac()

    config['MAC'] = mac
    conn = db.update_values(directory=dir_db, database=name_config_db, table_name="configuration", values={'MAC': mac}, where_clause="1 = 1")
    conn.close()
    
                
mac = str(config['MAC'])
print(f'MAC: {mac}')


def readSample(energy_meter):
    """This functions reads the Electrical variables in the ADE and saves the data into a list.

    Args:
        [Meter_I2C Object]: ADE object of the electrical variables sensor.

    Returns:
        [list or int]: List with the data read from the ADE, 0 if there was an error during reading of the data.
    """
    if(energy_meter.meter_type=='Dual'):
        global average_counter_gen
        data =energy_meter.Full_DataN()
        data_array = data
        data =energy_meter.Full_DataN('gen')
        data_array.extend(data)
        

    else:
        try:
            data =energy_meter.Full_DataN()
            
            data_array = data
    
            return data_array
        except:
            print("error en la lectura de los datos")
            return 0

# Función asincrónica para establecer la conexión con Redis y publicar datos en el canal
def conectar_y_publicar(arg):
    """This functions sends the electrical data with POSYX queues to the electrical, metering and realtime scripts.

    Args:
        [Meter_I2C Object]: ADE object of the electrical variables sensor.

    """

    #We  check if it's necessary to update parameters, message comming from MQTT
    try:
        message, msg_type = message_queue.receive(block = False)
        if message.decode() == "Update":
            print(f"Consumidor: Recibido '{message.decode()}' de la cola")
            Energy_Meter.update_var()

        #print(f"Consumidor: Recibido '{message.decode()}' de la cola")
    except:
        pass
    data = readSample(arg)
    #print(f'Datos enviados al canal sensor_channel: {data}', 2)
    #print(f"Datos enviados al canal sensor_channel: {np.around(np.array(data), 2)}")
    try:
        queue_rt.send(pickle.dumps(data),block=False, type=1)
        queue_mt.send(pickle.dumps(data), block=False, type=1)
        queue_ener.send(pickle.dumps(data), block=False, type=1)
    except:
        print("Error al enviar los datos de realtime y metering")
        queue_ener.send(pickle.dumps(data))
    #time.sleep(1)


    
# Code from the main cycle
def main():
    #print("BOTON PULSADO")
    conectar_y_publicar(Energy_Meter)

def initTimer(period):
    """ 
    @brief Function to init de PWM (Hardware timer)
    @param period Time in seconds between timer calls
    """
    flag = subprocess.run(["echo 0 > /sys/class/pwm/pwmchip0/export"], shell=True, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)    # Generate the PWM Channel 1
    if(flag.returncode == 0):
        print("Timer initialized")
    else:
        print("Timer already initialized")
    subprocess.run(["echo 0 > /sys/class/pwm/pwmchip0/pwm0/enable"], shell=True)           # Stop the timer, to give write access
    period_ns = int(period * (1e9))
    duty      = int(period_ns / 2)
    subprocess.run([f"echo {period_ns} > /sys/class/pwm/pwmchip0/pwm0/period"], shell=True) # Set the period
    subprocess.run([f"echo {duty} > /sys/class/pwm/pwmchip0/pwm0/duty_cycle"], shell=True)  # Set the duty cycle
    subprocess.run(["echo 1 > /sys/class/pwm/pwmchip0/pwm0/enable"], shell=True)            # Enable the PWM Channel 1 (Start the timer)
    

def InfiniteTimer(period):
    """A Timer class that does not stop, unless you want it to.
    @param period Time in seconds between timer calls
    """

    # Global defines
    pinPWM  = 14
    portPWM = 4     # GPIOE = 4 -> Pin PE14

    # Configuration for the EXTI line
    chip_name = f"/dev/gpiochip{portPWM}"
    chip = gpiod.chip(chip_name, gpiod.chip.OPEN_BY_PATH)       # GPIO chip configuration
    line = chip.get_line(pinPWM)                                # GPIO line configuration
    # Struct for configuration
    config_line              = gpiod.line_request()
    config_line.consumer     = "Exti_event"
    config_line.flags        = gpiod.line_request.FLAG_BIAS_PULL_UP
    config_line.request_type = gpiod.line_request.EVENT_FALLING_EDGE
    line.request(config_line)                               # Initializate the line 
    fd = line.event_get_fd()                                # Get file descriptor

    poll = select.poll()                                    # Create poll object
    poll.register(fd, select.EPOLLIN)                       # Register file descriptor (Interruption)

    initTimer(period)                                       # Init the PWM (Hardware timer)

    try:
        while True:
            poll.poll(None)                                # Polling for events (Infinite Time)
            event = line.event_read()                      # Read the event (to turn off the flag)
            # Main code
            #print("Boton pulsado")
            main()
    except KeyboardInterrupt:
        line.release()
        chip.reset()
        poll.unregister()
        print("Finishing program...")


# Principal code
if __name__ == "__main__":
    
    # Abrir la cola de mensajes existente
    while True:
        try:
            message_queue = sysv_ipc.MessageQueue(1234567)
            break
        except:
            print("La cola de MQTT no existe todavía. Esperando...")
            time.sleep(1.0)

    try:
        Energy_Meter = EnergyMeter.Meter_I2C()
        Energy_Meter.Initial_Setup(False, True)
        

        queue_rt = sysv_ipc.MessageQueue(1234, sysv_ipc.IPC_CREAT)
        queue_mt = sysv_ipc.MessageQueue(12345, sysv_ipc.IPC_CREAT)
        queue_ener = sysv_ipc.MessageQueue(123456, sysv_ipc.IPC_CREAT)

        InfiniteTimer(int(config['TIME_REALTIME']))

        print("Program finished.")

    except sysv_ipc.ExistentialError:
        print("Error al inicializar las colas o el ADE")
        sys.exit(0)