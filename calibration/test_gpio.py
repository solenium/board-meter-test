# Import for GPIO management
import gpiod
# Import for polling
import select
import yaml
import os
import time


# Global defines
GPIOA = 0
GPIOB = 1
GPIOC = 2
GPIOD = 3
GPIOE = 4
GPIOF = 5
GPIOG = 6
GPIOH = 7
GPIOI = 8

# Global definitions
pin1_Number = 7
pin1_Port   = GPIOC


# Definiciones globales
chip_name = f"/dev/gpiochip{pin1_Port}"

# Configuración del chip GPIO
chip = gpiod.chip(chip_name, gpiod.chip.OPEN_BY_PATH)

# Configuración de la línea GPIO
line = chip.get_line(pin1_Number)

# Struct for configuration
config              = gpiod.line_request()
config.consumer     = "Exti_event"
config.flags        = gpiod.line_request.FLAG_BIAS_PULL_UP
config.request_type = gpiod.line_request.DIRECTION_OUTPUT

# Initializate the line
line.request(config)
# Get file descriptor
fd = line.event_get_fd()
line.set_value(0)
