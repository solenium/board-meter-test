import numpy as np
import yaml, environ
import base64
from dotenv import dotenv_values
from dotenv import set_key

import os, sys

import paho.mqtt.client as mqtt
import sysv_ipc


from datetime import datetime as dt

import time
import pickle
import sqlite3
import json
#TODO: Agregar lectura de la mac al arranque
#TODO: Agregar comando  para pasar a modo de un modo a otro por MQTT, el script debe parar los scripts de medicion y ejecutar el script de calibracion
sys.path.insert(1, '/home/root/app/board-meter/')
from src.EnergyMeter import EnergyMeter
from src.utils import functions
from src.infinite_Timer import InfiniteTimer
import src.utils_db as db
from src.mqtt import mqtt_encoder
from measurements.centralizado_with_Htimers import mac

#hsfdsdfdsjfsf

#with open('src/mqtt/frames/metering4bytes.yml', 'r') as f:
#    frame_metering = yaml.safe_load(f)
#    f.close()

#with open('config_files/config.yaml','r') as f:
#    config = yaml.safe_load(f)
#    f.close()
#
#with open('config_files/network.yaml','r') as f:
#    net = yaml.safe_load(f)
#    f.close()
#
#with open('config_files/calibration.yaml', 'r') as f:
#    calibration = yaml.safe_load(f)
#    f.close()





service_topic = "quoia/production/server/home/energy/mt"+mac+"/service"
control_topic = "quoia/production/device/home/energy/mt"+mac+"/control"
mqtt_broaker = "MQTT_DEFAULT"

PATH_DB = os.path.join(os.path.dirname(os.path.abspath(__file__)))
dir_db = "/home/root/app/config_files"
name_config_db = "config_values"

Settings_Received = False
get_calibration_flag = False

Gains = [0]*4
LSB = [0]*4
Phcal = [0]*3
PGains = [0]*3
Phi = 0.0
WH = 0
WH_1 = 0.0
Wh_lsb = 0
Wac = 0
Data_standard_measurement = [0]*6
Data_Quoia = [0]*18

Flag1 = False
Flag_validation = False
Validation_full = None
Vol = 0
Cur = 0

def get_config(table_name: str):
    list_conf_keys = db.return_column_names(directory=dir_db, database=name_config_db, table_name=f"\'{table_name}\'")
    list_conf_values = db.return_values(directory=dir_db, database=name_config_db, table_name=table_name, columns=['*'])
    dict_conf = dict()
    #print(list_conf_values)
    for i in range(len(list_conf_keys) - 1):
        dict_conf[list_conf_keys[i+1][0]] = list_conf_values[0][i+1]
        #print(i)      
    return dict_conf

def set_env(file_name: str, key: str, value: str):
    """This function set a new value for a enviromental variable in the 
    file that the user wants to  write.
    
    Args:
        Configuration file name
        Key that the user wants to edit
        Value: New value  that the user wants in the specified key
    """

    try:
        set_key(f'/home/root/app/config_files/.env_{file_name}', key, value)
    except:
        raise ValueError("Check that the .env file is in the  config_files folder")


def get_env(file_name: str):
    """This functions reads the environmental variables file and loads it
    to the script so the variables can be used.

    Args:
        Configuration file name.

    Returns:
        [Env Object]: [Environmental object of the environ library with the environmental variables]
    """
    try:
        #env = environ.Env(DEBUG=(bool, False))
        env = dotenv_values(f'/home/root/app/config_files/.env_{file_name}')
        while (len(env) == 0):
            env = dotenv_values(f'/home/root/app/config_files/.env_{file_name}')
        return env
    except:
        raise ValueError("Revisa que el archivo de variables de entorno se llame .env y esté en la misma carpera que este script")


config = get_config(table_name='configuration')
net = get_env("network")

def stop_acumulate(meter: EnergyMeter.Meter_I2C):
        meter.Flag_step4 = True

def on_connect_mqtt(client, userdata, flags, rc):
    global get_calibration_flag
    
    client.subscribe(control_topic)
    client.publish(service_topic,"{\"message_type\":\"get_calibration_settings\",\"type\":\"ind\"}")
    
    get_calibration_flag = True
    print("Connected")
    

    
def on_message_mqtt(client, userdata, msg):
    global Settings_Received, ct_t, Vol, Cur, Flag1, Phi, WH_1, WH, Wh_lsb, Wac, Data_standard_measurement, Flag_validation, Validation_full
    print("arrive msg",msg)
    dev_stage = msg.topic.split('/')[1]
    device_type = msg.topic.split('/')[3]
    meter_id = msg.topic.split('/')[-2]

    try:
        d = json.loads(msg.payload)
    except json.decoder.JSONDecodeError:
        print("MQTT payload received was not JSON-compatible")
        return

    #response_topic = f"quoia/{dev_stage}/device/{device_type}/energy/{meter_id}/control"

    try:
        stage = d['message_type']
    except KeyError:
        return
    
    if stage == 'set_calibration_settings':
        try:
            
            new_config = dict()
            new_config['ADE_CONFIG_M1_CT_CURRENT'] = str(d['ct_c'])

            new_config['ADE_CONFIG_M1_CT_RATIO'] = '0.353553391'
                


            new_config["PHASES"] = str(d["w_p"])
            new_config["NOMINAL_VOLTAGE"] = str(d["n_v"])

            new_config["SETTINGS_FLAG"] = str(d["settings_flag"])
            Settings_Received = True

            for key, value in new_config.items():
                conn = db.update_values(directory=dir_db, database=name_config_db, table_name="configuration", values=new_config, where_clause="id=1")
                conn.close()
            
            return

        except json.decoder.JSONDecodeError:
            print(f"MQTT payload received was not JSON-compatible")
            return

    elif stage == "step2":
        try:
            Vol = d["vol"]
            Cur = d["cur"]
            Flag1 = True
        except json.decoder.JSONDecodeError:
            print("Error parsing mqtt message")
        return
    
    elif stage == "step3":
        try:
            Phi = d["phi"]
            Flag1 = True

        except json.decoder.JSONDecodeError:
            print("Error parsing mqtt message")

    elif stage == "step4":
        try:
            WH_1 = d["wh_1"]
            Flag1 = True
        except json.decoder.JSONDecodeError:
            print("Error parsing mqtt message")

    elif stage == "step4_1":
        try:
            WH = d["wh"]
            Wh_lsb = d["wh_lsb"]
            Flag1 = True
        except json.decoder.JSONDecodeError:
            print("Error parsing mqtt message")
    
    elif stage == "step5":
        print("Step5 received")
        try:
            Wac = d["wac"]
            Flag1 = True
        except json.decoder.JSONDecodeError:
            print("Error parsing mqtt message")
        print(f"Flag after step5: {Flag1}")
            
    elif stage == "validation":
        print("validation received")
        try:

            Data_standard_measurement[0] = d["vol"]
            Data_standard_measurement[1] = d["cur"]
            Data_standard_measurement[2] = d["p"]
            Data_standard_measurement[3] = d["q"]
            Data_standard_measurement[4] = d["iae"]
            Data_standard_measurement[5] = d["ire"]
            fp = d["pf"]
            print(f"Power factor carga: {fp}")
            Flag_validation = True

            print("Validation received correctly")
        except json.decoder.JSONDecodeError:
            print("Error parsing mqtt message")

    elif stage == "validate":
        try:
            Validation_full = d["value"]

        except json.decoder.JSONDecodeError:
            print("Error parsing mqtt message")


    else:
        return


mqtt_loop_pid = 0
def mqtt_loop(client):
    global mqtt_loop_pid
    mqtt_loop_pid=os.getpid()
    print("Trying to connect")
    try:
        client.connect(host=net[mqtt_broaker+'_SERVER'], port=int(net[mqtt_broaker+'_PORT']), keepalive=30)
        client.loop_start()
    except Exception as e:
        print(e)

def Step1(gains: list, meter: EnergyMeter.Meter_I2C):
    print("Step 1")
    meter.AutoCalibration1(gains)

def Step2(lsb: float, client: mqtt.Client, meter: EnergyMeter.Meter_I2C):
    global LSB, Vol, Cur, Flag1
    print("Step 2")
    client.publish(service_topic,"{\"message_type\":\"step2\"}")
    count = 0
    while(count <= 15):
        count += 1
        if (Flag1 == True):
            break
        time.sleep(1.0)
    
    if Flag1 == True:
        Flag1 = False
    else:
        Flag1 = True
        return
    
    meter.AutoCalibration2(lsb=LSB, V_input=Vol, C_input=Cur)


    
def Step3(Ph: list, client: mqtt.Client,  meter: EnergyMeter.Meter_I2C):
    global Phi, Flag1
    print("step3")
    meter.AutoCalibration3()
    client.publish(service_topic, "{\"message_type\":\"step3\"}")
    count = 0
    while(count <= 15):
        count += 1
        if(Flag1 == True):
            break
        
        time.sleep(0.1)
    if (Flag1 == True):
        Flag1 = False
    else:
        Flag1 = True
    
    meter.AutoCalibration3_1(Phcal=Ph, Phi=Phi)

def Step4(client: mqtt.Client, meter: EnergyMeter.Meter_I2C, timer_acumulate: InfiniteTimer):
    global Flag1
    client.publish(service_topic, "{\"message_type\":\"step4\"}")
    print("step 4")
    count = 0
    while(count <= 15):
        if Flag1 == True:
            break
        count += 1
        time.sleep(0.1)

    if Flag1 == True:
        Flag1 = False
    
    else:
        Flag1 = True
        return
    meter.AutoCalibration4(Time_acumulate=30, delay_acumulate=timer_acumulate)
    

def Step4_1(client: mqtt.Client, meter: EnergyMeter.Meter_I2C):
    global LSB, PGains, Flag1, WH, WH_1, Wh_lsb
    print("step 4_1")
    request = {
        "message_type": "step4_1",
        "meter": 1
    }

    try:
        client.publish(service_topic, json.dumps(request))
    except Exception as e:
        print("Error: ", e)

    count = 0
    while(count <= 15):
        if Flag1 == True:
            break
        count += 1
        time.sleep(0.1)

    if Flag1 == True:
        Flag1 = False
    
    else:
        Flag1 = True
        return

    WH = WH - WH_1
    meter.AutoCalibration4_1(lsb=LSB, Pga=PGains, WH_input=WH, wh_lsb_input=Wh_lsb, FirtsMeter=True)

def Step5(client: mqtt.Client, meter: EnergyMeter.Meter_I2C):
    global LSB, Flag1
    print("step 5")
    
    client.publish(service_topic, "{\"message_type\":\"step5\"}")
    count = 0
    while count <= 15:
        count += 1
        if Flag1 == True:
            break
        time.sleep(0.5)
    
    print(f"Flag1 after request: {Flag1}")
    if Flag1 == True:
        Flag1 = False

    else:
        Flag1 = True
        return
    meter.AutoCalibration5(lsb=LSB, Wa_input=Wac)

def Validation_mqtt(client: mqtt.Client, meter: EnergyMeter.Meter_I2C):
    global Flag1, Flag_validation
    print("Validation")
    client.publish(service_topic, "{\"message_type\":\"all_data\"}")
    count = 0
    while count <= 15:
        count += 1
        if Flag1 == True:
            break
        time.sleep(0.5)
    if Flag1 == True:
        Flag1 = False
    else:
        Flag1 = True
        return

def Send_CSV(client: mqtt.Client):
    global Gains, Phcal, PGains, LSB
    response = dict()
    response["message_type"] = "auto_cv"

    response["vgb1"] = Gains[0]
    response["igb1"] = Gains[2]
    response["vgc1"] = Gains[1]
    response["igc1"] = Gains[3]

    response["pga1"] = Phcal[0]
    response["pgb1"] = Phcal[1]
    response["pgc1"] = Phcal[2]

    response["poga1"] = PGains[0]
    response["pogb1"] = PGains[1]
    response["pogc1"] = PGains[2]

    response["v"] = LSB[0]
    response["c"] = LSB[1]
    response["wh"] = LSB[2]
    response["wac"] = LSB[3]

    client.publish(service_topic, json.dumps(response))


def Save_CV(meter: int):
    if meter == 1:
        #Ganancias
        calibration  =  get_config(table_name='calibration')
        calibration['VGB_M1'] = str(Gains[0])
        calibration['IGB_M1'] = str(Gains[2])
        calibration['VGC_M1'] = str(Gains[1])
        calibration['IGC_M1'] = str(Gains[3])

        #PGains
        calibration['POGA_M1'] = str(PGains[0])
        calibration['POGB_M1'] = str(PGains[1])
        calibration['POGC_M1'] = str(PGains[2])

        #Phcals
        calibration['PGA_M1'] = str(Phcal[0])
        calibration['PGB_M1'] = str(Phcal[1])
        calibration['PGC_M1'] = str(Phcal[2])

        #LSB
        calibration['LSB_VOL_M1'] =  str(LSB[0])
        calibration['LSB_CURR_M1'] = str(LSB[1])
        calibration['LSB_WH_M1'] =   str(LSB[2])
        calibration['LSB_WAC_M1'] =  str(LSB[3])
        
        for key, value in calibration.items():
            conn = db.update_values(directory=dir_db, database=name_config_db, table_name="calibration", values=calibration, where_clause="id=1")
            conn.close()
        
        




def Save_ADE_Calibration(meter: int):
    if meter == 1:
        #TODO: Guardar el ADE que se calibró
        pass



    
def Auto_Calibration(error: float, client: mqtt.Client, energymeter: EnergyMeter.Meter_I2C, timer: InfiniteTimer):
    global Gains, Flag1, Phcal, Data_Quoia
    Step1(Gains, energymeter)

    Step2(LSB, client=client, meter=energymeter)

    if (Flag1 == True):
        client.publish(service_topic,"{\"message_type\":\"step2\",\"calibration_error\":\"step 2 not answer received\"}")
        return 0 

    Step3(Ph=Phcal, client=client, meter=energymeter)

    if (Flag1 == True):
        client.publish(service_topic,"{\"message_type\":\"step3\",\"calibration_error\":\"step 3 not answer received\"}")
        return 0
    
    Step4(client=client, meter=energymeter, timer_acumulate=timer)
    if Flag1 == True:
        client.publish(service_topic,"{\"message_type\":\"step4\",\"calibration_error\":\"step 4 not answer received\"}")
        return 0 

    Step4_1(client=client, meter=energymeter)
    if(Flag1 == True):
        client.publish(service_topic,"{\"message_type\":\"step4_1\",\"calibration_error\":\"step step4_1 not answer received\"}")
        return 0 
    
    Step5(client=client, meter=energymeter)
    if Flag1 == True:
        client.publish(service_topic,"{\"message_type\":\"step5\",\"calibration_error\":\"step step5 not answer received\"}")
        return 0 

    Validation_mqtt(client=client, meter=energymeter)
    time.sleep(5)
    global Flag_validation
    energymeter.Calibration_Validation(Data_Array=Data_Quoia)

    val_v = 0
    val_c = 0
    val_p = 0
    val_q = 0


    if Flag_validation:
        val_v = abs((Data_standard_measurement[0] - Data_Quoia[0]) / Data_standard_measurement[0])*100
        val_c = abs((Data_standard_measurement[1] - Data_Quoia[3]) / Data_standard_measurement[1])*100
        val_p = abs((Data_standard_measurement[2] - Data_Quoia[6]) / Data_standard_measurement[2])*100
        val_q = abs((Data_standard_measurement[3] - Data_Quoia[9]) / Data_standard_measurement[3])*100

    print(f"Vol expected: {Data_standard_measurement[0]}, Vol measured: {Data_Quoia[0]}, Error: {val_v} \n")
    print(f"Curr expected: {Data_standard_measurement[1]}, Curr measured: {Data_Quoia[3]}, Error: {val_c} \n")
    print(f"App expected: {Data_standard_measurement[2]}, App measured: {Data_Quoia[6]}, Error: {val_p} \n")
    print(f"Rpp expected: {Data_standard_measurement[3]}, Rpp measured: {Data_Quoia[9]}, Error: {val_q} \n")


    Flag_validation = False

    if (val_v > error and val_c > error and val_p > error and val_q > error):
        
        return 0
    else:
        Send_CSV(client=client)
    
    response = dict()
    response["message_type"] = "error"
    response["meter"] = 1
    response["e_v"] = val_v
    response["e_c"] = val_c
    response["e_p"] = val_p
    response["e_q"] = val_q

    client.publish(service_topic, json.dumps(response))
    
    Data_validation = [0]*12
    for i in range(10):
        time.sleep(1)
        energymeter.View_New_Calibration_Data(Data_validation)
        frame_validation = dict()
        frame_validation["Vol"] = f"{Data_validation[0]},{Data_validation[1]},{Data_validation[2]}"
        frame_validation["Cur"] = f"{Data_validation[3]},{Data_validation[4]},{Data_validation[5]}"
        frame_validation["App"] = f"{Data_validation[6]},{Data_validation[7]},{Data_validation[8]}"
        frame_validation["Reac"] = f"{Data_validation[9]},{Data_validation[10]},{Data_validation[11]}"

        client.publish(service_topic, json.dumps(frame_validation))
    
    while Validation_full is None:
        pass

    if Validation_full == 2:
        pass
    else:
        Save_CV(1)

        Save_ADE_Calibration(1)
    return 1




def main():
    global LSB, Phcal, PGains, Phi, WH  , WH_1, Wh_lsb, Wac, Data_standard_measurement, Data_Quoia, Flag1, Flag_validation, Validation_full, Vol, Cur, mqtt_broaker
    mqttClient = mqtt.Client("calib"+mac)
    mqtt_broaker='MQTT_'+config['MQTT_BROKER']
    mqttClient.username_pw_set(username=net[mqtt_broaker + '_USER'], password=net[mqtt_broaker + '_PASSW'])
    mqttClient.on_connect = on_connect_mqtt
    mqttClient.on_message = on_message_mqtt

    mqtt_loop(mqttClient)
    

    count_get = 0
    count_exit = 0
    while (True):
        if get_calibration_flag == False:
            continue
        
        if(count_exit >= 60*5):
            break
        
        count_exit += 1
        count_get += 1
        if (Settings_Received == True):
            break
        if (count_get <= 30):
            mqttClient.publish(service_topic,"{\"message_type\":\"get_calibration_settings\",\"type\":\"ind\"}")
        
        time.sleep(1.0)

    while(True):
        if(Settings_Received):

            Energy_Meter = EnergyMeter.Meter_I2C()
            Energy_Meter.Initial_Setup(False, False)
            timer1 = InfiniteTimer(30, stop_acumulate, Energy_Meter)
            calibrate_ade = Auto_Calibration(error=0.1, client=mqttClient, energymeter=Energy_Meter, timer = timer1)
            if calibrate_ade == 1:
                break
            
            else:
                LSB = [0]*4
                Phcal = [0]*3
                PGains = [0]*3
                Phi = 0.0
                WH = 0
                WH_1 = 0.0
                Wh_lsb = 0
                Wac = 0
                Data_standard_measurement = [0]*6
                Data_Quoia = [0]*18

                Flag1 = False
                Flag_validation = False
                Validation_full = None
                Vol = 0
                Cur = 0
            break
    

    #os.kill(mqtt_loop_pid)
    #sys.exit(0)


if __name__=="__main__":
    
    main()