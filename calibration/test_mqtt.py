import yaml
import sys

sys.path.insert(1, '/home/obed/Documents/Solenium/Calibracion/quoia-meter')
with open('config_files/calibration.yaml', 'r') as f:
    calibration = yaml.safe_load(f)
    f.close()

Gains = [0]*4
LSB = [0]*4
Phcal = [0]*3
PGains = [0]*3



calibration["cv_m1"]["vgb"] = Gains[0]
calibration["cv_m1"]["igb"] = Gains[2]
calibration["cv_m1"]["vgc"] = Gains[1]
calibration["cv_m1"]["igc"] = Gains[3]

#PGains
calibration["cv_m1"]["poga"] = PGains[0]
calibration["cv_m1"]["pogb"] = PGains[1]
calibration["cv_m1"]["pogc"] = PGains[2]

#Phcals
calibration["cv_m1"]["pga"] = Phcal[0]
calibration["cv_m1"]["pgb"] = Phcal[1]
calibration["cv_m1"]["pgc"] = Phcal[2]

#LSB
calibration["cv_m1"]["lsb_vol"] = LSB[0]
calibration["cv_m1"]["lsb_curr"] = LSB[1]
calibration["cv_m1"]["lsb_wh"] = LSB[2]
calibration["cv_m1"]["lsb_wac"] = LSB[3]


with open('config_files/calibration.yaml','w') as f:
    yaml.dump(calibration, f)
    
    f.close()