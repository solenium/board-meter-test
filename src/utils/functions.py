import subprocess as sp

def get_ID():
    command = ["grep", "Serial" ,"/proc/cpuinfo"]
    out=sp.check_output(command)
    return out.decode().strip().replace("Serial\t\t: ","")[8:16]