from http.server import BaseHTTPRequestHandler
from urllib.parse import urlparse, parse_qs
from base64 import b64decode
from yaml import safe_load,dump
from http.client import HTTPSConnection as http_cli
from datetime import datetime as dt
from time import sleep
from subprocess import check_output

form_html= '''
<!DOCTYPE html>
<html>
<head>
	<title>Quoia Webserver</title>
</head>
<body>
    <button type="button" onclick="showPage(1)"> 
      Info
    </button>
    <button type="button" onclick="showPage(2)"> 
      Wifi settings
    </button>
    <button type="button" onclick="showPage(3)"> 
      Metering
    </button>

    <button type="button" onclick="showPage(4)">
        Installation
    </button>

    <button type="button" onclick="showPage(5)"> 
      Advanced
    </button>
    
    <div id="page1">   
        <h2>Firmware_Version: {f_ver}</h2>
        <h2>Operation Mode: {o_m}</h2>
        <h2>Wifi state: {wifi_state}</h2>
     </div>
     <div id="page2" hidden>
    
      <form name="myForm" action="/setting" onsubmit="return validateForm()" method="post">
        <h2>MAC Address: </h2>
        SSID: <input type="text" name="ssid"><br>
        Password: <input type="text" name="pass"><br>
        <input type="submit" value="Submit">
      </form>
    </div>
    <div id="page3" hidden>
        <form id="met" action="/metering" method="post">
        Actual metering values
        <br>
        <input type="submit" value="load data">
        </form>
        <br>
    </div>
    <div id="page4" hidden>
        <form action="/installation_ready" method="post"><input type='submit' value='Installation Ready'></form>
        <form action="/opt_mode" method="post">
            <br><br>
            Change operation mode
            <br>
            <input type="radio" name="o_m" value="0">0</input>
            <input type="radio" name="o_m" value="1">1</input>
            <input type="radio" name="o_m" value="2">2</input>
            <br>
            <input type="submit" value='Change'>
        </form>
    </div>
    <div id="page5" hidden>
        <button id="adv-conf" onclick="showAdvancedConfig()">Advanced wifi</button>
        <button id="webs-sett" onclick="showWebServerSettings()">Webserver settings</button>
        <button id="mqtt_conf" onclick="showMqttConfig()">Mqtt config</button>
        <div id="advanced_set" hidden>
            <br>
            <form method='post' action='/advanced_setting'>
                <label>SSID: </label>
                <input name='ssid' length=32>
                <br><br>
                <label>PASS: </label>
                <input name='pass' length=64>
                <br><br>
                <label>IP: </label>
                <input name='ip' length=64>
                <label>Ej:123.123.123.123</label>
                <br><br>
                <label>GATEWAY IP: </label>
                <input name='gw_ip' length=64>
                <br><br>
                <label>SUBNET: </label>
                <input name='subnet' length=64>
                <br><br>
                <label>DNS1: </label>
                <input name='dns1' length=64>
                <label>(Optional)</label>
                <br><br>
                <label>DNS2: </label>
                <input name='dns2' length=64>
                <label>(Optional)</label>
                <br><br>
                <input type='submit' value='Connect'>
            </form>
        </div>
        <div id="adv-web-set"  hidden>
            <br>
            <form action='/webserver_settings' method="post">
                <label>new SSID: </label>
                <input name='ssid' length=32>
                <br><br>
                <label>new PASS: </label>
                <input name='pass' length=64>
                <br><br>
                <input type="submit" value="Save">

            </form>
        </div>

        <div id="adv-mqtt-conf" hidden>
            <h2>Advanced mqtt configuration</h2>
        <h2> Status: {mqtt_state}</h2>

        <form method='post' action='/advanced_setting_mqtt'>
        <label>Server: </label>
        <input name='server' length=32>
        <label>Ej:123.123.123.123</label>
        <br><br>
        
        <label>Port: </label>
        <input name='port' length=64><br><br>
        <label>User: </label>
        <input name='user' length=64>
        <br><br>
        <label>Password: </label>
        <input name='pass' length=64><br><br>
        <input type='submit' value='Connect'>
    </form>
    <br>
    <form method='post' action='/default_broaker_mqtt'>
        <input type='submit' value='Default Broaker'>
    </form>

    </div>

    </div>
    <form action="/logout" method="post">
        <button type="submit">Logout</button>
    </form>
    '''
form_html_js='''
    <script>
    function showPage(divNumber){
    if (divNumber == 1) {
            document.getElementById('page1').hidden = false;
            document.getElementById('page2').hidden = true;
            document.getElementById('page3').hidden = true;
            document.getElementById('page4').hidden = true;
            document.getElementById('page5').hidden = true;

        } else if (divNumber == 2) {
            document.getElementById('page1').hidden = true;
            document.getElementById('page2').hidden = false;
            document.getElementById('page3').hidden = true;
            document.getElementById('page4').hidden = true;
            document.getElementById('page5').hidden = true;
        } else if (divNumber == 3) {
            document.getElementById('page1').hidden = true;
            document.getElementById('page2').hidden = true;
            document.getElementById('page3').hidden = false;
            document.getElementById('page4').hidden = true;
            document.getElementById('page5').hidden = true;
        } else if (divNumber == 4){
            document.getElementById('page1').hidden = true;
            document.getElementById('page2').hidden = true;
            document.getElementById('page3').hidden = true;
            document.getElementById('page4').hidden = false;
            document.getElementById('page5').hidden = true;
        } else if (divNumber == 5){
            document.getElementById('page1').hidden = true;
            document.getElementById('page2').hidden = true;
            document.getElementById('page3').hidden = true;
            document.getElementById('page4').hidden = true;
            document.getElementById('page5').hidden = false;
        }  
    }
    function showOpButtons(){
    document.getElementById('op_mode').hidden = !document.getElementById('op_mode').hidden;
    }

    function instReady () {
        window.location.replace(`/installation_ready`);
    }

    function mode1 () {
        window.location.replace(`/mode1`);
    }

    function mode3 () {
        window.location.replace(`/mode3`);
    }
    function showAdvancedConfig () {
    document.getElementById('adv-web-set').hidden = true;
    document.getElementById('adv-mqtt-conf').hidden = true;
    document.getElementById('advanced_set').hidden = !document.getElementById('advanced_set').hidden;
    }

    function showWebServerSettings () {

        document.getElementById('adv-mqtt-conf').hidden = true;
        document.getElementById('advanced_set').hidden = true;
        document.getElementById('adv-web-set').hidden = !document.getElementById('adv-web-set').hidden;
    }

    function showMqttConfig () {
        document.getElementById('advanced_set').hidden = true;
        document.getElementById('adv-web-set').hidden = true;
        document.getElementById('adv-mqtt-conf').hidden = !document.getElementById('adv-mqtt-conf').hidden;
    }
    </script>
</body>
</html>
'''
#with open("../../network.yaml",'r') as w:
with open("../config_files/network.yaml",'r') as w:
    net=safe_load(w)
with open("../config_files/config.yaml",'r') as w:
    config=safe_load(w)
users = {net['webserver']['user']: net['webserver']['passw'],"admin":"quoia123*"}

def have_internet() -> bool:
    con = http_cli("google.com",timeout=5)
    try:
        con.request("HEAD","/")
        return True
    except Exception:
        return False
    finally:
        con.close()

# Clase de manejador de solicitudes HTTP
class MyHandler(BaseHTTPRequestHandler):
    authenticated = False
    """def __init__(self, _energy_meter):
        BaseHTTPRequestHandler.__init__(self)
        self.energy_meter = _energy_meter
        print(self.energy_meter)
        
    def __init__(self,_energy_meter):
        #super().__init__()
        self.energy_meter = _energy_meter
"""
    def set_meter(self,_energy_meter):
        self.energy_meter=_energy_meter
    
    def set_mqtt_client(self,_client):
        self.mqtt_client=_client

    def do_GET(self):
        if not self.authenticate():
            self.do_AUTHHEAD()
            self.wfile.write(b'Acceso no autorizado')
        else:
            self.init_page()

    def do_POST(self):
        # Procesar datos del formulario en respuesta a POST
        if self.path == "/logout":
            print("Logout")
            self.do_AUTHHEAD()
            self.send_response(200)
            self.send_header('Content-type', 'text/html')
            self.end_headers()
            message = """
                    <html>
                        <body>
                            <h2>You have been logged out</h2>
                            <p><a href="/">Return to login page</a></p>
                        </body>
                    </html>
                """
            self.wfile.write(message.encode('utf-8'))
        elif self.path == "/setting":
            content_length = int(self.headers['Content-Length'])
            post_data = self.rfile.read(content_length).decode('utf-8')
            post_params = parse_qs(post_data)
            ssid = post_params['ssid'][0]
            passw = post_params['pass'][0]
            with open("network.yaml",'r') as w:
                file=safe_load(w)
                w.close()
            if file:
                file['wifi']['ssid']=ssid
                file['wifi']['passw']=passw
                file['wifi']['ip'] =None
                file['wifi']['gw_ip']=None
                file['wifi']['subnet']=None
                file['wifi']['dns1']=None
                file['wifi']['dns2']=None
                with open("network.yaml",'w') as w:
                    dump(file,w)
                    w.close()

            self.send_response(302)
            self.send_header('Location', "/saving")
            self.end_headers()
        elif self.path == "/installation_ready":
            #print("Instalaltion ready")
            with open("config.yaml",'r') as w:
                file=safe_load(w)
                w.close()
            if file:
                if file['acumulate'] == False:
                    file['acumulate'] = True
                    #TODO: Cargar primero el archio
                    with open("config.yaml",'w') as w:
                        dump(file,w)
                        w.close()
            self.send_response(302)
            self.send_header('Location', "/saving")
            self.end_headers()
        elif self.path == "/opt_mode":
            content_length = int(self.headers['Content-Length'])
            post_data =self.rfile.read(content_length).decode('utf-8')
            print(parse_qs(post_data))
            self.init_page()
        elif self.path == "/advanced_setting":
            content_length = int(self.headers['Content-Length'])
            post_data = self.rfile.read(content_length).decode('utf-8')
            post_params = parse_qs(post_data)
            with open("network.yaml",'r') as w:
                file=safe_load(w)
                w.close()
            if file:
                file['wifi']['ssid']=post_params['ssid'][0]
                file['wifi']['passw']=post_params['pass'][0]
                file['wifi']['ip'] =post_params['ip'][0]
                file['wifi']['gw_ip']=post_params['gw_ip'][0]
                file['wifi']['subnet']=post_params['subnet'][0]
                file['wifi']['dns1']=post_params['dns1'][0]
                file['wifi']['dns2']=post_params['dns2'][0]
                with open("network.yaml",'w') as w:
                    dump(file,w)
                    w.close()

            self.send_response(302)
            self.send_header('Location', "/saving")
            self.end_headers()
        elif self.path == "/webserver_settings":
            content_length = int(self.headers['Content-Length'])
            post_data = self.rfile.read(content_length).decode('utf-8')
            post_params = parse_qs(post_data)
            with open("network.yaml",'r') as w:
                file=safe_load(w)
                w.close()
            if file:
                file['webserver']['ssid']=post_params['ssid'][0]
                file['webserver']['passw']=post_params['pass'][0]
                with open("network.yaml",'w') as w:
                    dump(file,w)
                    w.close()

            self.send_response(302)
            self.send_header('Location', "/saving")
            self.end_headers()
        elif self.path == "/advanced_setting_mqtt":
            content_length = int(self.headers['Content-Length'])
            post_data = self.rfile.read(content_length).decode('utf-8')
            post_params = parse_qs(post_data)
            with open("network.yaml",'r') as w:
                file=safe_load(w)
                w.close()
            if file:
                file['mqtt_second']['server']=post_params['server'][0]
                file['mqtt_second']['port']=post_params['port'][0]
                file['mqtt_second']['user']=post_params['user'][0]
                file['mqtt_second']['passw']=post_params['pass'][0]
                with open("network.yaml",'w') as w:
                    dump(file,w)
                    w.close()
            with open("config.yaml",'r') as w:
                file=safe_load(w)
                w.close()
            if file:
                file['mqtt_broaker']='second'
                with open("config.yaml",'w') as w:
                    dump(file,w)
                    w.close()
            self.send_response(302)
            self.send_header('Location', "/saving")
            self.end_headers()
        elif self.path == "/default_broaker_mqtt":
            content_length = int(self.headers['Content-Length'])
            post_data = self.rfile.read(content_length).decode('utf-8')
            post_params = parse_qs(post_data)
            with open("config.yaml",'r') as w:
                file=safe_load(w)
                w.close()
            if file:
                file['mqtt_broaker']='default'
                with open("config.yaml",'w') as w:
                    dump(file,w)
                    w.close()

            self.send_response(302)
            self.send_header('Location', "/saving")
            self.end_headers()
        elif self.path == "/metering":          
            data=self.energy_meter.Full_DataN()
            #print(data)
            self.send_response(200)
            self.send_header('Content-type', 'text/html')
            self.end_headers()
            str_table=""
            for i in range(len(data)):
                str_table+="<tr><th>Pase "+str(i)+"</th></tr>"
                str_table+="<tr><th>Voltage</td><td>"+str(data[i][0])+"</th></tr>"
                str_table+="<tr><th>Currrent</td><td>"+str(data[i][1])+"</td></tr>"
                str_table+="<tr><th>Active Power</td><td>"+str(data[i][2])+"</td></tr>"
                str_table+="<tr><th>Reactive Power</td><td>"+str(data[i][3])+"</td></tr>"
                str_table+="<tr><th>Power Factor</td><td>"+str(data[i][4])+"</td></tr>"
            str_metering=f'''
                <!DOCTYPE html>
                <html>
                <head>
                    <title>Meter data</title>
                </head>
                <body>
                    <table>
                    {str_table}
                    </table>
                    <form id="met" action="/metering" method="post">
                    <input type="submit" value="Reload data">
                    </form>
                </body>
                </html>
                '''
            self.wfile.write(str_metering.encode('utf-8'))
        else:
            self.init_page()
    
    def do_AUTHHEAD(self):
        self.send_response(401)
        self.send_header('WWW-Authenticate', 'Basic realm="Acceso restringido"')
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def authenticate(self):
        # Verificar credenciales de autenticación básica
        auth_header = self.headers.get('Authorization')
        if auth_header:
            auth_decoded = b64decode(auth_header.split()[1]).decode()
            username, password = auth_decoded.split(':')
            return username in users and password == users[username]
        else:
            return False
    def check_status(self):
        out = check_output(['iw','wlan0','link']).decode('utf-8').strip().split()
        SSID_str = 'SSID:'
        if(SSID_str in out):
            self.wifi_state="Connected to "+out[out.index(SSID_str)+1]
            if have_internet():
                self.wifi_state+=" and have internet access"
                if not self.mqtt_client.is_connected():
                    self.mqtt_client.reconnect()
            else:
                self.wifi_state=" and don't have internet access"
                if self.mqtt_client.is_connected():
                    self.mqtt_client.reconnect()
                    print("Reconnect")
                    sleep(5)
            if self.mqtt_client.is_connected():
                self.mqtt_status = "Connected"
            else:
                self.mqtt_status = "Disconnected"
        else:
            self.wifi_state="Disconnected"
            self.mqtt_status = "Disconnected"

    def init_page(self):
        global form_html
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        self.check_status()
        status_html=form_html.format(f_ver="V1.0.1",o_m="Normal operation",wifi_state=self.wifi_state,mqtt_state=self.mqtt_status)
        _html=status_html+form_html_js
        self.wfile.write(_html.encode('utf-8'))

    def status_page(self,msg):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        message = "<html><body>"+msg+"</body></html>"
        self.wfile.write(message.encode('utf-8'))

