from http.server import BaseHTTPRequestHandler
from base64 import b64decode, b64encode
from time import sleep
from http.server import HTTPServer

form_html= '''
<!DOCTYPE html>
<html>
<head>
	<title>Quoia Webserver</title>
</head>
<body>
    <button type="button" onclick="showPage(1)"> 
      Info
    </button>
    <button type="button" onclick="showPage(2)"> 
      Wifi settings
    </button>
    <button type="button" onclick="showPage(3)"> 
      Metering
    </button>

    <button type="button" onclick="showPage(4)">
        Installation
    </button>

    <button type="button" onclick="showPage(5)"> 
      Advanced
    </button>
    
    <div id="page1">   
        <h2>Firmware_Version: {f_ver}</h2>
        <h2>Operation Mode: {o_m}</h2>
        <h2>Wifi state: {wifi_state}</h2>
     </div>
     <div id="page2" hidden>
    
      <form name="myForm" action="/setting" onsubmit="return validateForm()" method="post">
        <h2>MAC Address: </h2>
        SSID: <input type="text" name="ssid"><br>
        Password: <input type="text" name="pass"><br>
        <input type="submit" value="Submit">
      </form>
    </div>
    <div id="page3" hidden>
        <form id="met" action="/metering" method="post">
        Actual metering values
        <br>
        <input type="submit" value="load data">
        </form>
        <br>
    </div>
    <div id="page4" hidden>
        <form action="/installation_ready" method="post"><input type='submit' value='Installation Ready'></form>
        <form action="/opt_mode" method="post">
            <br><br>
            Change operation mode
            <br>
            <input type="radio" name="o_m" value="0">0</input>
            <input type="radio" name="o_m" value="1">1</input>
            <input type="radio" name="o_m" value="2">2</input>
            <br>
            <input type="submit" value='Change'>
        </form>
    </div>
    <div id="page5" hidden>
        <button id="adv-conf" onclick="showAdvancedConfig()">Advanced wifi</button>
        <button id="webs-sett" onclick="showWebServerSettings()">Webserver settings</button>
        <button id="mqtt_conf" onclick="showMqttConfig()">Mqtt config</button>
        <div id="advanced_set" hidden>
            <br>
            <form method='post' action='/advanced_setting'>
                <label>SSID: </label>
                <input name='ssid' length=32>
                <br><br>
                <label>PASS: </label>
                <input name='pass' length=64>
                <br><br>
                <label>IP: </label>
                <input name='ip' length=64>
                <label>Ej:123.123.123.123</label>
                <br><br>
                <label>GATEWAY IP: </label>
                <input name='gw_ip' length=64>
                <br><br>
                <label>SUBNET: </label>
                <input name='subnet' length=64>
                <br><br>
                <label>DNS1: </label>
                <input name='dns1' length=64>
                <label>(Optional)</label>
                <br><br>
                <label>DNS2: </label>
                <input name='dns2' length=64>
                <label>(Optional)</label>
                <br><br>
                <input type='submit' value='Connect'>
            </form>
        </div>
        <div id="adv-web-set"  hidden>
            <br>
            <form action='/webserver_settings' method="post">
                <label>new SSID: </label>
                <input name='ssid' length=32>
                <br><br>
                <label>new PASS: </label>
                <input name='pass' length=64>
                <br><br>
                <input type="submit" value="Save">

            </form>
        </div>

        <div id="adv-mqtt-conf" hidden>
            <h2>Advanced mqtt configuration</h2>
        <h2> Status: {mqtt_state}</h2>

        <form method='post' action='/advanced_setting_mqtt'>
        <label>Server: </label>
        <input name='server' length=32>
        <label>Ej:123.123.123.123</label>
        <br><br>
        
        <label>Port: </label>
        <input name='port' length=64><br><br>
        <label>User: </label>
        <input name='user' length=64>
        <br><br>
        <label>Password: </label>
        <input name='pass' length=64><br><br>
        <input type='submit' value='Connect'>
    </form>
    <br>
    <form method='post' action='/default_broaker_mqtt'>
        <input type='submit' value='Default Broaker'>
    </form>

    </div>

    </div>
    <form action="/logout" method="post">
        <button type="submit">Logout</button>
    </form>
    '''
form_html_js='''
    <script>
    function showPage(divNumber){
    if (divNumber == 1) {
            document.getElementById('page1').hidden = false;
            document.getElementById('page2').hidden = true;
            document.getElementById('page3').hidden = true;
            document.getElementById('page4').hidden = true;
            document.getElementById('page5').hidden = true;

        } else if (divNumber == 2) {
            document.getElementById('page1').hidden = true;
            document.getElementById('page2').hidden = false;
            document.getElementById('page3').hidden = true;
            document.getElementById('page4').hidden = true;
            document.getElementById('page5').hidden = true;
        } else if (divNumber == 3) {
            document.getElementById('page1').hidden = true;
            document.getElementById('page2').hidden = true;
            document.getElementById('page3').hidden = false;
            document.getElementById('page4').hidden = true;
            document.getElementById('page5').hidden = true;
        } else if (divNumber == 4){
            document.getElementById('page1').hidden = true;
            document.getElementById('page2').hidden = true;
            document.getElementById('page3').hidden = true;
            document.getElementById('page4').hidden = false;
            document.getElementById('page5').hidden = true;
        } else if (divNumber == 5){
            document.getElementById('page1').hidden = true;
            document.getElementById('page2').hidden = true;
            document.getElementById('page3').hidden = true;
            document.getElementById('page4').hidden = true;
            document.getElementById('page5').hidden = false;
        }  
    }
    function showOpButtons(){
    document.getElementById('op_mode').hidden = !document.getElementById('op_mode').hidden;
    }

    function instReady () {
        window.location.replace(`/installation_ready`);
    }

    function mode1 () {
        window.location.replace(`/mode1`);
    }

    function mode3 () {
        window.location.replace(`/mode3`);
    }
    function showAdvancedConfig () {
    document.getElementById('adv-web-set').hidden = true;
    document.getElementById('adv-mqtt-conf').hidden = true;
    document.getElementById('advanced_set').hidden = !document.getElementById('advanced_set').hidden;
    }

    function showWebServerSettings () {

        document.getElementById('adv-mqtt-conf').hidden = true;
        document.getElementById('advanced_set').hidden = true;
        document.getElementById('adv-web-set').hidden = !document.getElementById('adv-web-set').hidden;
    }

    function showMqttConfig () {
        document.getElementById('advanced_set').hidden = true;
        document.getElementById('adv-web-set').hidden = true;
        document.getElementById('adv-mqtt-conf').hidden = !document.getElementById('adv-mqtt-conf').hidden;
    }
    </script>
</body>
</html>
'''

# Create a custom handler that inherits from BaseHTTPRequestHandler
class MyHandler(BaseHTTPRequestHandler):

    # Override the do_GET() function to require authentication and serve the webpage
    def do_GET(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        status_html=form_html.format(f_ver="V1.0.1",o_m="Normal operation",wifi_state="self.wifi_state",mqtt_state="self.mqtt_status")
        _html=status_html+form_html_js
        self.wfile.write(_html.encode('utf-8'))

    def do_POST(self):
        if self.path == "/installation_ready":
            self.send_response(200)
            self.send_header('Content-type', 'text/html')
            self.end_headers()
            message = """
                    <html>
                        <body>
                            <h2>Saving data</h2>
                        </body>
                    </html>
                """
            self.wfile.write(message.encode('utf-8'))
            
            self.send_response(302)
            self.send_header('Location', "/saving")
            self.end_headers()

# Set the server port and handler
PORT = 80

# Start the server
server_address = ('', PORT)
server_handler = MyHandler
httpd = HTTPServer(server_address, server_handler)
print("Server on port", PORT)
httpd.serve_forever()