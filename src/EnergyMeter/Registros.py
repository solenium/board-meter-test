"""______       _             _ 
 / _____)     | |           (_)
( (____  _____| |_____ _____ _ _   _ _____
 \____ \|  _  | |  __ |  _  | | | | |     |
 _____) ) |_| | |  ___| | | | | |_| | | | |
(______/|_____|_|_____)_| |_|_|_____|_|_|_| 
2020(c) Created by Joan Sebastian Jimenez
Medidor I2C
"""
"""
*** Constants
"""
MASK_PF = 0x8000
V_Constant = 815.3458647 #Voltage divider at VLOW_Lin
W_Bits_Constant = 54119356.0
VC_Bits_Constant = 5326737.0
FP_Bits_Constant = 32767.0
Constante_Angulo = 0.0843
Full_Scale = 0.5
PMAX = 27059678
fs2 = 1024000
nm = 2

# ADE I2C Address
Direccion_ADE = 0x38
# LED Indicator
LED = 13
#  Rogoswki Configurations registers
DICOEFF = 0x4388
# Grid Frequency
FREC_60Hz = 0x41FF
FREC_50Hz = 0x1FF
# Nominal Voltage Values
VNOM_120 = 120
VNOM_240 = 120 #¿?
VNOM_480 = 277
# HSDC Configures Values
HSDC_EN = 0x42
HSDC_DIS = 0x2
# Harmonic constants configurations registers
ARM_A = 0x08
ARM_B = 0x0A
ARM_C = 0x0C
ARM_ISUM = 0x0E
STATUS0 = 0xE502
STATUS1 = 0xE503
MASK_VALID_THD = 0x80000
MASK0 = 0xE50A
#Valor a escribir para configurar el HSDC
#(Frecuencia  4 MHz, entrega paquetes de 8 bits, no hay saltos de reloj entre paquete, envia 7 registros de 32 bits.
#HSDC_CONFIG  0x02 #(0xA - SS LOW - 8 MHz) & 0xB - (SS LOW - 4 MHz)
HSDC_CONFIG = 0x02 #(0xA - SS LOW - 8 MHz) & 0xB - (SS LOW - 4 MHz)
#HSDC_CONFIG  0x03 #(0xA - SS LOW - 8 MHz) & 0xB - (SS LOW - 4 MHz)
#HSDC_CONFIG  0x00 # Prueba para recibir paquetes de 32 bits
"""
*** Gain Registers
"""
# Calibration Voltage Gain Registers
AVGAIN = 0x4381
BVGAIN = 0x4383
CVGAIN = 0x4385
# Calibration Current Gain Registers
AIGAIN = 0x4380
BIGAIN = 0x4382
CIGAIN = 0x4384
# Calibration Voltage Offset Registers  
AVRMSOS = 0x4390
BVRMSOS = 0x4392
CVRMSOS = 0x4394
# Calibration Current Offset Registers
AIRMSOS = 0x438F
BIRMSOS = 0x4391
CIRMSOS = 0x4393
# Calibrarion CFDEN Registers
CF1DEN = 0xE611
CF2DEN = 0xE612
CF3DEN = 0xE613
# Calibration Phase Registers
APHCAL = 0xE614
BPHCAL = 0xE615
CPHCAL = 0xE616
# Calibration Power Gain Registers
APGAIN = 0X4389
BPGAIN = 0X438B
CPGAIN = 0X438D

"""
*** Energy Variables Registers
"""
# RMS Voltage
AVRMS = 0X43C1
BVRMS = 0x43C3
CVRMS = 0x43C5
#RMS Current
AIRMS = 0x43C0
BIRMS = 0x43C2
CIRMS = 0x43C4
NIRMS = 0x43C6
# Active Powers.
AWATT = 0xE513
BWATT = 0xE514
CWATT = 0xE515
# Apparent Powers.
AVA = 0xE519
BVA = 0xE51A
CVA = 0xE51B
# Power Factor.
APF = 0xE902
BPF = 0xE903
CPF = 0xE904
# Harmonics
VTHD = 0xE886
CTHD = 0xE887
HXVHD = 0xE88E
HXIHD = 0xE88F
HYVHD = 0xE896
HYIHD = 0xE897
HZVHD = 0xE89E
HZIHD = 0xE89F

HXVRMS = 0xE888
HXIRMS = 0xE889
FVRMS = 0xE880
#HXIHD = 0xE88F
#HXVHD = 0xE88E
FIRMS = 0xE881
HX = 0xEA08
HY = 0xEA09
HZ = 0xEA0A


#Instant Voltage
VAWV = 0xE510
VBWV = 0xE511
VCWV = 0xE512
#Instant Current
IAWV = 0xE50C
IBWV = 0xE50D
ICWV = 0xE50E
# Total Active Energy Accumulation.
AWATTHR = 0xE400
BWATTHR = 0xE401
CWATTHR = 0xE402
#Other register Roen   
ANGLE0 = 0xE601
ANGLE1 = 0xE602
ANGLE2 = 0xE603
#CWATTHR = 0xE402
#CWATTHR = 0xE402
#CWATTHR = 0xE402
# Fundamental Active Energy Accumulation.
AFWATTHR = 0xE403
BFWATTHR = 0xE404
CFWATTHR = 0xE405
# Fundamental Reactive Energy Accumulation.
AFVARHR = 0xE409
BFVARHR = 0xE40A
CFVARHR = 0xE40B
# Apparent Energy Acumulation.
AVAHR = 0xE40C
BVAHR = 0xE40D
CVAHR = 0xE40E
# Total ative power offset adjust
AWATTOS = 0x438A
BWATTOS = 0x438C
CWATTOS = 0x438E

"""
*** Configure Registers
"""
HSDC_CFG = 0xE706
CONFIG = 0xE618
CONFIG2 = 0xEC01
CONFIG3 = 0xEA00
COMPMODE = 0xE60E
ACCMODE = 0xE701
LCYCMODE = 0xE702
CHECKSUM = 0xE51F
VLEVEL = 0x439F
VNOM = 0xE520
RUNREG = 0xE228
CFMODE = 0xE610
HCONFIG = 0xE900

PHSIGN = 0xE617

