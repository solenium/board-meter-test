import yaml, time, numpy as np, sys, os, subprocess as sp, gpiod
from  dotenv import dotenv_values
from dotenv import set_key
#import smbus

sys.path.insert(1, '/home/root/app/board-meter/')
from src.mqtt.mqtt_encoder import frame_energy as p
from src.EnergyMeter import Registros as R
from src.infinite_Timer import InfiniteTimer
from src import utils_db as db



PATH_DB = os.path.join(os.path.dirname(os.path.abspath(__file__)))
dir_db = "/home/root/app/config_files"
dir_energy = "/home/root/app/board-meter/data"
name_db = "config_values"
config_table = "configuration"
calibration_table = "calibration"









class Meter_I2C:
	def __init__(self):
		#self.config  = self.get_env(file_name="config")
		#self.calibration = self.get_env(file_name="calibration")

		
		self.meter_type = db.return_values(directory=dir_db, database=name_db, table_name=config_table, columns=['TYPE'])[0][0]
	#self.i2c_bus_m1 = smbus.SMBus(self.config['ade_config_m1']['i2c_bus'])
		self.i2c_bus_m1 = db.return_values(directory=dir_db, database=name_db, table_name=config_table, columns=['ADE_CONFIG_M1_I2C_BUS'])[0][0]
		print(f"bus m1:  {self.i2c_bus_m1}")
		self.rst_gpio_m1_chip = db.return_values(directory=dir_db, database=name_db, table_name=config_table, columns=['ADE_CONFIG_M1_RST_GPIO_BLOCK'])[0][0]
		self.rst_gpio_m1 = db.return_values(directory=dir_db, database=name_db, table_name=config_table, columns=['ADE_CONFIG_M1_RST_GPIO_PIN'])[0][0]
		self.ct_current_m1 = db.return_values(directory=dir_db, database=name_db, table_name=config_table, columns=['ADE_CONFIG_M1_CT_CURRENT'])[0][0]
		self._VB_1 = db.return_values(directory=dir_db, database=name_db, table_name=config_table, columns=['ADE_CONFIG_M2_CT_RATIO'])[0][0]
		
		self.phases = db.return_values(directory=dir_db, database=name_db, table_name=config_table, columns=['PHASES'])[0][0]
		#self._TActive_Positive_m1 = [[0.0, 0.0, 0.0] if db.return_values(directory=dir_energy, database=metering_db, table_name=energies, columns=energy_keys)]
		#self._TReactive_Positive_m1 = [0.0, 0.0, 0.0]
		#self._TActive_Negative_m1 = [0.0, 0.0, 0.0]
		#self._TReactive_Negative_m1 = [0.0, 0.0, 0.0]
		energy_keys = list()
		for i in range(4):
			for j in range(self.phases):
				energy_keys.append(p[(i*3)+j][0])
		connection_en = db.return_values(directory=dir_energy, database="metering_db", table_name="acumulate", columns=energy_keys, where_clause="1 = 1")
		if  len(connection_en)==0:
			self._TReactive_Positive_m1 = [0.0, 0.0, 0.0]
			self._TActive_Positive_m1 = [0.0, 0.0, 0.0]
			self._TActive_Negative_m1 = [0.0, 0.0, 0.0]
			self._TReactive_Negative_m1 = [0.0, 0.0, 0.0]
		else:
			self._TActive_Positive_m1 = [connection_en[0][0], connection_en[0][1], connection_en[0][2]]
			self._TReactive_Positive_m1 = [connection_en[0][3],  connection_en[0][4], connection_en[0][5]]
			self._TActive_Negative_m1 = [connection_en[0][6], connection_en[0][7], connection_en[0][8]]
			self._TReactive_Negative_m1 = [connection_en[0][9], connection_en[0][10], connection_en[0][11]]
		
		
		self.AWA = 0
		self.BWA = 0
		self.CWA = 0
		self.AWR = 0
		self.BWR = 0
		self.CWR = 0
		self.WH_A_Read = 0
		self.WH_B_Read = 0
		self.WH_C_Read = 0
		self.Voltage_LSB = 0
		self.Current_LSB = 0
		self.WH_LSB = 0
		self.Wac_LSB = 0
		self.Flag_step4 = False

		self.k_vol = 0.0
		self.k_curr = 0.0
		self.k_wh = 0.0
		self.k_wac = 0.0



			
		#self._Calibrate = False
		

		

		if self.meter_type == 'Dual':
			#self.i2c_bus_m2 = smbus.SMBus(self.config['type']['ade_config_m2']['i2c_bus'])
			self.i2c_bus_m2 = db.return_values(directory=dir_db, database=name_db, table_name=config_table, columns=['ADE_CONFIG_M2_I2C_BUS'])[0][0]
			self.rst_gpio_m2_chip = db.return_values(directory=dir_db, database=name_db, table_name=config_table, columns=['ADE_CONFIG_M2_RST_GPIO_BLOCK'])[0][0]
			self.rst_gpio_m2 = db.return_values(directory=dir_db, database=name_db, table_name=config_table, columns=['ADE_CONFIG_M2_RST_GPIO_PIN'])[0][0]			
			self.ct_current_m2 = db.return_values(directory=dir_db, database=name_db, table_name=config_table, columns=['ADE_CONFIG_M2_CT_CURRENT'])[0][0]
			self._VB_2 = db.return_values(directory=dir_db, database=name_db, table_name=config_table, columns=['ADE_CONFIG_M2_CT_RATIO'])[0][0]
			self._TActive_Positive_m2 = [0.0, 0.0, 0.0]
			self._TReactive_Positive_m2 = [0.0, 0.0, 0.0]
			self._TActive_Negative_m2 = [0.0, 0.0, 0.0]
			self._TReactive_Negative_m2 = [0.0, 0.0, 0.0]

			self.k_vol_2 = 0.0
			self.k_curr_2 = 0.0
			self.k_wh_2 = 0.0
			self.k_wac_2 = 0.0

		if db.return_values(directory=dir_db, database=name_db, table_name=config_table, columns=['FREQUENCY'])[0][0] == '50Hz':
			self.frecuency = R.FREC_50Hz
		else:
			self.frecuency = R.FREC_60Hz
		self.nominal_voltage =  db.return_values(directory=dir_db, database=name_db, table_name=config_table, columns=['NOMINAL_VOLTAGE'])[0][0]
		self.VRMS_Regs = [R.AVRMS,R.BVRMS,R.CVRMS]
		self.IRMS_Regs = [R.AIRMS,R.BIRMS,R.CIRMS]
		self.WATT_Regs = [R.AWATT,R.BWATT,R.CWATT]
		self.VA_Regs = [R.AVA,R.BVA,R.CVA]
		self.PF_Regs = [R.APF,R.BPF,R.CPF]
		self.WATTHR_Regs = [R.AWATTHR,R.BWATTHR,R.CWATTHR]
		self.VAHR_Regs = [R.AVAHR,R.BVAHR,R.CVAHR]
	
	
	def Initial_Setup(self, HSDC_Enable, Calibrate):
		self._Meter_Setup(self.i2c_bus_m1, self.rst_gpio_m1_chip, self.rst_gpio_m1, False,'M1', Calibrate)
		if self.meter_type == 'Dual':
			self._Meter_Setup(self.i2c_bus_m1, self.rst_gpio_m2_chip, self.rst_gpio_m2,True,'M2', Calibrate)
		
	def _Meter_Setup(self,bus,rst_chip,rst_pin,dual,cv, calibrate):
		self._ADE_Reset(rst_chip, rst_pin)
		time.sleep(0.1) #?
		self._ADE_Enable_I2C(bus)
		time.sleep(0.1)
		time.sleep(0.1)
		self._ADE_COMPMODE_W(bus)
		time.sleep(0.1)
		self._ADE_RAM_Register_Initialization(bus)
		time.sleep(0.1)
		self._Register_Initialize(bus,dual)
		time.sleep(0.1)
		self._ADE_Lock_RAM_Protection(bus)
		time.sleep(0.1)
		if dual:
			print("Cheking the RAM register of the Meter #2 - ",end="")
			if self._ADE_RAM_Verification(self.i2c_bus_m2):
				print("(✔)")
			else:
				print("(✘)")
				time.sleep(1)
				sys.exit()
		else:
			print("Cheking the RAM register of the Meter #1 - ",end="")
			if self._ADE_RAM_Verification(self.i2c_bus_m1):
				print("(✔)")
			else:
				print("(✘)")
				time.sleep(1)
				sys.exit()
		time.sleep(0.1)
		self._ADE_Enable_DSP(bus)
		time.sleep(0.1)
		self._ADE_Clear_Energy_Registers(bus)
		time.sleep(0.1)
		self._ADE_Enable_CF(bus)
		time.sleep(0.1)
		if (calibrate == True):

			self.load_calibration_data(bus,cv)
		time.sleep(0.1)
		"""
		self._ADE_HSDC_Config()with open('config.yaml','r') as f:
			self.config = yaml.safe_load(f)
		with open('calibration.yaml','r') as f:
			self.calibration = yaml.safe_load(f)
		time.sleep(0.1)
		self._ADE_Enable_HSDC()
		time.sleep(0.1)
		"""

	def _ADE_Reset(self,rst_chip, rst_pin):
		# High low high rst pin
		
		chip = gpiod.chip(rst_chip, gpiod.chip.OPEN_BY_PATH)
		line = chip.get_line(rst_pin)
		config_rst              = gpiod.line_request()
		config_rst.consumer     = "Exti_event"
		config_rst.flags        = gpiod.line_request.FLAG_BIAS_PULL_UP
		config_rst.request_type = gpiod.line_request.DIRECTION_OUTPUT
		
		line.request(config_rst)
		line.set_value(1)
		time.sleep(1)
		line.set_value(0)
		time.sleep(1)
		line.set_value(1)
		time.sleep(0.001)

		line.release()
		chip.reset()


	
	def _ADE_Enable_I2C(self,bus):
		self.i2c_write(bus,R.CONFIG2,0x2,1)


	def _ADE_Unlock_RAM_Protection(self,bus):
		self.i2c_write(bus, 0xE7FE, 0xAD,1)
		self.i2c_write(bus, 0xE7E3, 0x00,1)
	
	def _ADE_Lock_RAM_Protection(self, bus):
		self.i2c_write(bus, 0xE7FE, 0xAD,1)
		self.i2c_write(bus, 0xE7E3, 0x80,1)
	
	def _ADE_COMPMODE_W(self,bus):
		self.i2c_write(bus, R.COMPMODE,self.frecuency,2)

	def _ADE_RAM_Register_Initialization(self,bus):
		self.i2c_write(bus, R.ACCMODE,0x80,1)
		self.i2c_write(bus, R.LCYCMODE,0x78,1)

	def _Register_Initialize(self,bus,dual):
		if dual:
			self._Energy_Constant_2=self._Energy_Constant_Calculate(self._VB_2, self.ct_current_m2)
			self.i2c_write(bus, R.VLEVEL, int((self._ADE_VFS() / self.nominal_voltage) * 4 * pow(10, 6)),4)
			self.i2c_write(bus, R.VNOM, int((self.nominal_voltage / self._ADE_VFS()) * 3766572),4)
		else:
			self._Energy_Constant_1=self._Energy_Constant_Calculate(self._VB_1, self.ct_current_m1)
			self.i2c_write(bus, R.VLEVEL, 0x8000000,4)
			#VNOM Meter 1-> Ecuacion 42
			self.i2c_write(bus, R.VNOM, int((self.nominal_voltage / self._ADE_VFS()) * 3766572),4)

	def _Energy_Constant_Calculate(self, VB, CB):
		return (pow(2,27)/R.PMAX)*((self._ADE_VFS()*self._ADE_IFS(VB,CB))/R.fs2)*0.000833333
	
	def _ADE_VFS(self):
		return ((R.Full_Scale * R.V_Constant) / np.sqrt(2))

	def _ADE_IFS(self,VB, CB):
		return ((CB / VB) * R.Full_Scale)

	def _ADE_RAM_Verification(self,bus):
		for i in range(10):
			if (self.i2c_read(bus,R.COMPMODE,2)== self.frecuency) and (self.i2c_read(bus,R.ACCMODE,1)== np.byte(0x80)) and (self.i2c_read(bus,R.LCYCMODE,1)== 0x78):
				return True
		return False

	def _ADE_Enable_DSP(self,bus):
		self.i2c_write(bus, 0xE7FE, 0xAD,1)
		self.i2c_write(bus, 0xE7E2, 0x14,1)
		time.sleep(0.0002)
		self.i2c_write(bus, 0xE7FE, 0xAD,1)
		self.i2c_write(bus, 0xE7E2, 0x04,1)
		self.i2c_write(bus, R.RUNREG, 0x0001,2)

	def _Calculate_Phase_Error(self, Phi: float, Active_Energy: float, Reactive_Energy: float):
		Frequency = 60
		Phi_Rad = ((Phi) * (np.pi)) / 180
		Numerator = (Reactive_Energy * np.cos(Phi_Rad) - (Active_Energy * np.sin(Phi_Rad)))
		Denominator = (Reactive_Energy * np.sin(Phi_Rad)) + (Active_Energy * np.cos(Phi_Rad))
		Error_Ra = np.arctan( Numerator / Denominator )
		Error_De = ((Error_Ra) * (180)) / np.pi

		print(f"Error Ra: {Error_Ra}")
		
		Value = abs( (Error_De * 1024000) / (360 * Frequency) )
		if Error_De > 0:
			print(f"Error De: {Error_De}")
			return Value + 512
		else:
			print(f"Error De: {Error_De}")
			return Value
	




	def _ADE_Clear_Energy_Registers(self, bus):
		self.i2c_read(bus, R.AWATTHR,4)
		self.i2c_read(bus, R.BWATTHR,4)
		self.i2c_read(bus, R.CWATTHR,4)

		self.i2c_read(bus, R.AFWATTHR,4)
		self.i2c_read(bus, R.BFWATTHR,4)
		self.i2c_read(bus, R.CFWATTHR,4)

		self.i2c_read(bus, R.AFVARHR,4)
		self.i2c_read(bus, R.BFVARHR,4)
		self.i2c_read(bus, R.CFVARHR,4)

		self.i2c_read(bus, R.AVAHR,4)
		self.i2c_read(bus, R.BVAHR,4)
		self.i2c_read(bus, R.CVAHR,4)
	def _ADE_Enable_CF(self,bus):
		self.i2c_write(bus, R.CFMODE, 0xEA0,2)

	def _ADE_HSDC_Config(self):
		self.i2c_write(self.i2c_bus_m1, R.HSDC_CFG, R.HSDC_CONFIG,1)
	
	def _ADE_Enable_HSDC(self):
		temp = self.i2c_read(self.i2c_bus_m1, R.CONFIG,2) or 0b1000000
		self.i2c_write(self.i2c_bus_m1, R.CONFIG,temp,2)

	def _ADE_Disable_HSDC(self):
		temp = self.i2c_read(self.i2c_bus_m1, R.CONFIG,2) and 0b0111111
		self.i2c_write(self.i2c_bus_m1, R.CONFIG,temp,2)

	def i2c_write(self,bus,reg,val,len):
		if val < 0:
			val_list = list(map(str,val.to_bytes(len+1,'big',signed=True)))[1:]
		else:
			val_list = list(map(str,val.to_bytes(len,'big',signed=False)))
		return sp.run(['i2ctransfer','-y',str(bus),'w'+str(len+2)+'@'+str(R.Direccion_ADE)]+list(map(str,reg.to_bytes(2,'big')))+val_list).returncode
		#sp.run(['i2ctransfer','-y',str(bus),'w'+str(len+2)+'@0x38']+list(map(str,reg.to_bytes(2,'big'))+list(map(str,val.to_bytes(len,'big')))))

	def i2c_read(self,bus,reg,len):
		out=sp.check_output(['i2ctransfer','-y',str(bus),'w2@'+str(R.Direccion_ADE)]+list(map(str,reg.to_bytes(2,'big')))+['r'+str(len)])		
		return int.from_bytes(bytearray.fromhex(out.decode().strip().replace('0x','')),'big',signed=True)
	
	def Full_DataN(self,meter='mt'):
		data=[]
		for i in range(self.phases): data.append([0]*9)
		if meter== 'gen':
			_meter = 'M2'
			bus = self.i2c_bus_m2
			k_mul = self.ct_current_m2/(100)
		else:
			_meter = 'M1'
			bus = self.i2c_bus_m1
			k_mul = self.ct_current_m1/(100)
		
		

		for i in range(self.phases):
			if self.meter_type == 'gen':
				data[i][0] = self.i2c_read(bus,self.VRMS_Regs[i],4)*self.k_vol_2	#Voltage
				data[i][1] = self.i2c_read(bus,self.IRMS_Regs[i],4)*self.k_curr_2	#Current
				data[i][2] = self.i2c_read(bus,self.WATT_Regs[i],4)*self.k_wac_2	#Active power
				aparent_power = self.i2c_read(bus,self.VA_Regs[i],4)*self.k_wac_2	#Aparent power
				data[i][3] = np.sqrt(abs(((aparent_power)**2) - ((data[i][2])**2)))	#Reactive power 
				data[i][4] = self.i2c_read(bus,self.PF_Regs[i],2)/R.FP_Bits_Constant	#Power factor
			else:
				data[i][0] = self.i2c_read(bus,self.VRMS_Regs[i],4)*self.k_vol	#Voltage
				data[i][1] = self.i2c_read(bus,self.IRMS_Regs[i],4)*self.k_curr	#Current
				data[i][2] = self.i2c_read(bus,self.WATT_Regs[i],4)*self.k_wac	#Active power
				aparent_power = self.i2c_read(bus,self.VA_Regs[i],4)*self.k_wac	#Aparent power
				data[i][3] = np.sqrt(abs(((aparent_power)**2) - ((data[i][2])**2)))	#Reactive power 
				data[i][4] = self.i2c_read(bus,self.PF_Regs[i],2)/R.FP_Bits_Constant	#Power factor

		if meter== 'gen':
			for i in range(self.phases):
				temp = self.i2c_read(bus,self.WATTHR_Regs[i],4)*self.k_wh_2	#Energy active
				if temp >= 0:
					self._TActive_Positive_m2[i] += (temp/1000.0)
				else:
					self._TActive_Negative_m2[i] -= (temp/1000.0)
				temp1 = self.i2c_read(bus,self.VAHR_Regs[i],4)*self.k_wh_2	#Energy aparent
				temp = np.sqrt(temp1**2 - temp**2)
				sign = (self.i2c_read(bus, R.PHSIGN,2)>>(4+i)) and 0b1
				if sign:
					self._TReactive_Negative_m2[i] += (temp/1000.0)
				else:
					self._TReactive_Positive_m2[i] += (temp/1000.0)
			
				data[i][5] = self._TActive_Positive_m2[i]		#Total positive active energy acumulation 
				data[i][6] = self._TReactive_Positive_m2[i]	#Total positive reactive energy acumulation 
				data[i][7] = self._TActive_Negative_m2[i]		#Total negative active energy acumulation 
				data[i][8] = self._TReactive_Negative_m2[i]	#Total negative reactive energy acumulation
		else:
			for i in range(self.phases):
				temp = self.i2c_read(bus,self.WATTHR_Regs[i],4)*self.k_wh	#Energy active
				if temp >= 0:
					self._TActive_Positive_m1[i] += (temp/1000.0)
				else:
					self._TActive_Negative_m1[i] -= (temp/1000.0)
				temp1 = self.i2c_read(bus,self.VAHR_Regs[i],4)*self.k_wh	#Energy aparent
				temp = np.sqrt(abs(temp1**2 - temp**2))
				sign = (self.i2c_read(bus, R.PHSIGN,2)>>(4+i)) and 0b1
				if sign:
					self._TReactive_Negative_m1[i] += (temp/1000.0)
				else:
					self._TReactive_Positive_m1[i] += (temp/1000.0)
			
				data[i][5] = self._TActive_Positive_m1[i]		#Total positive active energy acumulation 
				data[i][6] = self._TReactive_Positive_m1[i]	#Total positive reactive energy acumulation 
				data[i][7] = self._TActive_Negative_m1[i]		#Total negative active energy acumulation 
				data[i][8] = self._TReactive_Negative_m1[i]	#Total negative reactive energy acumulation
		return data

	def Read_Powers(self,meter='mt'):
		data=[]
		for i in range(self.phases): data.append([0]*2)
		if meter== 'gen':
			bus = self.i2c_bus_m2
		else:
			bus = self.i2c_bus_m1
		
		#k_wac = db.return_values(directory=dir_db, database=name_db, table_name=calibration_table, columns=[f'LSB_WAC_{_meter}'])[0][0]*k_mul

		for i in range(self.phases):
			data[i][0] = self.i2c_read(bus,self.WATT_Regs[i],4)*self.k_wac 	#Active power
			aparent_power = self.i2c_read(bus,self.VA_Regs[i],4)*self.k_wac  #Aparent power
			data[i][1] = np.sqrt(abs(((aparent_power)**2) - ((data[i][0])**2)))	#Reactive power
		return data

	def load_calibration_data(self,bus,cv):
		
		k_mul = self.ct_current_m1/100
		self.k_vol = db.return_values(directory=dir_db, database=name_db, table_name=calibration_table, columns=[f'LSB_VOL_M1'])[0][0]
		self.k_curr = db.return_values(directory=dir_db, database=name_db, table_name=calibration_table, columns=[f'LSB_CURR_M1'])[0][0]*k_mul
		self.k_wh = db.return_values(directory=dir_db, database=name_db, table_name=calibration_table, columns=[f'LSB_WH_M1'])[0][0]*k_mul
		self.k_wac = db.return_values(directory=dir_db, database=name_db, table_name=calibration_table, columns=[f'LSB_WAC_M1'])[0][0]*k_mul
		
		self._ADE_Unlock_RAM_Protection(bus)
		self.i2c_write(bus, R.BVGAIN, int(db.return_values(directory=dir_db, database=name_db, table_name=calibration_table, columns=[f'VGB_{cv}'])[0][0]),4)
		self.i2c_write(bus, R.CVGAIN, int(db.return_values(directory=dir_db, database=name_db, table_name=calibration_table, columns=[f'VGC_{cv}'])[0][0]),4)
		self.i2c_write(bus, R.BIGAIN, int(db.return_values(directory=dir_db, database=name_db, table_name=calibration_table, columns=[f'IGB_{cv}'])[0][0]),4)
		self.i2c_write(bus, R.CIGAIN, int(db.return_values(directory=dir_db, database=name_db, table_name=calibration_table, columns=[f'IGC_{cv}'])[0][0]),4)
		self.i2c_write(bus, R.APHCAL, int(db.return_values(directory=dir_db, database=name_db, table_name=calibration_table, columns=[f'PGA_{cv}'])[0][0]),2)
		self.i2c_write(bus, R.BPHCAL, int(db.return_values(directory=dir_db, database=name_db, table_name=calibration_table, columns=[f'PGB_{cv}'])[0][0]),2)
		self.i2c_write(bus, R.CPHCAL, int(db.return_values(directory=dir_db, database=name_db, table_name=calibration_table, columns=[f'PGC_{cv}'])[0][0]),2)
		self.i2c_write(bus, R.APGAIN, int(db.return_values(directory=dir_db, database=name_db, table_name=calibration_table, columns=[f'POGA_{cv}'])[0][0]),4)
		self.i2c_write(bus, R.BPGAIN, int(db.return_values(directory=dir_db, database=name_db, table_name=calibration_table, columns=[f'POGB_{cv}'])[0][0]),4)
		self.i2c_write(bus, R.CPGAIN, int(db.return_values(directory=dir_db, database=name_db, table_name=calibration_table, columns=[f'POGC_{cv}'])[0][0]),4)
		self._ADE_Lock_RAM_Protection(bus)

		if self.meter_type == 'Dual':
			k_mul = self.ct_current_m2/100
			self.k_vol_2 = db.return_values(directory=dir_db, database=name_db, table_name=calibration_table, columns=['LSB_VOL_M2'])[0][0]
			self.k_curr_2 = db.return_values(directory=dir_db, database=name_db, table_name=calibration_table, columns=['LSB_CURR_M2'])[0][0]*k_mul
			self.k_wh_2 = db.return_values(directory=dir_db, database=name_db, table_name=calibration_table, columns=['LSB_WH_M2'])[0][0]*k_mul
			self.k_wac_2 = db.return_values(directory=dir_db, database=name_db, table_name=calibration_table, columns=['LSB_WAC_M2'])[0][0]*k_mul

	def write_register(self,reg,val,len,meter='mt'):
		if(meter=='gen'):
			bus=self.i2c_bus_m2
		else:
			bus=self.i2c_bus_m1
		self._ADE_Unlock_RAM_Protection(bus)
		self.i2c_write(bus, reg, val,len)
		self._ADE_Lock_RAM_Protection(bus)
	
	def read_register(self,reg,len,meter='mt'):
		if(meter=='gen'):
			bus=self.i2c_bus_m2
		else:
			bus=self.i2c_bus_m1
		return self.i2c_read(bus, reg,len)
	
	def update_var(self):
		#with open('config_files/config.yaml','r') as f:
		#	self.config = yaml.safe_load(f)
		#with open('config_files/calibration.yaml','r') as f:
	#		self.calibration = yaml.safe_load(f)
		
		#self.i2c_bus_m1 = smbus.SMBus(self.config['ade_config_m1']['i2c_bus'])
		self.meter_type = db.return_values(directory=dir_db, database=name_db, table_name=config_table, columns=['TYPE'])[0][0]
		#self.i2c_bus_m1 = smbus.SMBus(self.config['ade_config_m1']['i2c_bus'])
		self.i2c_bus_m1 = db.return_values(directory=dir_db, database=name_db, table_name=config_table, columns=['ADE_CONFIG_M1_I2C_BUS'])[0][0]
		self.rst_gpio_m1_chip = db.return_values(directory=dir_db, database=name_db, table_name=config_table, columns=['ADE_CONFIG_M1_RST_GPIO_BLOCK'])[0][0]
		self.rst_gpio_m1 = db.return_values(directory=dir_db, database=name_db, table_name=config_table, columns=['ADE_CONFIG_M1_RST_GPIO_PIN'])[0][0]
		self.ct_current_m1 = db.return_values(directory=dir_db, database=name_db, table_name=config_table, columns=['ADE_CONFIG_M1_CT_CURRENT'])[0][0]
		self._VB_1 = db.return_values(directory=dir_db, database=name_db, table_name=config_table, columns=['ADE_CONFIG_M2_CT_RATIO'])[0][0]
		
		if self.meter_type == 'Dual':
			#self.i2c_bus_m2 = smbus.SMBus(self.config['type']['ade_config_m2']['i2c_bus'])
			self.i2c_bus_m2 = db.return_values(directory=dir_db, database=name_db, table_name=config_table, columns=['ADE_CONFIG_M2_I2C_BUS'])[0][0]
			self.rst_gpio_m2_chip = db.return_values(directory=dir_db, database=name_db, table_name=config_table, columns=['ADE_CONFIG_M2_RST_GPIO_BLOCK'])[0][0]
			self.rst_gpio_m2 = db.return_values(directory=dir_db, database=name_db, table_name=config_table, columns=['ADE_CONFIG_M2_RST_GPIO_PIN'])[0][0]			
			self.ct_current_m2 = db.return_values(directory=dir_db, database=name_db, table_name=config_table, columns=['ADE_CONFIG_M2_CT_CURRENT'])[0][0]
			self._VB_2 = db.return_values(directory=dir_db, database=name_db, table_name=config_table, columns=['ADE_CONFIG_M2_CT_RATIO'])[0][0]


		if db.return_values(directory=dir_db, database=name_db, table_name=config_table, columns=['ADE_CONFIG_M2_I2C_BUS'])[0][0] == '50Hz':
			self.frecuency = R.FREC_50Hz
		else:
			self.frecuency = R.FREC_60Hz
		self.nominal_voltage =  db.return_values(directory=dir_db, database=name_db, table_name=config_table, columns=['NOMINAL_VOLTAGE'])[0][0]
		self.phases = db.return_values(directory=dir_db, database=name_db, table_name=config_table, columns=['PHASES'])[0][0]
		self.load_calibration_data(bus=self.i2c_bus_m1, cv='M1')

		energy_keys = list()
		for i in range(4):
			for j in range(self.phases):
				energy_keys.append(p[(i*3)+j][0])
		connection_en = db.return_values(directory=dir_energy, database="metering_db", table_name="acumulate", columns=energy_keys, where_clause="1 = 1")
		if  len(connection_en)==0:
			self._TReactive_Positive_m1 = [0.0, 0.0, 0.0]
			self._TActive_Positive_m1 = [0.0, 0.0, 0.0]
			self._TActive_Negative_m1 = [0.0, 0.0, 0.0]
			self._TReactive_Negative_m1 = [0.0, 0.0, 0.0]
		else:
			#self._TActive_Positive_m1 = [0.0, 0.0, 0.0]
			#self._TReactive_Positive_m1 = [0.0, 0.0, 0.0]
			#self._TActive_Negative_m1 = [0.0, 0.0, 0.0]
			#self._TReactive_Negative_m1 = [0.0, 0.0, 0.0]
			
			self._TActive_Positive_m1 = [connection_en[0][0], connection_en[0][1], connection_en[0][2]]
			self._TReactive_Positive_m1 = [connection_en[0][3],  connection_en[0][4], connection_en[0][5]]
			self._TActive_Negative_m1 = [connection_en[0][6], connection_en[0][7], connection_en[0][8]]
			self._TReactive_Negative_m1 = [connection_en[0][9], connection_en[0][10], connection_en[0][11]]
	 

	def AutoCalibration1(self, gains: list):
		Count = 0
		Vol_A = [0]*20
		Vol_B = [0]*20
		Vol_C = [0]*20
		Cur_A = [0]*20
		Cur_B = [0]*20
		Cur_C = [0]*20

		

		STATUS1_READ = 0
		exit_prom = True
		
		bus = self.i2c_bus_m1


#		while(True):
#			
#			VolB = self.i2c_read(bus=bus, reg=R.BVRMS, len=4)
#			VolA = self.i2c_read(bus=bus, reg=R.AVRMS, len=4)
#			VolC = self.i2c_read(bus=bus, reg=R.CVRMS, len=4)
#			
#			print("VOLTAGE POST GAINS:")
#			print(f"VolA: {VolA}")
#			print(f"VolB: {VolB}")
#			print(f"VolC: {VolC}")
#			time.sleep(0.1)
		"""self._ADE_Unlock_RAM_Protection(bus=bus)
		self.i2c_write(bus=bus, reg=R.STATUS1, val=0xFFFFFFFF, len=4)
		self._ADE_Lock_RAM_Protection(bus=bus)"""
		

		#print(f"COMPMODE: {self.i2c_read(bus, R.COMPMODE,2)}")
		time.sleep(1.0)
		while(Count < 20):
			

			Vol_B[Count] = self.i2c_read(bus=bus, reg=R.BVRMS, len=4)
			Vol_A[Count] = self.i2c_read(bus=bus, reg=R.AVRMS, len=4)
			Vol_C[Count] = self.i2c_read(bus=bus, reg=R.CVRMS, len=4)

			
			Cur_B[Count] = self.i2c_read(bus=bus, reg=R.BIRMS, len=4)
			Cur_A[Count] = self.i2c_read(bus=bus, reg=R.AIRMS, len=4)
			Cur_C[Count] = self.i2c_read(bus=bus, reg=R.CIRMS, len=4)

			Count += 1
			time.sleep(0.1)
			
		Vol_A_Prom = 0.0
		Vol_B_Prom = 0.0
		Vol_C_Prom = 0.0
		Cur_A_Prom = 0.0
		Cur_B_Prom = 0.0
		Cur_C_Prom = 0.0

		for x in range(20):
			Vol_A_Prom = Vol_A_Prom + Vol_A[x]
			Vol_B_Prom = Vol_B_Prom + Vol_B[x]
			Vol_C_Prom = Vol_C_Prom + Vol_C[x]

			Cur_A_Prom = Cur_A_Prom + Cur_A[x]
			Cur_B_Prom = Cur_B_Prom + Cur_B[x]
			Cur_C_Prom = Cur_C_Prom + Cur_C[x]


		Vol_A_Prom = Vol_A_Prom / 20
		Vol_B_Prom = Vol_B_Prom / 20
		Vol_C_Prom = Vol_C_Prom / 20

		Cur_A_Prom = Cur_A_Prom / 20
		Cur_B_Prom = Cur_B_Prom / 20
		Cur_C_Prom = Cur_C_Prom / 20
		
		print(f"Vol_A: {Vol_A}")
		print(f"Vol_B: {Vol_B}")
		print(f"Vol_C: {Vol_C}")

		print(f"Cur_A: {Cur_A}")
		print(f"Cur_B: {Cur_B}")
		print(f"Cur_C: {Cur_C}")
		# Voltage Initial Gain
		gains[0] = (np.power(2, 23))*((Vol_A_Prom / Vol_B_Prom) - 1 ) #pow(2,23) * ( (Vol_A_Prom / Vol_B_Prom) - 1)
		gains[1] = (np.power(2, 23))*((Vol_A_Prom / Vol_C_Prom) - 1 )

    	# Current Initial Gain
		gains[2] = (np.power(2, 23))*((Cur_A_Prom / Cur_B_Prom) - 1 )
		gains[3] = (np.power(2, 23))*((Cur_A_Prom / Cur_C_Prom) - 1 )

		print(f"Gain_0: {int(gains[0])}")
		print(f"Gain_1: {int(gains[1])}")
		print(f"Gain_2: {int(gains[2])}")
		print(f"Gain_3: {int(gains[3])}")

		self._ADE_Unlock_RAM_Protection(bus=bus)
		


		self.i2c_write(bus=bus, val=int(gains[0]), reg=R.BVGAIN, len=4)
		self.i2c_write(bus=bus, val=int(gains[1]), reg=R.CVGAIN, len=4)
		self.i2c_write(bus=bus, val=int(gains[2]), reg=R.BIGAIN, len=4)
		self.i2c_write(bus=bus, val=int(gains[3]), reg=R.CIGAIN, len=4)
		self._ADE_Lock_RAM_Protection(bus=bus)

		
		
		print(f"AVGAIN: {self.i2c_read(bus=bus, reg=R.AVGAIN, len=4)}")
		print(f"BVGAIN: {self.i2c_read(bus=bus, reg=R.BVGAIN, len=4)}")
		print(f"CVGAIN: {self.i2c_read(bus=bus, reg=R.CVGAIN, len=4)}")

		print(f"AIGAIN: {self.i2c_read(bus=bus, reg=R.AIGAIN, len=4)}")
		print(f"BIGAIN: {self.i2c_read(bus=bus, reg=R.BIGAIN, len=4)}")
		print(f"CIGAIN: {self.i2c_read(bus=bus, reg=R.CIGAIN, len=4)}")

		time.sleep(1)
		Count = 0
		


	def AutoCalibration2(self, lsb, V_input, C_input):
		bus = self.i2c_bus_m1

		print(f"Vol_input: {V_input}")
		print(f"Cur_input: {C_input}")
		self.Voltage_LSB = V_input / self.i2c_read(bus=bus, reg=R.AVRMS, len=4)
		self.Current_LSB = C_input / self.i2c_read(bus=bus, reg=R.AIRMS, len=4)

		lsb[0] = self.Voltage_LSB
		lsb[1] = self.Current_LSB
		
		print(f"Voltage LSB: {self.Voltage_LSB}")
		print(f"Current LSB: {self.Current_LSB}")

		

	def AutoCalibration3(self):
		bus = self.i2c_bus_m1
		self._ADE_Clear_Energy_Registers(bus=bus)
		time.sleep(1.0)
		
		A_App = 0
		B_App = 0
		C_App = 0

		#while True:
		#	print(f"Power factor: {self.i2c_read(bus=bus, reg=R.APF, len=2)/32767}")
		#	time.sleep(1.0)
		

		for i in range(20):
			self.AWA += self.i2c_read(bus=bus, reg=R.AWATTHR, len=4)
			self.BWA += self.i2c_read(bus=bus, reg=R.BWATTHR, len=4)
			self.CWA += self.i2c_read(bus=bus, reg=R.CWATTHR, len=4)
			
			A_App += self.i2c_read(bus=bus, reg=R.AVAHR, len=4)
			B_App += self.i2c_read(bus=bus, reg=R.BVAHR, len=4)
			C_App += self.i2c_read(bus=bus, reg=R.CVAHR, len=4)

			time.sleep(1.0)

		print(f"AWATTHR: {self.AWA}")
		print(f"A_APP: {A_App}")

		self.AWR = np.sqrt((np.power(float(A_App), 2) - np.power(float(self.AWA), 2)))
		self.BWR = np.sqrt((np.power(float(B_App), 2) - np.power(float(self.BWA), 2)))
		self.CWR = np.sqrt((np.power(float(C_App), 2) - np.power(float(self.CWA), 2)))



	def AutoCalibration3_1(self, Phcal: list, Phi: float):
		bus = self.i2c_bus_m1
		print(f"Active Energy: {self.AWA}")
		print(f"Reactive Energy: {self.AWR}")
		Phcal[0] = self._Calculate_Phase_Error(Phi=Phi, Active_Energy=self.AWA, Reactive_Energy=self.AWR)
		Phcal[1] = self._Calculate_Phase_Error(Phi=Phi, Active_Energy=self.BWA, Reactive_Energy=self.BWR)
		Phcal[2] = self._Calculate_Phase_Error(Phi=Phi, Active_Energy=self.CWA, Reactive_Energy=self.CWR)

		self._ADE_Unlock_RAM_Protection(bus=bus)
		
		self.i2c_write(bus=bus, reg=R.APHCAL, val=int(Phcal[0]), len=2)
		self.i2c_write(bus=bus, reg=R.BPHCAL, val=int(Phcal[1]), len=2)
		self.i2c_write(bus=bus, reg=R.CPHCAL, val=int(Phcal[2]), len=2)

		self._ADE_Lock_RAM_Protection(bus=bus)

		time.sleep(1.0)

		print(f"PHCAL {self.i2c_read(bus=bus, reg=R.APHCAL, len=2)}")

	
	def AutoCalibration4(self, Time_acumulate: int, delay_acumulate :InfiniteTimer):
		
		bus = self.i2c_bus_m1
		self._ADE_Clear_Energy_Registers(bus=bus)
		delay_acumulate.start()
		
		while(self.Flag_step4 == False):
			pass
		delay_acumulate.cancel()

		
		self.WH_A_Read = self.i2c_read(bus=bus, reg=R.AWATTHR, len=4)
		self.WH_B_Read = self.i2c_read(bus=bus, reg=R.BWATTHR, len=4)
		self.WH_C_Read = self.i2c_read(bus=bus, reg=R.CWATTHR, len=4)

		print(f"AWATTHR: {self.WH_A_Read}")

	def AutoCalibration4_1(self, lsb: list, Pga: list, WH_input: float, wh_lsb_input: float, FirtsMeter: bool):
		bus = self.i2c_bus_m1
		if FirtsMeter == True:
			self.WH_LSB = WH_input / self.WH_A_Read
		else:
			self.WH_LSB = wh_lsb_input
		
		print(f"WH_LSB: {self.WH_LSB}")
		
		AWATTHR_Expected = WH_input / self.WH_LSB
		print(f"AWATTHR Expected: {AWATTHR_Expected}")

		Pga[0] = np.power(2, 23)*((AWATTHR_Expected / self.WH_A_Read) - 1)
		Pga[1] = np.power(2, 23)*((AWATTHR_Expected / self.WH_B_Read) - 1)
		Pga[2] = np.power(2, 23)*((AWATTHR_Expected / self.WH_C_Read) - 1)

		self._ADE_Unlock_RAM_Protection(bus=bus)
		
		self.i2c_write(bus=bus, reg=R.APGAIN, val=int(Pga[0]), len=4)
		self.i2c_write(bus=bus, reg=R.BPGAIN, val=int(Pga[0]), len=4)
		self.i2c_write(bus=bus, reg=R.CPGAIN, val=int(Pga[0]), len=4)

		self._ADE_Lock_RAM_Protection(bus=bus)
		
		time.sleep(1.0)
		lsb[2] = self.WH_LSB




	def AutoCalibration5(self, lsb: list, Wa_input: float):
		bus = self.i2c_bus_m1
		self.Wac_LSB = Wa_input / self.i2c_read(bus=bus, reg=R.AWATT, len=4)
		lsb[3] = self.Wac_LSB

		print(f"Wac: {self.Wac_LSB}")





	def Calibration_Validation(self, Data_Array: list):
		bus = self.i2c_bus_m1
		#Voltajes
		print("Valibration validation")
		print(f"Voltage LSB: {self.Voltage_LSB}")
		print(f"Current LSB: {self.Current_LSB}")

		print(f"AVGAIN: {self.i2c_read(bus=bus, reg=R.AVGAIN, len=4)}")
		print(f"BVGAIN: {self.i2c_read(bus=bus, reg=R.BVGAIN, len=4)}")
		print(f"CVGAIN: {self.i2c_read(bus=bus, reg=R.CVGAIN, len=4)}")

		print(f"Power factor: {self.i2c_read(bus=bus, reg=R.APF, len=2)/32767}")

		Data_Array[0] = self.i2c_read(bus=bus, reg=R.AVRMS, len=4)*self.Voltage_LSB
		Data_Array[1] = self.i2c_read(bus=bus, reg=R.BVRMS, len=4)*self.Voltage_LSB
		Data_Array[2] = self.i2c_read(bus=bus, reg=R.CVRMS, len=4)*self.Voltage_LSB

		#Corrientes
		Data_Array[3] = self.i2c_read(bus=bus, reg=R.AIRMS, len=4) * self.Current_LSB
		Data_Array[4] = self.i2c_read(bus=bus, reg=R.BIRMS, len=4) * self.Current_LSB
		Data_Array[5] = self.i2c_read(bus=bus, reg=R.CIRMS, len=4) * self.Current_LSB

		#Potencias activas
		Data_Array[6] = self.i2c_read(bus=bus, reg=R.AWATT, len=4) * self.Wac_LSB
		Data_Array[7] = self.i2c_read(bus=bus, reg=R.BWATT, len=4) * self.Wac_LSB
		Data_Array[8] = self.i2c_read(bus=bus, reg=R.CWATT, len=4) * self.Wac_LSB

		#Potencias reactivas
		Apparent_P = self.i2c_read(bus=bus, reg=R.AVA, len=4) * self.Wac_LSB
		Active_P = self.i2c_read(bus=bus, reg=R.AWATT, len=4) * self.Wac_LSB
		Reactive_P = np.power(float(Apparent_P), 2) - np.power(float(Active_P), 2)

		if Reactive_P >= 0:
			Reactive_P = np.sqrt(Reactive_P)
		else:
			Reactive_P = -1*np.sqrt(-1*Reactive_P)
		
		Data_Array[9] = Reactive_P
		
		Apparent_P = self.i2c_read(bus=bus, reg=R.BVA, len=4) * self.Wac_LSB
		Active_P = self.i2c_read(bus=bus, reg=R.BWATT, len=4) * self.Wac_LSB
		Reactive_P = np.power(float(Apparent_P), 2) - np.power(float(Active_P), 2)
		
		if Reactive_P >= 0:
			Reactive_P = np.sqrt(Reactive_P)
		else:
			Reactive_P = -1*np.sqrt(-1*Reactive_P)
		
		Data_Array[10] = Reactive_P

		Apparent_P = self.i2c_read(bus=bus, reg=R.CVA, len=4) * self.Wac_LSB
		Active_P = self.i2c_read(bus=bus, reg=R.CWATT, len=4) * self.Wac_LSB
		Reactive_P = np.power(float(Apparent_P), 2) - np.power(float(Active_P), 2)
		
		if Reactive_P >= 0:
			Reactive_P = np.sqrt(Reactive_P)
		
		else:
			Reactive_P =  -1*np.sqrt(-1 * Reactive_P)
    
		#Fase C
		Data_Array[11] = Reactive_P		
	
		Vol_B = [0]*10
		Vol_A = [0]*10
		Vol_C = [0]*10

		Cur_B = [0]*10
		Cur_A = [0]*10
		Cur_C = [0]*10


		

	def View_New_Calibration_Data(self,Data_Array: list):
		bus = self.i2c_bus_m1
		print("Step view calibration Data")
		#Lectura de voltajes para validar calibracion por MQTT
		Data_Array[0] =self.i2c_read(bus=bus, reg=R.AVRMS, len=4)*self.Voltage_LSB
		Data_Array[1] =self.i2c_read(bus=bus, reg=R.BVRMS, len=4)*self.Voltage_LSB
		Data_Array[2] =self.i2c_read(bus=bus, reg=R.CVRMS, len=4)*self.Voltage_LSB

		

		print(f"V_A: {Data_Array[0]*self.Voltage_LSB}")
		print(f"V_B: {Data_Array[1]*self.Voltage_LSB}")
		print(f"V_C: {Data_Array[2]*self.Voltage_LSB}")

		print(f"V_A: {Data_Array[0]}, V_B: {Data_Array[1]}, V_C: {Data_Array[2]} \n")
		#Lectura de corrientes para validar calibracion por MQTT

		Data_Array[3] =self.i2c_read(bus=bus, reg=R.AIRMS, len=4) * self.Current_LSB
		Data_Array[4] =self.i2c_read(bus=bus, reg=R.BIRMS, len=4) * self.Current_LSB
		Data_Array[5] =self.i2c_read(bus=bus, reg=R.CIRMS, len=4) * self.Current_LSB
		
		print(f"I_A: {Data_Array[3]}, I_B: {Data_Array[4]}, I_C: {Data_Array[5]} \n")
    	#Lectura de Potencias activas para validar calibracion por MQTT

		Data_Array[6] =self.i2c_read(bus=bus, reg=R.AWATT, len=4) * self.Wac_LSB
		Data_Array[7] =self.i2c_read(bus=bus, reg=R.BWATT, len=4) * self.Wac_LSB
		Data_Array[8] =self.i2c_read(bus=bus, reg=R.CWATT, len=4) * self.Wac_LSB

		print(f"APP_A: {Data_Array[6]}, APP_B: {Data_Array[7]}, APP_C: {Data_Array[8]} \n")
		print(f"Power factor: {self.i2c_read(bus=bus, reg=R.APF, len=2)/32767}")
		#Lectura de Potencias reactivas para validar calibracion por MQTT

		Apparent_P =self.i2c_read(bus=bus, reg=R.AVA, len=4) * self.Wac_LSB
		Active_P =self.i2c_read(bus=bus, reg=R.AWATT, len=4) * self.Wac_LSB
		Reactive_P = np.power(float(Apparent_P), 2) - np.power(float(Active_P), 2)
		
		if(Reactive_P >= 0):
			Reactive_P = np.sqrt(Reactive_P)
		else:
			Reactive_P =  -1 * np.sqrt(-1 * Reactive_P)    
		
		Data_Array[9] = Reactive_P


		Apparent_P =self.i2c_read(bus=bus, reg=R.BVA, len=4) * self.Wac_LSB
		Active_P =self.i2c_read(bus=bus, reg=R.BWATT, len=4) * self.Wac_LSB
		Reactive_P = np.power(float(Apparent_P), 2) - np.power(float(Active_P), 2)
		
		if(Reactive_P >= 0):
			Reactive_P = np.sqrt(Reactive_P)
		else:
			Reactive_P =  -1 * np.sqrt(-1 * Reactive_P)    
		
		Data_Array[10] = Reactive_P
	

		Apparent_P =self.i2c_read(bus=bus, reg=R.CVA, len=4) * self.Wac_LSB
		Active_P =self.i2c_read(bus=bus, reg=R.CWATT, len=4) * self.Wac_LSB
		Reactive_P = np.power(float(Apparent_P), 2) - np.power(float(Active_P), 2)
		
		if(Reactive_P >= 0):
			Reactive_P = np.sqrt(Reactive_P)
		else:
			Reactive_P =  -1 * np.sqrt(-1 * Reactive_P)    
		
		Data_Array[11] = Reactive_P

		print(f"RP_A: {Data_Array[9]}, RP_B: {Data_Array[10]}, RP_C: {Data_Array[11]} \n")
		
	






