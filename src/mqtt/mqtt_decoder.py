import os
import yaml
import base64

# Parent directory:
PATH = os.path.join(os.path.dirname(os.path.abspath(__file__)))

with open(os.path.join(PATH, 'frames', 'wv_disag.yml')) as y:
    frame_event = yaml.safe_load(y)

with open(os.path.join(PATH, 'frames', 'realtime.yml')) as y:
    frame_realtime = yaml.safe_load(y)

with open(os.path.join(PATH, 'frames', 'metering4bytes.yml')) as y:
    frame_metering = yaml.safe_load(y)
    
with open(os.path.join(PATH, 'frames', 'realtime_expo.yml')) as y:
    frame_realtime_expo = yaml.safe_load(y)
    
with open(os.path.join(PATH, 'frames', 'realtime_2B.yml')) as y:
    frame_realtime_2B = yaml.safe_load(y)

with open(os.path.join(PATH, 'frames', 'energy4bytes.yml')) as y:
    frame_energy = yaml.safe_load(y)


def map(x, nbytes, ymin, ymax):

    if ymin == None or ymax == None:
        return x

    ymin = float(ymin)
    ymax = float(ymax)
    xmax = pow(2, 8 * nbytes) - 1

    return x * (ymax - ymin) / xmax + ymin

def event_decode(frame):
    n = len(frame)
    k = 9
    if((n < 11) | ((n-5)%k!=0)):
        return "Invalid length "+str(n)
    data={}
    ts = int.from_bytes(frame[0:4], 'big')
    data["Phase"] = frame[4]

    for i in range(0,int((n-5)/k)):
        name = "E"+str(i)
        data[name] = {}
        data[name]["type"]=(frame[5+(k*i)]>>7) & 0b1
        data[name]["ts"]=ts
        data[name]["SBts"]=(frame[5+(k*i)]&0x7F)
        data[name]["E_time"]= (frame[6+(k*i)]+1)
        data[name]["Delta"]= round(map(frame[7+(k*i)]<<8 | frame[8+(k*i)],2,-50,50),2)
        data[name]["Steady"]= round(map(frame[9+(k*i)]<<8 | frame[10+(k*i)],2,0,50),2)
        #if(k == 9):
        	#data[name]["Voltage"]= round(map(frame[11+(9*i)]<<8 | frame[12+(9*i)],2,0,200),2)
        	#data[name]["Shift"] = frame[13+(9*i)]
    return data
def decode(payload, type):

    data = {}
    read_bytes = 0
    #frame = base64.b64decode(payload)
    frame = bytearray()
    frame.extend(payload)

    if type == 'disag_wv':
        #frame_obj = frame_event
        return event_decode(frame)
    elif type == 'realtime':
        frame_obj = frame_realtime
    elif type == 'metering':
        frame_obj = frame_metering
    elif type == 'realtime_expo':
        frame_obj = frame_realtime_expo 
    elif type == 'realtime_2B':
        frame_obj = frame_realtime_2B
    elif type == 'energies':
        frame_obj = frame_energy     
    else:
        raise ValueError('Invalid frame type')

    for f in frame_obj:

        # Dato en entero:
        d = int.from_bytes(frame[read_bytes:read_bytes+f['nbytes']], 'big')

        # Actualizar el número de bytes ya leidos
        read_bytes += f['nbytes']
            
        data[f['var']] = round(
            map(d, f['nbytes'], f['min'], f['max']), 
            f['ndec']
        )
    
    # If checksum available, check for frame integrity:
    if read_bytes + 1 == len(frame):
        checksum_byte = frame[-1]
        checksum = 0x00
        for byte in frame[:-1]:
            checksum += byte
            checksum &= 0xFF
        if checksum_byte != checksum:
            raise ValueError("Integrity check failed. Discard measurement")

    return data
