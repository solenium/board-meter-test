import os, sys
import yaml
import _sqlite3
sys.path.insert(1, '/home/root/app/board-meter/')

from src import utils_db  as db

# Parent directory:
PATH = os.path.join(os.path.dirname(os.path.abspath(__file__)))

#with open(os.path.join(PATH, 'frames', 'metering4bytes.yml')) as y:
#    frame_metering = yaml.safe_load(y)
frame_metering = list()



#with open(os.path.join(PATH, 'frames', 'energy4bytes.yml')) as y:
#    frame_energy = yaml.safe_load(y)
frame_energy = list()
db_folder = "/home/root/app/board-meter/src/mqtt/frames/"

columns = {
    'var': 'TEXT',
    'min': 'INTEGER',
    'max': 'INTEGER',
    'nbytes': 'INTEGER',
    }

metering_values = [
     'vp1', '0', '1000', '2', 
     'vp2', '0', '1000', '2', 
     'vp3', '0', '1000', '2',
     'cp1', '-5000', '5000', '3',
     'cp2', '-5000', '5000', '3',
     'cp3', '-5000', '5000', '3',
     'app1', '-5000000', '5000000', '3',
     'app2', '-5000000', '5000000', '3',
     'app3', '-5000000', '5000000', '3',
     'rpp1', '-5000000', '5000000', '3',
     'rpp2', '-5000000', '5000000', '3',
     'rpp3', '-5000000', '5000000', '3',
     'pfp1', '-1', '1', '2',
     'pfp2', '-1', '1', '2',
     'pfp3', '-1', '1', '2'    
    ]

energy_values = [
     'iaep1', '0', '10000000', '3',
     'iaep2', '0', '10000000', '3',
     'iaep3', '0', '10000000', '3',
     'irep1', '0', '10000000', '3',
     'irep2', '0', '10000000', '3',
     'irep3', '0', '10000000', '3',
     'eaep1', '0', '10000000', '3',
     'eaep2', '0', '10000000', '3',
     'eaep3', '0', '10000000', '3',
     'erep1', '0', '10000000', '3',
     'erep2', '0', '10000000', '3',
     'erep3', '0', '10000000', '3',
    ]
colum_keys = [x for x in columns.keys()]


def create_frames(name: str, variables: list):
    frame = list()
    db.create_database(directory=db_folder, database="frames_db")
    db.create_table(directory=db_folder, database="frames_db", table_name=name, columns=columns)
    connection_rc = db.return_values(directory=db_folder, database="frames_db", table_name=name, columns=colum_keys)
    
    list_input = list()
    if len(connection_rc) == 0:
        #print("db llena")
        for i in  range(int(len(variables)/4)):
            dict_input = {
                    'var': variables[i*4],
                    'min': variables[i*4 + 1],
                    'max': variables[i*4 + 2],
                    'nbytes': variables[i*4 + 3]
                }
            list_input.append(dict_input)
        
        #print(lista_input)
        for j in range(len(list_input)):
            db.insert_values(directory=db_folder, database="frames_db", table_name=name, values=list_input[j])
    frame = db.return_values(directory=db_folder, database="frames_db", table_name=name, columns=colum_keys)
    return frame

frame_metering = create_frames(name="metering4B", variables = metering_values)
frame_energy = create_frames(name="energies4B", variables= energy_values)

def map_to_bytes(x, xmin, xmax, nbytes):

    if xmin is None:
        return x.to_bytes(nbytes, 'big')

    xmin = float(xmin)
    xmax = float(xmax)
    ymax = pow(2, 8 * nbytes) - 1
    x = min(x, xmax)
    x = max(x, xmin)

    return int((x - xmin) * ymax / (xmax - xmin)).to_bytes(nbytes, 'big')


def build_frame(data, frame_id):
    
    with open(PATH + '/../yml/frames/{}.yml'.format(frame_id), 'r') as f:
        params = yaml.load(f)

    payload = bytearray()
    payload.append(frame_id)
    
    for p in params:
        if p['var'] in data:
            curr_bytes = map_to_bytes(data[p['var']], p['min'], p['max'], p['nbytes'])
            payload.extend(curr_bytes)
        else:
            curr_bytes = map_to_bytes(0, p['min'], p['max'], p['nbytes'])
            payload.extend(curr_bytes)

    return payload

def encode_energies(data, nphases):
    payload = bytearray()
    p = frame_energy

    for i in range(4):
        for j in range(nphases):
            curr_bytes = map_to_bytes(data[0+j][i], p[(i*3)+j][1], p[(i*3)+j][2], p[(i*3)+j][3])
            payload.extend(curr_bytes)
    return payload


def encode_metering(data, nphases):
    
    payload = bytearray()
    p = frame_metering


    for i in range(5):
        for j in range(nphases):
            curr_bytes = map_to_bytes(data[0+j][i], p[(i*3)+j][1], p[(i*3)+j][2], p[(i*3)+j][3]) # Para p: 1-> min, 2-> max, 3-> nbytes
            payload.extend(curr_bytes)
    return payload

def encode_realtime(data,min,max,nbytes):
    payload = bytearray()
    for i in data:
        for var in i:
            curr_bytes = map_to_bytes(var, min, max, nbytes)
            payload.extend(curr_bytes)
    
    return payload