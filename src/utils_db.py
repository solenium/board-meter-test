import sqlite3
#
def _database_connection(directory: str, database: str):

    """This function handles the connection to the SQLite3 database. Is the main function for all
    the other functions, and has the lowest level of programming SQLite with Python (nothing hard actually).

    Args:
        [directory]: Absolute of relative path of the folder of the desired database.
        [database]: Name of the desired database to connect to. DON'T ADD the '.db' extension at the end of the name,
        note that is added by default in the function.

    Returns:
        [connecion (sqlite3.Connecion)]: Connection object to handle the database.
    """

    try:
        PATH = directory + "/" + database + '.db'
    except:
        raise ValueError("Insert Correct path to Database. Remember to avoid the .db extension in the database name")
    
    connection = sqlite3.connect(PATH)
    return connection


def create_database(directory: str, database: str):

    """This function just creates a new SQLite3 database file (database-name.db) by connecting to the database name provided to the funciton
    and disconnecting inmediatly.

    Args:
        [directory]: Absolute of relative path of the folder of the desired database.
        [database]: Name of the desired database to create.
    """

    connection = _database_connection(directory=directory, database=database)
    connection.close()


def create_table(directory: str, database: str, table_name: str, columns: dict):

    """This function creates a table inside the database. If database doesn't exists, the function should create it.

    Args:
        [directory]: Absolute of relative path of the folder of the desired database.
        [database]: Name of the database to create the table.
        [table_name]: Name of the table to create.
        [columns]: Dictionary where keys must be column names of the table, and values must be string with column 
        respective type (INTEGER, REAL, TEXT (a date type is a TEXT))

    Returns:
        Nothing. Note thatthe connection is closed inmediatly after the table creation.
    """

    connection = _database_connection(directory=directory, database=database)

    QUERY = ""

    try: 
        for column_name, column_type in columns.items():
            QUERY += " " + column_name + " " + column_type + ","
    except:
        raise ValueError("Columns not passed correctly")
    
    try:
        connection.execute(f"CREATE TABLE IF NOT EXISTS {table_name} (id INTEGER PRIMARY KEY, {QUERY[1:-1]});")
    except:
        raise ValueError("Table not created succesfully. Check table name")

    connection.close()


def insert_values(directory: str, database: str, table_name: str, values: dict):

    """This function insert new values to an existing table in a database. Is mandatory to know the columns structure of the database
    to insert values correctly.

    Args:
        [directory]: Absolute of relative path of the folder of the desired database.
        [database]: Name of the database to insert the values.
        [table_name]: Name of the table to insert values to.
        [values]: Dictionary where keys must be column names of the table (not necessary to put all the columns, just the ones
        to insert), and values must be the value to insert in each column (it must be coherent with the column datatype).

    Returns:
        [connecion (sqlite3.Connecion)]: Connection object to handle the database. Note that the connection must be closed manually 
        outside the function when inserting values (there's no sense for Quoia to close connection automatically when inserting values).
    """
    connection = _database_connection(directory=directory, database=database)

    COLUMNS = tuple(values.keys())
    VALUES = tuple(values.values())
    QUERY = f"INSERT INTO {table_name} {COLUMNS} VALUES {VALUES};"
    #print(QUERY)
    if len(COLUMNS)==1:
        QUERY = f"INSERT INTO {table_name} ({COLUMNS[0]}) VALUES ({VALUES[0]});"
        #This if is necessary because the parsing to tuple when just one column is being inserted

    try:
        connection.execute(QUERY)
        connection.commit()
        
    except:
        raise ValueError("Values not inserted succesfully. Check table name and column names")
    
    return connection

def update_values(directory: str, database: str, table_name: str, values: dict, where_clause=""):

    """This function updates existing values to an existing table in a database. Is mandatory to know the columns structure of the database
    to insert values correctly.

    Args:
        [directory]: Absolute of relative path of the folder of the desired database.
        [database]: Name of the database to insert the values.
        [table_name]: Name of the table to update values to.
        [values]: Dictionary where keys must be column names of the table (not necessary to put all the columns, just the ones
        to update), and values must be the value to update (it must be coherent with the column datatype).
        [where_clause]: String with a where query to filter the exact values to modify.

    Returns:
        [connecion (sqlite3.Connecion)]: Connection object to handle the database. Note that the connection must be closed manually 
        outside the function when updating values.
    """

    if not where_clause:
        raise ValueError("Provide at least one WHERE clause to avoid wrong updating to all the database.")

    connection = _database_connection(directory=directory, database=database)

    SUBQUERY = ""

    for column_name, value in values.items():
        SUBQUERY += " " + column_name + " = " + value + ","

    QUERY = f"UPDATE {table_name} SET {SUBQUERY[1:-1]} WHERE {where_clause};"

    print(QUERY)
    try:
        connection.execute(QUERY)
        connection.commit()
        
    except:
        raise ValueError("Values not inserted succesfully. Check table name, column names and WHERE clause.")
    
    return connection

def return_values(directory: str, database: str, table_name: str, columns: list, where_clause="", debug=False):

    """This function filters values from an existing table in a database, acording to the WHERE clause passed to the function.

    Args:
        [directory]: Absolute of relative path of the folder of the desired database.
        [database]: Name of the database to consult.
        [table_name]: Name of the table to consult.
        [columns]: Tuple with the column names to return in the Query.
        [where_clause]: String with a where query to filter the exact values to return.
        [print]: Boolean to print the filtered table in console.

    Returns:
        [cursor (sqlite3.Cursor)]: Cursor object that holds all the data of the desired Query.
    """
    
    connection = _database_connection(directory=directory, database=database)

    stre = ",".join(columns) # Quitamos los corchetes y separamos las columnas por comas
    QUERY = f"SELECT {stre} FROM {table_name} WHERE {where_clause};"
    #print(f"{QUERY}")
    if not where_clause:
        QUERY = f"SELECT {stre} FROM {table_name};"
        

    try:
        cursor = connection.execute(QUERY)
    except:
        raise ValueError("Values not inserted succesfully. Check table name and column names")
    
    if debug:
        for fila in cursor:
            print(fila)

    
    object_fetch = cursor.fetchall()
    connection.close()

    return object_fetch

def return_column_names(directory: str, database: str, table_name: str):
    """This function search the column names from an existing table in a database.

    Args:
        [directory]: Absolute of relative path of the folder of the desired database.
        [database]: Name of the database to consult.
        [table_name]: Name of the table to consult.
        [print]: Boolean to print the filtered table in console.

    Returns:
        [cursor (sqlite3.Cursor)]: Cursor object that holds all the data of the desired Query.
    """
    connection = _database_connection(directory=directory, database=database)
    QUERY = f"SELECT name FROM pragma_table_info({table_name});"

    try:
        cursor = connection.execute(QUERY)
    except:
        raise ValueError("Values not inserted succesfully. Check table name and column names")


    object_fetch = cursor.fetchall()
    connection.close()

    return object_fetch




if __name__ == "__main__":

    #Use this for manual testing of the functions.

    create_database(directory=".", database="database_")
    create_table(directory=".", database="database_", table_name="measurements", columns={"vp1": "REAL", "vp2": "REAL"})
    connection = insert_values(directory=".", database="database_", table_name="measurements", values={"vp1": "100.222", "vp2": "200.1"})
    connection.close()
