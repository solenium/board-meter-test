"""
Initialization script for all the Quoia's services.

Author: Juan Pablo Toro and Obed Pantoja
Date: 1/12/23W

Description:
- This script implements a sequence of moving the files.service of each feature of the quoia Frontera Fimware.
- The purpose is to have the .services implementation under control versions
- This scrip is executed by the init_quoia_firmware.service

Usage:
- [Provide instructions on how to run or use the code]
- [Include any command-line arguments or input/output formats]

Examples:
- [Show example usages or expected outputs]

Note:
- [Add any additional notes or considerations]
"""

##########################################################################################

# Import required modules
import os
import subprocess
import time

time.sleep(5)

##########################################################################################

# Move the services files to the correct folder

os.system('cp /home/root/app/board-meter/lcd/lcd.service /lib/systemd/system/')
os.system('cp /home/root/app/board-meter/measurements/energies.service /lib/systemd/system/')
os.system('cp /home/root/app/board-meter/measurements/metering.service /lib/systemd/system/')
#os.system('cp /home/root/app/board-meter/mqtt/mqtt.service /etc/systemd/system/')

# Reload the systemctl informatio
os.system('systemctl daemon-reload')
os.system('systemctl start lcd.service')
os.system('systemctl start energies.service')
os.system('systemctl start metering.service')
#os.system('systemctl start mqtt.service')
