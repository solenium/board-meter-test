import paho.mqtt.client as mqtt
from dotenv  import dotenv_values
from dotenv import  set_key
import os, json
from datetime import datetime as dt
import sysv_ipc

from measurements.centralizado_with_Htimers import mac
from src import utils_db as db
from src.mqtt import  mqtt_encoder

frame_energy = mqtt_encoder.frame_energy
service_topic = "q/mt"+mac+"/s"
control_topic = "q/mt"+mac+"/c"
dir_db = "/home/root/app/config_files"
dir_data = "/home/root/app/board-meter/data"
energies_db = "metering_db"
acumulate_table = "acumulate"
name_config_db = "config_values"
mqtt_broaker = 'MQTT_DEFAULT'

def get_config(table_name: str):
    """This functions reads all the columns values of a configuration/calibration database and saves it into a dictionary.

    Args:
        table_name: Name of the table to read.

    Returns:
        dict: Configuration/calibration variables saved in the table that we want to read
    """

    list_conf_keys = db.return_column_names(directory=dir_db, database=name_config_db, table_name=f"\'{table_name}\'")
    list_conf_values = db.return_values(directory=dir_db, database=name_config_db, table_name=table_name, columns=['*'])
    dict_conf = dict()
    #print(list_conf_values)
    for i in range(len(list_conf_keys) - 1):
        dict_conf[list_conf_keys[i+1][0]] = list_conf_values[0][i+1]
        #print(i)      
    return dict_conf


def get_env(file_name: str):
    """This functions reads the environmental variables file and loads it
    to the script so the variables can be used.

    Args:
        Configuration file name.

    Returns:
        [Env Object]: [Environmental object of the environ library with the environmental variables]
    """
    try:
        #env = environ.Env(DEBUG=(bool, False))
        env = dotenv_values(f'/home/root/app/config_files/.env_{file_name}')
        while (len(env) == 0):
            env = dotenv_values(f'/home/root/app/config_files/.env_{file_name}')
        return env
    except:
        raise ValueError("Revisa que el archivo de variables de entorno se llame .env y esté en la misma carpera que este script")


config = get_config(table_name="configuration")
net = get_env(file_name="network")

def buildFrame(data_value: float, pharam: str):
    curr_dict = dict()
    curr_dict[pharam] = str(data_value)
    return curr_dict

def on_connect_mqtt(client, userdata, flags, rc):
    """This is a callback function is called once the client is connected to the broker.

    Args:
        MQTT client object.
    """
    
    print("mqtt connected")
    
    client.subscribe(control_topic)
    client.publish(service_topic,"{\"message_type\":\"start_up\",\"operation_mode\":2}")

def on_disconnect_mqtt(client, userdata, rc):
    """This is a callback function is called once the client gets disconnected from the broker.

    Args:
        MQTT client object.
    """
    print("mqtt disconnected")
    

    
def mqtt_loop(client):
    mqtt_loop_pid=os.getpid()
    try:
        client.connect(host=net[mqtt_broaker + '_SERVER'], port=int(net[mqtt_broaker + '_PORT']), keepalive=30)
        
        client.loop_start()
    except Exception as e:
        print(e)    

def on_message_mqtt(client, userdata, msg):
    print("arrive msg",msg)

    #dev_stage = msg.topic.split('/')[1]
    #device_type = msg.topic.split('/')[3]
    meter_id = msg.topic.split('/')[-2]

    try:
        d = json.loads(msg.payload)
    except json.decoder.JSONDecodeError:
        print("MQTT payload received was not JSON-compatible")
        return

    #response_topic = f"quoia/{dev_stage}/device/{device_type}/energy/{meter_id}/control"

    try:
        stage = d['cmd']
    except KeyError:
        return
    
    # if (myObject.hasOwnProperty("ct_c") &&
    # myObject.hasOwnProperty("ct_t") &&
    # myObject.hasOwnProperty("w_p") &&
    # myObject.hasOwnProperty("n_v") &&
    # myObject.hasOwnProperty("s_t")
    if stage == 'set_settings':
        try:
            
            new_config = dict()
            new_config['ADE_CONFIG_M1_CT_CURRENT'] = str(d['ct_c'])

            new_config['ADE_CONFIG_M1_CT_RATIO'] = '0.353553391'
                


            new_config["PHASES"] = str(d["w_p"])
            new_config["NOMINAL_VOLTAGE"] = str(d["n_v"])

            new_config["SETTINGS_FLAG"] = str("1")
            #Settings_Received = True
            new_config["TIME_METERING"] = str(d["s_t"])
            new_config["ACUMULATE"] = str("1")
            


            for key, value in new_config.items():
                conn = db.update_values(directory=dir_db, database=name_config_db, table_name="configuration", values=new_config, where_clause="id=1")
                conn.close()
            

            message = "Update"
            centralized_queue.send(message.encode(), type=1)
            realtime_queue.send(message.encode(), type=1)
            metering_queue.send(message.encode(), type=1)
            energies_queue.send(message.encode(), type=1)
            print(f"Productor: Enviado '{message}' a la cola")
            return
        
            

        except json.decoder.JSONDecodeError:
            print(f"MQTT payload received was not JSON-compatible")
            return
    #TODO: Replantear mejor la logica del modo de operacion
    elif stage == 'set_operation_mode':
        pass
    elif stage == 'info_operation_mode':
        pass

    elif stage == 'set_nominal_voltage':
        try:
            new_config = dict()
            new_config["NOMINAL_VOLTAGE"] = str(d["n_v"])
            for key, value in new_config.items():
                conn = db.update_values(directory=dir_db, database=name_config_db, table_name="configuration", values=new_config, where_clause="id=1")
                conn.close()
            
            message = "Update"
            centralized_queue.send(message.encode(), type=1)
            print(f"Productor: Enviado '{message}' a la cola")
            return

        except json.decoder.JSONDecodeError:
            print(f"MQTT payload received was not JSON-compatible")
            return
    elif stage == 'info_nominal_voltage':
        pass

    elif stage == 'set_works_phases':
        try:
            new_config = dict()
            new_config["PHASES"] = str(d["w_p"])
            for key, value in new_config.items():
                conn = db.update_values(directory=dir_db, database=name_config_db, table_name="configuration", values=new_config, where_clause="id=1")
                conn.close()
            
            message = "Update"
            centralized_queue.send(message.encode(), type=1)
            realtime_queue.send(message.encode(), type=1)
            metering_queue.send(message.encode(), type=1)
            energies_queue.send(message.encode(), type=1)
            print(f"Productor: Enviado '{message}' a la cola")
            return

        except json.decoder.JSONDecodeError:
            print(f"MQTT payload received was not JSON-compatible")
            return
    elif stage == 'info_works_phases':
        response = dict()
        response["cmd"] =  "info_works_phases"
        response["w_p"] = config['PHASES']

        client.publish(service_topic, json.dumps(response))
    elif stage == 'set_send_time':
        try:
            new_config = dict()
            new_config["TIME_METERING"] = str(d["s_t"])
            for key, value in new_config.items():
                conn = db.update_values(directory=dir_db, database=name_config_db, table_name="configuration", values=new_config, where_clause="id=1")
                conn.close()
            
            metering_queue.send(message.encode(), type=1)
            return

        except json.decoder.JSONDecodeError:
            print(f"MQTT payload received was not JSON-compatible")
            return
    elif stage == 'info_send_time':
        response = dict()
        response["cmd"] = "info_send_time"
        response["s_t"] = config['TIME_METERING']
        response["rt_e"] = bool(config['REALTIME_ENABLE'])
        response["rt_t"] = config['TIME_REALTIME']
        response["en_t"] =  config['TIME_ENERGIES']

        client.publish(service_topic, json.dumps(response))


    elif stage == 'set_ct_current':
        try:
            new_config = dict()
            new_config['ADE_CONFIG_M1_CT_CURRENT'] = str(d['ct_c'])
            for key, value in new_config.items():
                conn = db.update_values(directory=dir_db, database=name_config_db, table_name="configuration", values=new_config, where_clause="id=1")
                conn.close()

            message = "Update"
            centralized_queue.send(message.encode(), type=1)
            realtime_queue.send(message.encode(), type=1)

            print(f"Productor: Enviado '{message}' a la cola")

            return

        except json.decoder.JSONDecodeError:
            print(f"MQTT payload received was not JSON-compatible")
            return
    
    elif stage == 'info_ct':
        response = dict()
        response["cmd"] = "info_ct"
        response["ct_c"] = config['ADE_CONFIG_M1_CT_CURRENT']
        client.publish(service_topic, json.dumps(response))
        

    elif stage == 'set_cv':
        try:
            response = dict()

            response["vgb1"] = []
            response["igb1"] = []
            response["vgc1"] = []
            response["igc1"] = []

            response["pga1"] = []
            response["pgb1"] = []
            response["pgc1"] = []

            response["poga1"] = []
            response["pogb1"] = []
            response["pogc1"] = []

            response["v"] = []
            response["c"] = []
            response["wh"] = []
            response["wac"] = []


            calibration  =  get_config(table_name='calibration')
            calibration['VGB_M1'] = str(d["vgb1"])
            calibration['IGB_M1'] = str(d["igb1"])
            calibration['VGC_M1'] = str(d["vgc1"])
            calibration['IGC_M1'] = str(d["igc1"])

            #PGains
            calibration['POGA_M1'] = str(d["poga1"])
            calibration['POGB_M1'] = str(d["pogb1"])
            calibration['POGC_M1'] = str(d["pogc1"])

            #Phcals
            calibration['PGA_M1'] = str(d["pga1"])
            calibration['PGB_M1'] = str(d["pgb1"])
            calibration['PGC_M1'] = str(d["pgc1"])

            #LSB
            calibration['LSB_VOL_M1'] =  str(d["v"])
            calibration['LSB_CURR_M1'] = str(d["c"])
            calibration['LSB_WH_M1'] =   str(d["wh"])
            calibration['LSB_WAC_M1'] =  str(d["wac"])
            
            for key, value in calibration.items():
                conn = db.update_values(directory=dir_db, database=name_config_db, table_name="calibration", values=calibration, where_clause="1 = 1")
                conn.close()
            
            message = "Update"
            centralized_queue.send(message.encode(), type=1)
            print(f"Productor: Enviado '{message}' a la cola")

            return

        except json.decoder.JSONDecodeError:
            print(f"MQTT payload received was not JSON-compatible")
            return
        
    elif stage == 'info_cv':
        calibration  =  get_config(table_name='calibration')
        response = dict()
        response["cmd"]= "info_cv"
        response["vgb1"] = calibration['VGB_M1']
        response["igb1"] = calibration['IGB_M1']
        response["vgc1"] = calibration['VGC_M1']
        response["igc1"] = calibration['IGC_M1']

        #PGains
        response["poga1"] = calibration['POGA_M1']
        response["pogb1"] = calibration['POGB_M1']
        response["pogc1"] = calibration['POGC_M1']
        
        #Phcals
        response["pga1"] = calibration['PGA_M1']
        response["pgb1"] = calibration['PGB_M1']
        response["pgc1"] = calibration['PGC_M1']
        
        #LSB
        response["v"] = calibration['LSB_VOL_M1']
        response["c"] = calibration['LSB_CURR_M1']
        response["wh"] = calibration['LSB_WH_M1']
        response["wac"] = calibration['LSB_WAC_M1']
        client.publish(service_topic, json.dumps(response))

    elif stage == 'info_firmware':
        response = dict()
        response["cmd"] = "info_firmware"
        response["f_v"] = "QuoiaFrontera-V1.0"

    elif stage == 'clear_energy':

        p = frame_energy
        config = get_config(table_name="configuration")
        data_energy =[[0]*4 for i in range(3)]
        
        date = int(round(dt.now().timestamp()))

        data_dict = dict()
        data_dict['date'] = date

        for i in range(4):
            for j in range(int(config['PHASES'])):
                curr_dict = buildFrame(data_energy[j][i], p[(i*3)+j][0])
                data_dict |= curr_dict
                
            for k in range(int(config["PHASES"]), 3):
                curr_dict = buildFrame("NULL", p[(i*3)+k][0])
                data_dict |= curr_dict
        connection = db.update_values(directory=dir_data, database=energies_db, table_name=acumulate_table, values=data_dict, where_clause="1=1")
        connection.close()

        message = "Update"
        centralized_queue.send(message.encode(), type=1)
        print(f"Productor: Enviado '{message}' a la cola")

        #TODO: Revisar logica cuando se borran las filas de energia
    
    elif stage == 'set_rt_time':
        try:
            new_config = dict()
            new_config["TIME_REALTIME"] = str(d["r_t"])
            for key, value in new_config.items():
                conn = db.update_values(directory=dir_db, database=name_config_db, table_name="configuration", values=new_config, where_clause="id=1")
                conn.close()
        
            message = "Update"
            realtime_queue.send(message.encode(), type=1)
        except json.decoder.JSONDecodeError:
            print(f"MQTT payload received was not JSON-compatible")
            return
    
    elif stage == 'set_en_time':
        try:
            new_config = dict()
            new_config["TIME_ENERGIES"] = str(d["e_t"])
            for key, value in new_config.items():
                conn = db.update_values(directory=dir_db, database=name_config_db, table_name="configuration", values=new_config, where_clause="id=1")
                conn.close()
            message = "Update"
            energies_queue.send(message.encode(), type=1)
        except json.decoder.JSONDecodeError:
            print(f"MQTT payload received was not JSON-compatible")
            return
        
    
    elif stage == 'info_all_data':
        try:

            response = dict()
            config = get_config(table_name="configuration")
            response["cmd"] = "info_all_data"
            response["ts"] = int(round(dt.now().timestamp()))
            response["n_v"] = config['NOMINAL_VOLTAGE']
            response["w_p"] = config['PHASES']
            response["ct_c"] = config['ADE_CONFIG_M1_CT_CURRENT']
            response["s_t"] = config['TIME_METERING']
            response["rt_e"] = bool(config['REALTIME_ENABLE'])
            response["rt_t"] = config['TIME_REALTIME']
            response["en_t"] =  config['TIME_ENERGIES']
            #response["f_v"] = ""
            response["o_m"] = config['OPERATION_MODE']
            response["wifi_name"] = net['WIFI_SSID']
            client.publish(service_topic, json.dumps(response))
        
        except json.decoder.JSONDecodeError:
            print(f"MQTT payload received was not JSON-compatible")
            return


    


def main():
        #sys.exit(0)
    mqttClient = mqtt.Client("control"+mac)
    mqtt_broaker='MQTT_'+config['MQTT_BROKER']
    print(f"Broker: {mqtt_broaker}")
    mqttClient.username_pw_set(username=net[mqtt_broaker + '_USER'],password=net[mqtt_broaker + '_PASSW'])
    mqttClient.on_connect = on_connect_mqtt
    mqttClient.on_disconnect = on_disconnect_mqtt
    mqttClient.on_message = on_message_mqtt
    mqtt_loop(mqttClient)
    

if __name__=="__main__":
    try:
        centralized_queue = sysv_ipc.MessageQueue(1234567, sysv_ipc.IPC_CREAT)
        realtime_queue = sysv_ipc.MessageQueue(12345678, sysv_ipc.IPC_CREAT)
        metering_queue = sysv_ipc.MessageQueue(123456789, sysv_ipc.IPC_CREAT)
        energies_queue = sysv_ipc.MessageQueue(1234567890, sysv_ipc.IPC_CREAT)

    except sysv_ipc.ExistentialError:
        print("Error al inicializar las colas o el ADE")
    main()